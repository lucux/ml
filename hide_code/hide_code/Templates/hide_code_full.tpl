{%- extends 'full.tpl' -%}

{%-block html_head-%}
    <style type="text/css">
        div.output_subarea {
            max-width: 100% !important;
        }

.cell, img, .table{
page-break-inside: avoid !important;
}
        .table {
    border-collapse: collapse !important;
    width: 100%;
    margin-bottom: 20px;
}

.table thead {
    display: table-header-group;
    height: 1.1cm;
}

.table tfoot {
    display: table-row-group;
}

.table tr {
    page-break-inside: avoid !important;
}
    </style>
    {{ super() }}
{%- endblock html_head -%}

{%- block input -%}
    {%- if cell.metadata.hidden -%}
    {%- elif cell.metadata.hideCode or cell.metadata.hideAll -%}
        <div></div>
    {%- else -%}
        {{ super() }}
    {%- endif -%}
{%- endblock input -%}

{%- block in_prompt -%}
    {%- if cell.metadata.hidden -%}
    {%- elif cell.metadata.hidePrompt or cell.metadata.hideAll -%}
        <div class="prompt input_prompt"></div>
    {%- else -%}
        {{ super() }}
    {%- endif -%}
{%- endblock in_prompt -%}

{% block output %}
    <div class="output_area">
    {% if resources.global_content_filter.include_output_prompt %}
        {% block output_area_prompt %}
            {%- if cell.metadata.hidden -%}
            {%- elif output.output_type == 'execute_result' and not cell.metadata.hidePrompt and not cell.metadata.hideAll -%}
                <div class="prompt output_prompt">
                {%- if cell.execution_count is defined -%}
                    Out[{{ cell.execution_count|replace(None, "&nbsp;") }}]:
                {%- else -%}
                    Out[&nbsp;]:
                {%- endif -%}
            {%- else -%}
                <div class="prompt">
            {%- endif -%}
        </div>
        {% endblock output_area_prompt %}
    {% endif %}
{%- if cell.metadata.hideOutput or cell.metadata.hideAll or cell.metadata.hidden -%}
{%- else -%}
    {% if output.data %}
        {% block execute_result -%}	{{ super() }} {% endblock execute_result %}
        {% block stream -%} {{ super() }} {% endblock stream -%}
        {%- if output.output_type == 'error' -%}
            {% block error %} {{ super() }} {% endblock error %}
        {%- endif -%}
    {%- elif output.text -%}
		{% block stream_stdout -%} {{ super() }} {% endblock stream_stdout -%}
    {%- endif -%}
{%- endif -%}
</div>
{% endblock output %}

{% block markdowncell scoped %}

{%- if cell.metadata.hidden -%}
{%- elif cell.metadata.heading_collapsed -%}
<div class="cell border-box-sizing text_cell rendered collapsible_headings_collapsed">
<div class="prompt input_prompt"><div class="collapsible_headings_toggle" style="color: rgb(170, 170, 170);"><div class="h1"><i>></i></div></div></div>
{%- else -%}
<div class="cell border-box-sizing text_cell rendered">
<div class="prompt input_prompt"><div class="collapsible_headings_toggle" style="color: rgb(170, 170, 170);"><div class="h1"><i>\/</i></div></div></div>
{%- endif -%}
{{ super() }}
<div class="inner_cell">
<div class="text_cell_render border-box-sizing rendered_html">
{{ super() }}
</div>
</div>
</div>
{%- endblock markdowncell %}

