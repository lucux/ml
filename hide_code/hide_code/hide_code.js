/* -*- coding: utf-8 -*-
* ----------------------------------------------------------------------------
* Copyright (c) 2015 - Chris Kirby
*
* Distributed under the terms of the MIT License.
*
* An IPython notebook extension to hide code inputs and prompts for presentation.
* -----------------------------------------------------------------------------
*/

define([
   'require',
'jquery',
'notebook/js/celltoolbar',
'base/js/namespace',
"base/js/events"
], 
function (requirejs,$, celltoolbar, Jupyter,ev){
  "use strict";

  const ALL = 3;
  const CODE = 1;
  const PROMPT = 0;
  const OUTPUT = 2;

  var ctb = celltoolbar.CellToolbar;

  function hideCodeSetter(cell, value){
    if (cell.metadata.hideCode == undefined){ cell.metadata.hideCode = false }
      if (value === undefined) value = !cell.metadata.hideCode;

        cell.metadata.hideCode = value;
        toggleHideCode(cell);
  }

  function hideCodeGetter(cell){
    var isHidden = cell.metadata.hideCode;
    toggleHideCode(cell);
    return (isHidden == undefined) ? undefined : isHidden
  }

    function hideAllSetter(cell, value){
        if (cell.metadata.hideAll == undefined){ cell.metadata.hideAll = false }
      if (value === undefined) value = !cell.metadata.hideAll;
        cell.metadata.hideAll = value;
        hideCodeSetter(cell,value);
        hidePromptSetter(cell,value);
        hideOutputSetter(cell,value);
        // toggleHideAll(cell);
    }

    function hideAllGetter(cell){
        var isHidden = cell.metadata.hideAll;
        toggleHideCode(cell);
        toggleHidePrompt(cell);
        toggleHideOutput(cell);
        return (isHidden == undefined) ? undefined : isHidden
    }

  /*
  * Sets notebook cell's hide code metadata.
  * @param {Notebook Cell} cell
  * @param {Boolean} value
  */
  function hidePromptSetter(cell, value){
    if (cell.metadata.hidePrompt == undefined){ cell.metadata.hidePrompt = false }
    if (value === undefined) value = !cell.metadata.hidePrompt;
        cell.metadata.hidePrompt = value;
      toggleHidePrompt(cell);
  }

  function hidePromptGetter(cell){
    var isHidden = cell.metadata.hidePrompt;
    toggleHidePrompt(cell);
    return (isHidden == undefined) ? undefined : isHidden
  }

  function hideOutputSetter(cell, value){
    if (cell.metadata.hideOutput == undefined){ cell.metadata.hideOutput = false }
         if (value === undefined) value = !cell.metadata.hideOutput;

        cell.metadata.hideOutput = value;
      toggleHideOutput(cell);
  }

  function hideOutputGetter(cell){
    var isHidden = cell.metadata.hideOutput;
    toggleHideOutput(cell);
    return (isHidden == undefined) ? undefined : isHidden
  }

  function toggleHideCode(cell){
    var c = $(cell.element);
    if(cell.class_config.classname != 'MarkdownCell'){
      if (cell.metadata.hideCode){
        c.find('.input_area').removeClass("hide").addClass("hide"); 
      } else {
        c.find('.input_area').removeClass("hide"); 
      }
    }
  }
function addCSS(name){
var link = document.createElement("link");
        link.type = "text/css";
        link.rel = "stylesheet";
        link.href = requirejs.toUrl(name);
        document.getElementsByTagName("head")[0].appendChild(link);
}

  function toggleHidePrompt(cell){
    var c = $(cell.element);
    if(cell.class_config.classname != 'MarkdownCell'){
      if (cell.metadata.hidePrompt){
        c.find('.prompt').removeClass("visiHide").addClass("visiHide");
      } else {
        c.find('.prompt').removeClass("visiHide"); 
      }

    }
  }

  function toggleHideOutput(cell){
    var c = $(cell.element);
    if(cell.class_config.classname != 'MarkdownCell'){
      if (cell.metadata.hideOutput){
        c.find('.output_wrapper .output_subarea').parent().parent().removeClass("hide").addClass("hide");
      } else {
        c.find('.output_wrapper .output_subarea').parent().parent().removeClass("hide");
      }

    }
  }

  function toggleAllPromptsAndCode(){
    var all_hidden = Jupyter.notebook.metadata.hide_code_all_hidden;

        if ( all_hidden == undefined ) { 
          all_hidden = false; 
        };
      if ( all_hidden == false ) {
          Jupyter.notebook.metadata.hide_code_all_hidden = true;
          hideEach(CODE, Jupyter.notebook.get_cells(), true);
          hideEach(PROMPT, Jupyter.notebook.get_cells(), true);
        } else {
          Jupyter.notebook.metadata.hide_code_all_hidden = false;
          hideEach(CODE, Jupyter.notebook.get_cells(), false);
          hideEach(PROMPT, Jupyter.notebook.get_cells(), false);

        }
  }
    function toggleAllPrompts(){
        var all_hidden = Jupyter.notebook.metadata.hide_prompt_all_hidden;

        if ( all_hidden == undefined ) { 
            all_hidden = false; 
        };
        if ( all_hidden == false ) {
            Jupyter.notebook.metadata.hide_prompt_all_hidden = true;
            // hideEach(CODE, Jupyter.notebook.get_cells(), true);
            hideEach(PROMPT, Jupyter.notebook.get_cells(), true);
        } else {
            Jupyter.notebook.metadata.hide_prompt_all_hidden = false;
            // hideEach(CODE, Jupyter.notebook.get_cells(), false);
            hideEach(PROMPT, Jupyter.notebook.get_cells(), false);

        }
    }

  /**
  * Export handlers
  **/

    function exportHTML(){
        window.location = exportLink("html");
        Jupyter.notebook.kernel.reconnect();
    }

    function exportPDF(){
        window.location = exportLink("pdf");
        Jupyter.notebook.kernel.reconnect();
    }

    function exportPDFLatex(){
        window.location = exportLink("latexpdf");
        Jupyter.notebook.kernel.reconnect();
    }

  /**
  * Add a toolbar button to toggle visibility of all code cells, input/output prompts, and remove any highlighting for the selected cell.
  **/
  function addHideCodeButtonToToolbar(){
    Jupyter.toolbar.add_buttons_group([
        Jupyter.actions.register ({
                'help': 'Toggle everything',
                'icon': 'fa-code',
                'handler': toggleAllPromptsAndCode
        }, 'hide_code:toggle_all'),
            Jupyter.actions.register ({
                'help': 'Export to HTML',
                'icon': 'fa-file-text-o',
                'handler': exportHTML
        }, 'hide_code:export_html'),
        Jupyter.actions.register ({
                'help': 'Export to PDF via HTML',
                'icon': 'fa-file-pdf-o',
                'handler': exportPDF
        }, 'hide_code:export_pdf'),
        Jupyter.actions.register ({
                'help': 'Export to PDF via Latex',
                'icon': 'fa-file-o',
                'handler': exportPDFLatex
        }, 'hide_code:export_pdflatex')
      ]);
  }

  function exportLink(path){
    return window.location.origin + window.location.pathname + "/export/" + path;
  }

  var hideCodeCallback = ctb.utils.checkbox_ui_generator( 'Hide Code ', hideCodeSetter, hideCodeGetter);

  var hidePromptCallback = ctb.utils.checkbox_ui_generator('Hide Prompts ', hidePromptSetter, hidePromptGetter);

  var hideOutputCallback = ctb.utils.checkbox_ui_generator('Hide Outputs ', hideOutputSetter, hideOutputGetter);

    var hideAllCallback = ctb.utils.checkbox_ui_generator('Hide All ', hideAllSetter, hideAllGetter);

    function toggleAll(cells) {
        $.each(cells, function(index, value){
            var cell=cells[index];
            hideAllSetter(cell,!cell.metadata.hideAll);
        });
    }
  function hideEach(type, cells){
    $.each(cells, function(index, value){
      var hidden=false
      switch(type){
        case PROMPT:
          hidden=hidePromptSetter(cells[index]);
          break;
        case CODE:
          hidden=hideCodeSetter(cells[index]);
          break;
        case OUTPUT:
          hidden=hideOutputSetter(cells[index]);
          break;
        case ALL:
          hidden=hideAllSetter(cells[index]);
          break;
      }
      try{
        $($(Jupyter.notebook.get_selected_cell().celltoolbar.inner_element[0]).find('input')[type]).prop('checked', hidden);
      } catch(err){
        //do nothing
      }
    });
  }

  /*
  Keyboard shortcut handlers.
  */

  var hideCodeKeyboardAction = Jupyter.keyboard_manager.actions.register({
      help: 'Hides selected cell\'s code',
      icon: 'fa-code',
      help_index: '',
      handler: function(env){
        hideEach(CODE, env.notebook.get_selected_cells());
      },
    }, 'hide_code_action','hide_code');

  /**
  * Keyboard short cut action to hide selected cells.
  * Binds to W
  **/
  var showCodeKeyboardAction = Jupyter.keyboard_manager.actions.register({
      help: 'Shows selected cell\'s code',
      icon: 'fa-code',
      help_index: '',
      handler: function(env){
        hideEach(CODE, env.notebook.get_selected_cells(), false);
      },
    }, 'show_code_action','hide_code');

    var hidePromptKeyboardAction = Jupyter.keyboard_manager.actions.register({
      help: 'Hides selected cell\'s prompts.',
      icon: 'fa-code',
      help_index: '',
      handler: function(env){
        hideEach(PROMPT, env.notebook.get_selected_cells());
      },
    }, 'hide_prompt_action','hide_code');

  /**
  * Keyboard short cut action to hide selected cells.
  * Binds to W,W
  **/
  var showPromptKeyboardAction = Jupyter.keyboard_manager.actions.register({
      help: 'Shows selected cell\'s prompts.',
      icon: 'fa-code',
      help_index: '',
      handler: function(env){
        hideEach(PROMPT, env.notebook.get_selected_cells(), false);
      },
    }, 'show_prompt_action','hide_code');

    var showAllKeyboardAction = Jupyter.keyboard_manager.actions.register({
        help: 'hide All in selected cell\'s prompts.',
        icon: 'fa-code',
        help_index: '',
        handler: function(env){
            toggleAll(env.notebook.get_selected_cells())
            //hideEach(ALL, env.notebook.get_selected_cells(), true);
        },
    }, 'toggle_all_action','hide_code');

    var hideOutputKeyboardAction = Jupyter.keyboard_manager.actions.register({
      help: 'Hides selected cell\'s prompts.',
      icon: 'fa-code',
      help_index: '',
      handler: function(env){
        hideEach(OUTPUT, env.notebook.get_selected_cells());
      },
    }, 'hide_output_action','hide_code');

  /**
  * Keyboard short cut action to hide selected cells.
  * Binds to W,W
  **/
  var showOutputKeyboardAction = Jupyter.keyboard_manager.actions.register({
      help: 'Shows selected cell\'s prompts.',
      icon: 'fa-code',
      help_index: '',
      handler: function(env){
        hideEach(OUTPUT, env.notebook.get_selected_cells(), false);
      },
    }, 'show_output_action','hide_code');

    var hidePromptsAndCodeKeyboardAction = Jupyter.keyboard_manager.actions.register({
      help: 'Hides selected cell\'s prompts and code.',
      icon: 'fa-code',
      help_index: '',
      handler: function(env){
        toggleAllPromptsAndCode()
      },
    }, 'toggle_prompt_and_code_action','hide_code');

var hidePromptsKeyboardAction = Jupyter.keyboard_manager.actions.register({
        help: 'Hides selected cell\'s prompts.',
        icon: 'fa-code',
        help_index: '',
        handler: function(env){
            toggleAllPrompts()
        },
    }, 'toggle_prompt_action','hide_code');

    function addKeyboardShortcutBindings(){
      Jupyter.keyboard_manager.command_shortcuts.add_shortcut('shift-c', 'hide_code:hide_code_action', 'hide_code');
        // Jupyter.keyboard_manager.command_shortcuts.add_shortcut('shift-c', 'hide_code:show_code_action', 'hide_code');
        Jupyter.keyboard_manager.command_shortcuts.add_shortcut('shift-p', 'hide_code:hide_prompt_action', 'hide_code');
        // Jupyter.keyboard_manager.command_shortcuts.add_shortcut('shift-w', 'hide_code:show_prompt_action', 'hide_code');
        Jupyter.keyboard_manager.command_shortcuts.add_shortcut('shift-o', 'hide_code:hide_output_action', 'hide_code');
        // Jupyter.keyboard_manager.command_shortcuts.add_shortcut('shift-r', 'hide_code:show_output_action', 'hide_code');
        Jupyter.keyboard_manager.command_shortcuts.add_shortcut('ctrl-p', 'hide_code:toggle_prompt_and_code_action', 'hide_code');
        Jupyter.keyboard_manager.command_shortcuts.add_shortcut('shift-h', 'hide_code:toggle_all_action', 'hide_code');
        // Jupyter.keyboard_manager.command_shortcuts.add_shortcut('shift-o', 'hide_code:toggle_prompt_action', 'hide_code');

    }

    function addHideCodeMenuItem(){
        var menu = getMenuBar()
        menu.append(menuItem('Hide Code','#'));

        getHideCodeMenu().append(dropdownMenuItem('PDF Export (HTML)', exportLink('pdf'), 'HTML PDF exporter.'));
        getHideCodeMenu().append(dropdownMenuItem('PDF Export (Latex)', exportLink('latexpdf'), 'Latex PDF exporter.'));
        getHideCodeMenu().append(dropdownMenuItem('HTML Export', exportLink('html'), 'HTML exporter.'));
        getHideCodeMenu().append(dropdownMenuItem('Latex Export', exportLink('latex'), 'Latex exporter.'));
        getHideCodeMenu().append(dropdownMenuItem('Slides Export', exportLink('slides'), 'Slides exporter.'));
    }

    function getMenuBar(){
      return $('#menus').find('.navbar-nav')
    }

    function getHideCodeMenu(){
      return $('#hide_code_menu_list')
    }

    function dropdownMenuItem(text, url, tooltip){
      var item = $('<li>', {title: tooltip, });
      var link = $('<a>', {href: url, text: text});
      item.append(link);
      return item
    }

    function menuItem(text, url){
      var menu_item = $('<li>', {class: 'dropdown', id: 'hide_code_menu_item'});
      var link = $('<a>', {class: 'dropdown-toggle', href: url, text: text, 'data-toggle': 'dropdown'});
      var dropdown_menu = $('<ul>', {id: 'hide_code_menu_list', class: 'dropdown-menu'});
      menu_item.append(link);
      menu_item.append(dropdown_menu);
      return menu_item
    }
  function initialize_states(){

            var cells = Jupyter.notebook.get_cells();
            // console.log('cells',cells);\/
            $.each(cells, function(index, cellp){
                var cell=cells[index];
                hideAllGetter(cell);
            });
            console.log('hide_code setup complete');
  }
  function load_ipython_extension(){
    addCSS("./main.css");
    ctb.register_callback('hide_code.hideCode', hideCodeCallback);
        ctb.register_callback('hide_code.hidePrompts', hidePromptCallback);
        ctb.register_callback('hide_code.hideOutputs', hideOutputCallback);
        ctb.register_callback('hide_code.hideAll', hideAllCallback);

        ctb.register_preset('Hide code',['hide_code.hidePrompts','hide_code.hideAll','hide_code.hideCode','hide_code.hideOutputs']);
        addHideCodeButtonToToolbar();
        addKeyboardShortcutBindings();
        addHideCodeMenuItem();
        ev.on('notebook_loaded.Notebook',initialize_states) ;
        if (Jupyter.notebook !== undefined && Jupyter.notebook._fully_loaded) {
                // notebook already loaded, so we missed the event, so update all
                initialize_states();
        }
  }
  
  // setup();
  

  return {
        load_jupyter_extension: load_ipython_extension,
        load_ipython_extension: load_ipython_extension
    }
});


