import numpy as np
def isInt(i):
    return isinstance(i,int)
def isStr(i):
    return isinstance(i,str)
def isNumpyArr(i):
    return isinstance(i,np.ndarray)