#!/usr/bin/env python
# coding: utf-8

# **Chapter 3 – Classification**
# 
# _This notebook contains all the sample code and solutions to the exercises in chapter 3._

# # Setup

# First, let's import a few common modules, ensure MatplotLib plots figures inline and prepare a function to save the figures. We also check that Python 3.5 or later is installed (although Python 2.x may work, it is deprecated so we strongly recommend you use Python 3 instead), as well as Scikit-Learn ≥0.20.

# In[1]:


# Python ≥3.5 is required
import sys
assert sys.version_info >= (3, 5)

# Scikit-Learn ≥0.20 is required
import sklearn
assert sklearn.__version__ >= "0.20"

# Common imports
import numpy as np
import os

# to make this notebook's output stable across runs
np.random.seed(42)

# To plot pretty figures
get_ipython().run_line_magic('matplotlib', 'inline')
import matplotlib as mpl
import matplotlib.pyplot as plt
mpl.rc('axes', labelsize=14)
mpl.rc('xtick', labelsize=12)
mpl.rc('ytick', labelsize=12)

# Where to save the figures
PROJECT_ROOT_DIR = "."
CHAPTER_ID = "classification"
IMAGES_PATH = os.path.join(PROJECT_ROOT_DIR, "images", CHAPTER_ID)
os.makedirs(IMAGES_PATH, exist_ok=True)

def save_fig(fig_id, tight_layout=True, fig_extension="png", resolution=300):
    path = os.path.join(IMAGES_PATH, fig_id + "." + fig_extension)
    print("Saving figure", fig_id)
    if tight_layout:
        plt.tight_layout()
    plt.savefig(path, format=fig_extension, dpi=resolution)


# In[1062]:


T=True
F=False
import pandas as pd
from itertools import combinations
from operator import itemgetter
from sklearn.base import clone,is_classifier,BaseEstimator,ClassifierMixin
import matplotlib.pyplot as plt
import itertools
from sklearn.ensemble import BaggingClassifier
from sklearn.model_selection import cross_val_score,cross_val_predict,cross_validate,check_cv,StratifiedKFold
from sklearn.metrics import confusion_matrix,classification_report
from sklearn.model_selection import train_test_split
import pandas as pd
#from sklearn.utils.validation import check_is_fitted
import copy
from matplotlib import colors
from matplotlib.colors import ListedColormap
import matplotlib as mpl
import random
import string
from sklearn.exceptions import NotFittedError

class FalseClassif(BaseEstimator,ClassifierMixin):
    def __init__(self,y,w=None):
        self.y=y
        self.w=w
    def fit(self,X,y=None):
        return self
                   
    def predict(self,X):
        return self.y
    
    def score(self,X,y):
        return accuracy_score(y,self.y)
    def __getattr__(self, attr):
        # proxy to the wrapped object
        if self.w is None:
            return super(FalseClassif,self).__getattribute__(attr)
        return getattr(self.w, attr)
        
def mapl(*args,**xargs):
    return list(map(*args,**xargs))
def check_is_fitted(i):
    ii=True
    try:
        i.predict([])
    except NotFittedError as e:
        ii=False
    return ii
def randomString(stringLength=10):
    """Generate a random string of fixed length """
    letters = string.ascii_lowercase
    return ''.join(random.choice(letters) for i in range(stringLength))
def mplResetDefault():
    mpl.rcParams.update(mpl.rcParamsDefault)
ddl_heat = ['#DBDBDB','#DCD5CC','#DCCEBE','#DDC8AF','#DEC2A0','#DEBB91',            '#DFB583','#DFAE74','#E0A865','#E1A256','#E19B48','#E29539']
ddlheatmap = colors.ListedColormap(ddl_heat)
class placeholderFn:
    def __init__(self,fn):
        self.fn=fn
    def __eq__(self,other):
        return placeholderFn(lambda selfo: self(selfo)==other)
    def __ne__(self,other):
        return placeholderFn(lambda selfo: self(selfo)!=other)
    def __lt__(self,other):
        return placeholderFn(lambda selfo: self(selfo)<other)
    def __gt__(self,other):
        return placeholderFn(lambda selfo: self(selfo)>other)
    def __le__(self,other):
        return placeholderFn(lambda selfo: self(selfo)<=other)
    def __ge__(self,other):
        return placeholderFn(lambda selfo: self(selfo)>=other)
    
    def __add__(self, other):
        return placeholderFn(lambda selfo: self(selfo)+other)
    def __sub__(self, other):
        return placeholderFn(lambda selfo: self(selfo)-other)
    def __mul__(self, other):
        return placeholderFn(lambda selfo: self(selfo)*other)
    def __floordiv__(self, other):
        return placeholderFn(lambda selfo: self(selfo)//other)
    def __div__(self, other):
        return placeholderFn(lambda selfo: self(selfo)/other)
    def __mod__(self, other):
        return placeholderFn(lambda selfo: self(selfo)%other)
    def __pow__(self,other):
        return placeholderFn(lambda selfo: self(selfo)**other)
    def __lshift__(self, other):
        return placeholderFn(lambda selfo: self(selfo)<<other)
    def __rshift__(self, other):
        return placeholderFn(lambda selfo: self(selfo)>>other)
    def __and__(self, other):
        return placeholderFn(lambda selfo: self(selfo)&other)
    def __or__(self, other):
        return placeholderFn(lambda selfo: self(selfo)|other)
    def __xor__(self, other):
        return placeholderFn(lambda selfo: self(selfo)^other)
    
    
    def __call__(self,selfo):
        return self.fn(selfo)
    
    @staticmethod
    def mine(i):
        return isinstance(i,placeholderFn)
class placeholder:
    def __init__(self):pass
    def __getattr__(self,a):
        return placeholderFn(lambda selfo: getattr(selfo,a))
    @staticmethod
    def mine(i):
        return isinstance(i,placeholder)
class placeholderOrMe:
    def __init__(self,me):
        self.me=me
        self.isPlaceholderFn=placeholderFn.mine(me)
    def __call__(self,selfo):
        return self.me(selfo) if self.isPlaceholderFn else self.me
__=placeholder()
___=placeholderOrMe
from yellowbrick.classifier import ClassificationReport,ConfusionMatrix,ROCAUC,PrecisionRecallCurve,ClassPredictionError


        
class studyClassif_viz:
    PrecisionRecallCurve=PrecisionRecallCurve
    ClassPredictionError=ClassPredictionError
    ClassificationReport=ClassificationReport
    ConfusionMatrix=ConfusionMatrix
    ROCAUC=ROCAUC

def visualize_model(X=None, y=None, Xt=None, yt=None, estimator=None,fit=True,names=None,fn=ClassificationReport,
                    cmap="YlGn",size=(600, 360),title="",**kwargs):
    """
    Test various estimators.
    """
    model = estimator

    # Instantiate the classification model and visualizer
    visualizer = fn(
        model, classes=names,
        cmap=cmap, size=size,title=title,is_fitted=not fit, **kwargs
    )
    if fit: visualizer.fit(X, y)
    visualizer.score(Xt, yt)
    visualizer.show()
    return visualizer

import re
def getPrivateAttr(obj,private_begin="__",private_end="[^_]",private_suffixe=""):
    dd=re.compile("^"+private_begin+private_suffixe+".+"+private_end+"$")
    return {j:getattr(obj,str(j)) for j in [i for i in dir(obj) if dd.match(i)]}

def new_models(models):
    return [(i,clone(k)) for i,k in models]
def diff_classif_models(models,X_test=None,y_test=None,names=None):
    names=[ i.__class__.__name__ for i in models] if names is None else  names
    kkd=[ y_test!=i.predict(X_test) for i in models]
    combi=list(combinations(range(len(names)),2))
    o=np.zeros((len(names),len(names)))
    o2=np.zeros((len(names),len(names)))
    o3=np.full((len(names),len(names),np.shape(X_test)[0]),-1,dtype="O")
    for (i,j) in combi:
        o[i][j]=(kkd[i]*kkd[j]).sum()
        o2[i][j]=o[i][j]/(kkd[i] | kkd[j]).sum()
        if((o3[i][i]==-1).all()): o3[i][i]=kkd[i]
        if((o3[j][j]==-1).all()): o3[j][j]=kkd[j]
        o3[i][j]=kkd[i]*kkd[j]
        o3[j][i]=kkd[i]*kkd[j]
    return [pd.DataFrame(o,columns=names,index=names),pd.DataFrame(o2,columns=names,index=names),o3,kkd]

class case(object):
    def __init__(self,j,i,k):
        self.i=i
        self.j=j
        self.k=k
    def __repr__(self):
        return "'[Case] ligne {}, colonne {}, valeur {}'".format(self.i,self.j,self.k)
    def __str__(self):
        return self.__repr__()
    @classmethod
    def fromArr(cls,arr):
        return [cls(i,j,k) for i,j,k in arr]
    
class caseConfusionMat(case):
    def __init__(self,i,j,k,names=None):
        self.i=i
        self.j=j
        self.k=k
        self.ni=i if names is None else names[i]
        self.nj=j if names is None else names[j]
    def __repr__(self):
        return "[CaseConfMat] Il y a {} observations de classe '{}' predite en classe '{}'".format(self.k,self.ni,self.nj)
    @classmethod
    def fromArr(cls,arr,names=None):
        return [cls(i,j,k,names) for i,j,k in arr]
    
class caseConfusionGlobal(case):
    def __init__(self,ie,names=None,typeG=0):
        i=dict(ie)
        self.typeG=typeG
        self.i=i.items()
        self.ni=list(i.keys()) if names is None else names[i.keys()]
    def eachR(self,ni,k):
        typeG=self.typeG
        st=""
        if typeG==0:
            st="[CaseConfGlobal] Classe Actuelle '{}': '{}' mauvaise predictions (prediction de ≠ classes pour cette classe)"
        else:
            st="[CaseConfGlobal] Prediction Classe '{}': '{}' mauvaise predictions (prediction pour d'autres classes)"
        return  st.format(ni,k)
    def __repr__(self,typeG=0):
        return "\n\t".join([self.eachR(self.ni[_i],k) for _i,(i,k) in enumerate(self.i)])
    @classmethod
    def fromArr(cls,arr,names=None):
        return [cls(i,names,_i) for _i,i in enumerate(arr)]
    
def most_confused(pred,true,min_val=1,classes=None,lim=100,shuffle=None):
        "Sorted descending list of largest non-diagonal entries of confusion matrix, presented as actual, predicted, number of occurrences."
        classes=np.unique(np.concatenate([pred,true]))
        cm = confusion_matrix(true,pred)
        np.fill_diagonal(cm, 0)
        res = [(classes[i],classes[j],cm[i,j])
                for i,j in zip(*np.where(cm>=min_val))]
        return (sorted(res, key=itemgetter(2), reverse=True)[:lim],len(res)-lim if len(res)>lim else False)
def getObsMostConfused(classe,predit,X,y,pred,lim=10):
    return np.array(X)[(y==classe) & (predit==pred)][:lim]
def most_confused_global(pred,true,classes=None,lim=100):
    classes=np.unique(np.concatenate([pred,true]))
    cm = confusion_matrix(true,pred)
    np.fill_diagonal(cm, 0)
    lignes=sorted(list(enumerate(np.sum(cm,axis=1))),key=itemgetter(1), reverse=True)[:lim]
    cols=sorted(list(enumerate(np.sum(cm,axis=0))),key=itemgetter(1), reverse=True)[:lim]
    return ([lignes,cols],len(lignes)-lim if len(cols)>lim else False)
def plot_confusion_matrix2(true,pred,classes=None,normalize:bool=False, title:str='Confusion matrix', cmap="Blues", slice_size=1,
                              norm_dec:int=2, plot_txt:bool=True, return_fig:bool=None,normalizeByRows=False,
                              diagZero=False,colorbar=False,roundVal=None,col1="white",col2="black",**kwargs):
        "Plot the confusion matrix, with `title` and using `cmap`."
        #row_sums = conf_mx.sum(axis=1, keepdims=True)
        # This function is mainly copied from the sklearn docs
        if classes is None:
            classes = np.unique(np.concatenate([pred,true]))
        cm = confusion_matrix(true,pred)
        if normalize or normalizeByRows: cm = cm/cm.sum(axis=1, keepdims=True)
        if diagZero: np.fill_diagonal(cm,0)
        #if normalize or normalizeByRows: cm = (cm.astype('float') / cm.sum(axis=1))[:, np.newaxis]
        if roundVal is not None: cm=np.round(cm,roundVal)
        fig = plt.figure(**kwargs)
        plt.imshow(cm, interpolation='nearest', cmap=cmap)
        if colorbar: plt.colorbar()
        plt.title(title)
        tick_marks = np.arange(len(classes))
        plt.xticks(tick_marks, classes, rotation=0)
        plt.yticks(tick_marks, classes, rotation=0)

        if plot_txt:
            thresh = cm.max() / 2.
            for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
                coeff = "{:0.0f}%".format(cm[i, j]*100.0) if normalize or normalizeByRows else f'{cm[i, j]}'
                #value = cm_display[x, y]
                #svalue = "{:0.0f}".format(value)
                plt.text(j, i, coeff, horizontalalignment="center", verticalalignment="center", color=col1 if cm[i, j] > thresh else col2)
        
        plt.tight_layout()
        plt.ylabel('Actuelle')
        plt.xlabel('Predite')
        plt.grid(False)
        fig
def plot_confusion_matrix3(y,X,j,classes=None,normalize:bool=False, title:str='Confusion matrix', cmap="Blues", slice_size=1,
                              norm_dec:int=2, plot_txt:bool=True, return_fig:bool=None,cv=3,n_jobs=-1, **kwargs):
    plot_confusion_matrix2(y,cross_val_predict(j,X,y,cv=cv,n_jobs=n_jobs),classes=classes,title=title,cmap=cmap,
                           slice_size=1,normalize=normalize,norm_dec=norm_dec,plot_txt=plot_txt,return_fig=return_fig)
def _cross_val_score2(est,train,test,x_test,y_test,name):
    s=[i.score(x_test, y_test) for i in est]
    score2=list(zip(train,test,s))
    df=np.round(pd.DataFrame(score2,columns=["train","validation","test"]),2)
    df.index.name="CV"
    df=pd.concat([df,df.apply(np.mean,axis=0).rename('mean').to_frame().T],axis=0)
    df=pd.concat([df,df.apply(np.std,axis=0).rename('std').to_frame().T],axis=0)
    return (est,(name+"\n"+np.round(df,2).to_string()).replace("\n","\n\t"))

def computeCV(X=None,y=None,cv=5,classifier=True,random_state=42,shuffle=True):
    if isinstance(cv,check_cv2): cv=cv.splited
    else: cv = check_cv2(X,y,cv,classifier=classifier,random_state=random_state,shuffle=shuffle).splited
    return cv
                    
def cross_val_score2(es,X=None,y=None,x_test=None,y_test=None,cv=5,verbose=False,names=None,predict=False):
    if isinstance(cv,check_cv2): cv=cv.splited
    names=uniquify([i.__class__.__name__ for i in es]) if names is None else names
    es=np.array(es, dtype=object).flatten()
    if(cv==1):
        k=[i.fit(X,y) for i in es]
        d=[_cross_val_score2([i],[i.score(X,y)],[0],x_test,y_test,names[j]) for j,i in enumerate(es)]
        print("\n\n".join([i[1] for i in d]))
        return [i[0] for i in d]
    cvS=[cross_validate(i,X,y,n_jobs=-1,cv=cv,return_estimator=True,return_train_score=True,verbose=verbose) for i in es]
    d=[_cross_val_score2(i["estimator"],i["train_score"],i["test_score"],x_test,y_test,names[j]) for j,i in enumerate(cvS)]
    print("\n\n".join([i[1] for i in d]))
    return [i[0] for i in d]
class check_cv2:
    def __init__(self,X,y,cv_=3,classifier=True,random_state=42,shuffle=True):
        self.cv_=cv_
        self.y=y
        self.X=X
        self.classifier=classifier
        self.random_state=random_state
        self.shuffle=shuffle
        self.splited=self.split()
    def split(self,*args):
        e=check_cv(self.cv_,self.y,classifier=self.classifier)
        e.shuffle=self.shuffle
        e.random_state=self.random_state
        return list(e.split(self.X,self.y))
def removeTrimEmptyStr(s):
        return [i.strip() for i in s if len(i)>0]
def plot_classification_report(cr, title=None, cmap=ddlheatmap,figsize=(6,5)):
    title = title or 'Classification report'
    lines = cr.split('\n')
    classes = []
    matrix = []
    for line in lines[2:(len(lines)-1)]:
        s = removeTrimEmptyStr(line.split("    "))
        if len(s)==0: continue
        classes.append(s[0])
        value = [float(x) for x in s[1: len(s) - 1]]
        matrix.append(value)
    fig, ax = plt.subplots(1,figsize=figsize)
    for column in range(len(matrix[0])):
        for row in range(len(classes)):
            txt = matrix[row][column]
            ax.text(column,row,matrix[row][column],va='center',ha='center')
    fig = plt.imshow(matrix, interpolation='nearest', cmap=cmap)
    plt.title(title)
    plt.colorbar()
    x_tick_marks = np.arange(len(matrix[0]))
    y_tick_marks = np.arange(len(classes))
    plt.xticks(x_tick_marks, ['precision', 'recall', 'f1-score'], rotation=45)
    plt.yticks(y_tick_marks, classes)
    plt.ylabel('Classes')
    plt.xlabel('Measures')
    plt.show()
                           
def cross_fn_cv(fn,cv,X=None,y=None):
    if isinstance(cv,check_cv2):
        if X is None:
            X = cv.X
        if y is None:
            y = cv.y

        vc=cv.split()
    else:
        assert X is not None 
        assert Y is not None
        vc=cv
    return [fn(X[i],y[i],X[j],y[j]) for (i,j) in vc] 
from collections import Counter # Counter counts the number of occurrences of each item
from itertools import tee, count

def uniquify(seq2, suffs = count(1)):
    """Make all the items unique by adding a suffix (1, 2, etc).

    `seq` is mutable sequence of strings.
    `suffs` is an optional alternative suffix iterable.
    """
    seq=seq2
    not_unique = [k for k,v in Counter(seq).items() if v>1] # so we have: ['name', 'zip']
    # suffix generator dict - e.g., {'name': <my_gen>, 'zip': <my_gen>}
    suff_gens = dict(zip(not_unique, tee(suffs, len(not_unique))))  
    for idx,s in enumerate(seq):
        try:
            suffix = str(next(suff_gens[s]))
        except KeyError:
            # s was unique
            continue
        else:
            seq[idx] += suffix
    return seq
def zipl(*args):
    return list(zip(*args))
def filterl(*args):
    return list(filter(*args))
def rangel(*args):
    return list(range(*args))
def ifEmptySet(s,li):
    return s if len(li)==0 else li
def remove_empty_keys(d):
    for k in list(d.keys()):
        if not d[k]:
            del d[k]
    return d
import warnings
def offWarnings():
    warnings.filterwarnings('ignore')
def onWarnings(d="default"):
    warnings.filterwarnings('default')
def removeNone(arr):
    return [i for i in arr if i is not None]
class modErr:
    def __init__(self,arr,names,noEmpty=False):
        self.arr=arr
        self.names=names
        self.noEmpty=noEmpty
    def printM(self,arr,m):
        if self.noEmpty and len(arr)==0: return None
        a=str(m)
        a+="\t"+np.array2string(arr,separator=", ")
        return a
    def __repr__(self):
        return "\n".join(removeNone([self.printM(np.where(j)[0],self.names[i]) for i,j in enumerate(self.arr)]))

class loadFnIfCallOrSubset:
    def __init__(self,selfo,fn):
        self.fn=fn
        self.selfo=selfo
    def __getitem__(self,i):
        self.selfo=self.fn()
        return self.selfo[i]
class tg:
    def __init__(self,i):
        self.i=i
    def __repr__(self):
        return ""
class mostConfGlobL:
    def __init__(self,d,lignes=True):
        self.lignes=lignes
        self.d=d
    def __repr__(self):
        n="Actuelle" if self.lignes else "Predite"
        oo=self.d.items()
        jj=[]
def printArr(arr,noPrint=False):
    stre="\n".join([ "\t"+str(i) for i in arr])
    if not noPrint: print(stre)
    return arr if not noPrint else stre
def printMostConfGlob(arr,noPrint=False):
    ne=["ACTUELLE","PRÉDITE"]
    stre="\n".join([ "\n\t"+ne[_i]+"\n\t"+str(i) for _i,i in enumerate(arr)])
    if not noPrint: print(stre)
    return arr if not noPrint else stre
    
def printDico(dico,fn=lambda a,*args,**xargs:a,moreAdd=None,more="\n(+ {})"):
    moreAdd = [False]*len(dico.keys()) if moreAdd is None else  moreAdd
    oo=[ i+"\n"+fn(j,noPrint=True) for i,j in dico.items()]
    oo=[i+more.format(moreAdd[_i]) if moreAdd[_i] else i for _i,i in enumerate(oo)]
    print("\n\n".join(oo))
    return dico
def isInt(i):
    return isinstance(i,int)
def isStr(i):
    return isinstance(i,str)
def isNumpyArr(i):
    return isinstance(i,np.ndarray)
def ifOkDoFn(i,ok,fn):
    return fn(i) if ok else i            
def flatArray(arr):
    return np.array([arr]).flatten()
def getClassName(i):
    return i.__class__.__name__
class studyNpArray(np.ndarray):
    def __new__(cls, array, **kwargs):
        obj = np.asarray(array).view(cls)                                 
        for i,j in kwargs.items():
            obj.__setattr__(i,j)
        return obj

    def __array_finalize__(self, obj):
        if obj is None: return
        kwargs=getPrivateAttr(obj,private_suffixe="study")
        for i,j in kwargs.items():
            self.__setattr__(i,j)
                           
def studyDico(dico,**args):
    dico=StudyClassifDict(dico)
    for i,j in args.items():
        dico.__setattr__(i,j)
    return dico
def studyList(dico,**args):
    dico=StudyClassifList(dico)
    for i,j in args.items():
        dico.__setattr__(i,j)
    return dico
from collections import UserDict,UserList
class StudyClassifDict(UserDict):
    def __getitem__(self, key):
        key=list(self.keys())[key] if isInt(key) else key
        if key in self.data:
            rep = self.data[key]
            atty=getPrivateAttr(self,private_suffixe="study")
            if isinstance(rep,list):
                return studyList(rep,**atty)
            elif isNumpyArr(rep):
                rep=studyNpArray(rep,**atty)
                return rep
            else:
                return rep
        if hasattr(self.__class__, "__missing__"):
            return self.__class__.__missing__(self, key)
        raise KeyError(key)
from sklearn.metrics import accuracy_score
class StudyClassifList(UserList): pass
class StudyClassif:
    def __init__(self,models=None,X_test=None,y_test=None,X_train=None,y_train=None,namesCls=None,metric=accuracy_score):
        #offWarnings()
        self._cv={}
        self.metric=metric
        self._nameCV=None
        if X_test is not None and y_test is not None: self.setDataTest(X_test,y_test)
        if X_train is not None and y_train is not None: self.setDataTrain(X_train,y_train)
        if namesCls is not None: self.setNamesCls(namesCls)
        if models is not None: self._init(models)
                           
    def _init(self,models):
        self._models=flatArray([models])
        self._names=np.array(uniquify([getClassName(i) for i in self._models]))
        self._inames=np.array(rangel(len(self._models)))
        self._namesi=dict(zip(self._names,self._inames))
        self._diff=None
        self._diff=loadFnIfCallOrSubset(self._diff,lambda:self.diff_classif_models())
    
    def setDataXY(self,X,y,test_size=0.2,shuffle=True,random_state=42):
        X=___(X)(self)
        y=___(y)(self)
        X_train,X_test,y_train,y_test=train_test_split(X,y,test_size=test_size,shuffle=shuffle,random_state=random_state)
        self.setDataX(X_train,X_test)
        self.setDataY(y_train,y_test)
                           
    def setDataTrainTest(self,X_train,X_test,y_train,y_test,names=None):
        X_train=___(X_train)(self)
        y_train=___(y_train)(self)
        X_test=___(X_test)(self)
        y_test=___(y_test)(self)
        self.setDataX(X_train,X_test)
        self.setDataY(y_train,y_test,names=names)
                           
    def setDataY(self,y_train,y_test,names=None):
        self._y_train=___(y_train)(self)
        self._y_test=___(y_test)(self)
        if names is not None: 
            self.setNamesCls(names)
    def setDataTrain(self,X_train,y_train,names=None):
        self._X_train=___(X_train)(self)
        self._y_train=___(y_train)(self)
        if names is not None: 
            self.setNamesCls(names)
    def setDataTest(self,X_test,y_test,names=None):
        self._X_test=___(X_test)(self)
        self._y_test=___(y_test)(self)
        if names is not None: 
            self.setNamesCls(names)                    
    def setDataX(self,X_train,X_test):
        self._X_train=___(X_train)(self)
        self._X_test=___(X_test)(self)

    def setNamesCls(self,names):
        self._namesCls=names
    
    def setModels(self,models):
        self._init(models)
                           
    def getIndexFromNames(self,arr,returnOK=True):
        return [self.namesi[i] if isStr(i) else i for i in arr] if returnOK else None
                           
    def diff_classif_models(self,returnOK=True):
        return diff_classif_models(self.models,self.X_test,self.y_test,self.names) if returnOK else None
                           
    def most_confused(self,min_val=1,noEmpty=False,classNames=True,lim=100,shuffle=False,
                     globally=False,returnOK=False):
        if not globally:
            offWarnings()
            res= {i+" (N°"+str(o)+")" :most_confused(self.y_test,j.predict(self.X_test),min_val=min_val,lim=lim,shuffle=shuffle)  for o,(i,j) in enumerate(zipl(self.names,self.models))}
            moreS=[j[1] for i,j in res.items()]
            res={i:caseConfusionMat.fromArr(j[0],self.namesCls if classNames else None ) for i,j in res.items()}
            onWarnings()
            res=remove_empty_keys(res) if noEmpty else res
            o= printDico(res,fn=printArr,moreAdd=moreS)
        else:
            allPred=[[self.y_test,j.predict(self.X_test)]  for o,(i,j) in enumerate(zipl(self.names,self.models))]
            allPredN=np.hstack(allPred)
            res=most_confused(allPredN[0],allPredN[1],min_val=min_val,lim=lim,shuffle=shuffle) 
            moreS=[res[1]]
            res={"globally":caseConfusionMat.fromArr(res[0],self.namesCls if classNames else None )}
            res=remove_empty_keys(res) if noEmpty else res
            o= printDico(res,fn=printArr,moreAdd=moreS)
        return o if returnOK else None 
                           
    def plot_conf_matrix(self,m=[],returnOK=False,test=False,normalizeByRows=False,
                        cmap="Blues",diagZero=False,colorbar=False,roundVal=None,errors=False,**xargs):
        
        if errors:
            yellowBrick=F
            normalizeByRows=True
            diagZero=True,
            colorbar=T
            roundVal=2
            cmap=plt.cm.gray
            col1="black"
            col2="white"
        if False and yellowBrick:
            if diagZero:
                def oop(selfo):
                    selfo.cmap.set_under(color="k")
                    print(selfo.cmap)
                    np.fill_diagonal(selfo.confusion_matrix_,0.0001)
                def oop2(selfo):
                    global CMAP_UNDERCOLOR_
                    selfo.cmap.set_under(color=CMAP_UNDERCOLOR_)
                fnDraw=oop
                fnFinalize=oop2
                           
            a=[self.visualize_model(FalseClassif(i,self.getCV()[1]["cv"]["cv_validate"][0]["estimator"][0]),False,
                                  studyClassif_viz.ConfusionMatrix,title=k,percent=normalizeByRows,cmap=cmap,
                                   fnFinalize=fnFinalize,fnDraw=fnDraw) for i,k in zipl(self.getCV()[1]["preds"]["Val"]["sorted"],self.names)]
            return a if returnOK else None
        mm=flatArray(m)
        mm=self.getIndexFromNames(mm)
        if not test: zizi=zipl(self.names if len(mm)==0 else self.names[mm],
                               self.getCV()[1]["preds"]["Val"]["sorted"] if len(mm)==0 else np.array(self.getCV()[1]["preds"]["Val"]["sorted"])[mm])
        else: zizi=zipl(self.names[mm],self.models[mm]) if len(mm)>0 else zipl(self.names,self.models)
        o= [ plot_confusion_matrix2(self.y_test,j.predict(self.X_test),title=i,classes=self.namesCls,
                                    normalizeByRows=normalizeByRows,cmap=cmap,diagZero=diagZero,colorbar=colorbar,
                                    roundVal=roundVal,col1=col1,col2=col2,**xargs) 
            for (i,j) in zizi] if test else [ plot_confusion_matrix2(self.y_train,j,title=i,classes=self.namesCls,
                                                                     normalizeByRows=normalizeByRows,cmap=cmap,
                                                                    diagZero=diagZero,colorbar=colorbar,roundVal=roundVal,col1=col1,col2=col2,**xargs)  
            for (i,j) in zizi]
        return o if returnOK else None  
                           
    def getModelsErrors(self,m=[],noEmpty=False,returnOK=False):
        o3=self.diff[2]
        mm=flatArray(m)
        mm=self.getIndexFromNames(mm)
        o33=[o3[i][i] for i in range(np.shape(o3)[0])]
        o= modErr(o33 if len(mm) == 0 else np.array(o33)[mm] ,self.names if len(mm) == 0 else self.names[mm],noEmpty=noEmpty)
        return o if returnOK else None
    def getModelsSameErrors(self,m=[],pct=False,noEmpty=False,returnOK=True):
        o3=self.diff[0]
        mm=flatArray(m)
        mm=self.getIndexFromNames(mm)
        if pct:
            o3 = self.diff[1]
        o3=o3 if len(mm)==0 else o3.iloc[mm,mm]
        return o3 if returnOK else None
                           
    def cross_val_score2(self,m=[],cv=3,verbose=0,returnNewStudy=False,returnOK=False,predict=False):
        X_train=self.X_train
        y_train=self.y_train
        mm=flatArray(m)
        mm=self.getIndexFromNames(mm)
        cv = cross_val_score2(self.models if len(mm)==0 else self.models[mm],
                              X_train,y_train,
                              self.X_test,self.y_test,
                              cv=cv,verbose=verbose,
                              names=self.names if len(mm)==0 else self.names[mm],
                             predict=predict)
        if returnNewStudy: 
            return (cv,self.updateModels(cv)) if  returnOK else self.updateModels(cv)
        else : 
            return cv if returnOK else None
                           
    def updateModels(self,models):
        me=copy.deepcopy(self)
        me._init(models)
        return me
    
    def classification_report(self,returnOK=False,printOK=True,test=None,**xargs):
        predict=test
        predict = (not np.all([check_is_fitted(i) for i in self.models])) if predict is None else predict
        if not predict: #fitted
            od= {i:classification_report(self.y_test,j.predict(self.X_test),target_names=self.namesCls) 
                 for (i,j) in zipl(self.names,self.models)}
        else: #not fitted
            od= {i:classification_report(self.y_train,j,target_names=self.namesCls) 
                 for (i,j) in zipl(self.names,self.getCV()[1]["preds"]["Val"]["sorted"])}
        o=od if not printOK else printDico(od)
        if False and train:
            od= {i:classification_report(self.y_train,j,target_names=self.namesCls) 
                 for (i,j) in zipl(self.names,self.getCV()[1]["preds"]["Tr"]["sorted"])}
            o=[o,od if not printOK else printDico(od)]
        return o if returnOK else None
                           
    def classification_report2(self,returnOK=False,printOK=True,predict=None,cv=3,**xargs):
        predict = (not np.all([check_is_fitted(i) for i in self.models])) if predict is None else predict
        if globally:
            allPredN=np.hstack([[self.y_test,j.predict(self.X_test)]  
                                for (i,j) in zipl(self.names,self.models)])  if not predict else np.hstack([[self.y_train,cross_val_predict(j,self.X_train,self.y_train,cv=cv)]  
                                for (i,j) in zipl(self.names,self.models)])
            od={"globally":classification_report(allPredN[0],allPredN[1],target_names=self.namesCls)}
            o=od if not printOK else printDico(od)
        else:
            od={i:classification_report(self.y_test,j.predict(self.X_test),target_names=self.namesCls) for (i,j) in zipl(self.names,self.models)} if not predict else {i:classification_report(self.y_train,cross_val_predict(j,self.X_train,self.y_test,cv=cv),target_names=self.namesCls) for (i,j) in zipl(self.names,self.models)}
            o=od if not printOK else printDico(od)
        return o if returnOK else None

    def plot_classification_report(self,title=None,returnOK=False,test=None,figsize=(6,5),yellowBrick=False,**xargs):
        if yellowBrick:            
            [self.visualize_model(FalseClassif(i,self.getCV()[1]["cv"]["cv_validate"][0]["estimator"][0]),False,studyClassif_viz.ClassificationReport,title=k) for i,k in zipl(self.getCV()[1]["preds"]["Val"]["sorted"],self.names)]
            return
        od=self.classification_report(returnOK=True,printOK=False,test=test)
        oo=od
        o=[plot_classification_report(j,i,figsize=figsize) for i,j in oo.items()]                
        return o if returnOK else None
    def plot_roc_auc(self,title=None):          
        [self.visualize_model(FalseClassif(i,self.getCV()[1]["cv"]["cv_validate"][0]["estimator"][0]),False,
                              studyClassif_viz.ROCAUC,title=k, micro=False, macro=False, per_class=False) 
                             for i,k in zipl(self.getCV()[1]["preds"]["Val"]["sorted"],self.names)]
        return
    def plot_precision_recall_curves(self,title=None):          
        [self.visualize_model(FalseClassif(i,self.getCV()[1]["cv"]["cv_validate"][0]["estimator"][0]),True,
                              studyClassif_viz.PrecisionRecallCurve,title=k, micro=False, macro=False, per_class=False) 
                             for i,k in zipl(self.getCV()[1]["preds"]["Val"]["sorted"],self.names)]
        return
    def plot_classes_pred_erreurs(self,title=None):          
        [self.visualize_model(FalseClassif(i,self.getCV()[1]["cv"]["cv_validate"][0]["estimator"][0]),False,
                              studyClassif_viz.ClassPredictionError,title=k, micro=False, macro=False, per_class=False) 
                             for i,k in zipl(self.getCV()[1]["preds"]["Val"]["sorted"],self.names)]
        return
    def most_confused_global(self,lim=10,classNames=True,returnOK=False):
        res= {i+" (N°"+str(o)+")" :most_confused_global(self.y_test,j.predict(self.X_test),lim=lim)  for o,(i,j) in enumerate(zipl(self.names,self.models))}
        moreS=[j[1] for i,j in res.items()]
        res={i:caseConfusionGlobal.fromArr(j[0],self.namesCls) for i,j in res.items()}
        o=printDico(res,fn=printMostConfGlob,moreAdd=moreS)
        return o if returnOK else None
    
    def getObsConfused(self,classe,predit,m=[],lim=10,printOk=False,returnOK=True,shuffle=True):
        mm=flatArray(m)
        mm=self.getIndexFromNames(mm)
        zz=zipl(self.names,self.models) if len(mm)==0 else zipl(self.names[mm],self.models[mm])
        o=studyDico({i:getObsMostConfused(classe,predit,self.X_test,self.y_test,j.predict(self.X_test),lim=lim) for i,j in zz},
                    __study_i=classe,__study_j=predit,__study=self)
        if printOk: printDico(o,fn=printArr)
        return o if returnOK else None
                           
    def addModels(self,m=[],inplace=False):
        mm=flatArray(m)
        o=[self.models.tolist()+mm.tolist()]
        if inplace:
            self._init(o)
        else:
            return self.updateModels(o)
    def plot_classe_balance(self,test=False,both=False):
        cb=ClassBalance(labels=self._namesCls)
        if not both: cb.fit(self.y_train if not test  else self.y_test)
        else: cb.fit(self.y_train,self.y_test)
        cb.show()
        return
    def visualize_model(self,i=None,fit=True,fn=ClassificationReport,cmap="YlGn",size=(600, 360),title="",
                        noTest=True,fnFinalize=lambda *a,**ba:None,**xargs):
        ni=self._namesCls
        return visualize_model(self._X_train,self._y_train,
                               self._X_train if noTest else self.X_test,self._y_train if noTest else self.y_test,
                               i,fit,ni,fn,cmap=cmap,size=size,title=title,fnFinalize=fnFinalize,**xargs)

    def computeCV(self,cv=5,random_state=42,shuffle=True,classifier=True,
                 name=None,recreate=False,parallel=True):
        #if cv == 1 :
        #    crossV.crossV(self.models, self.X_train, self.y_train ,train, test,self.X_test,self.y_test)
        cv=computeCV(X=self._X_train,y=self._y_train,cv=cv,classifier=classifier,random_state=random_state,shuffle=shuffle)
        if name is None:
            self._nameCV = "CV_"+randomString(10) 
            cvo=self.__crossV(cv,parallel=parallel)
            self._cv[self._nameCV]=[cv,cvo]
        else:
            #delattr(self,"_nameCV")
            if (name in self._cv and recreate) or (name not in self._cv):
                cvo=self.__crossV(cv,parallel=parallel)
                self._cv[name]=[cv,cvo]
                self._nameCV=name
                           
    def __crossV(self,cv=3,verbose=0,n_jobs=-1,parallel=True):
        cvv=cv
                           
        cvvTr=[i[0] for i in cvv]
        cvvVal=[i[1] for i in cvv]
                           
        cvvTrCon=np.argsort(np.concatenate(cvvTr))
        cvvValCon=np.argsort(np.concatenate(cvvVal))

        resu2=[cross_validate(mod ,self.X_train,self.y_train,return_train_score=True,
                            return_estimator=True,cv=cvv,n_jobs=n_jobs,verbose=verbose) for mod in self.models]

        preduVal=[[i.predict(self.X_train[k]) for i,k in zipl(resuI["estimator"],cvvVal) ] for resuI in resu2]
                           
        #print(np.shape(preduVal))
        #print(np.shape(cvvValCon))
        #return (cvv,cvvTr,cvvTrCon,preduVal,resu2)
        preduuVal=[np.concatenate(preduI)[cvvValCon] for preduI in preduVal]
        
        scoreVal = [resuI["test_score"] for resuI in resu2]
        
        preduTr=[[i.predict(self.X_train[k]) for i,k in zipl(resuI["estimator"],cvvTr) ] for resuI in resu2]
                           
        preduuTr=[np.concatenate(preduI)[cvvTrCon] for preduI in preduTr]
        
        scoreTr = [resuI["train_score"] for resuI in resu2]
        
        return { "preds": {
                            "Tr":{"original":preduTr,"sorted":preduuTr},
                            "Val":{"original":preduVal,"sorted":preduuVal}
                            },
                "scores": {
                            "Tr":scoreTr,
                            "Val":scoreVal
                            },
                "cv":{ 
                        "splitted":cvv,
                        "Tr":{"original":cvvTr,"argsort":cvvTrCon},
                        "Val":{"original":cvvVal,"argsort":cvvValCon},
                        "cv_validate":resu2
                        }
        }
                           
    def __crossV2(self,cv=3,verbose=0,n_jobs=-1,parallel=True):
        if parallel:
            parallel = Parallel(n_jobs=n_jobs, verbose=verbose)
            scores = parallel(
                delayed(crossV.crossV)(
                    self.models, self.X_train, self.y_train ,train, test,self.X_test,self.y_test,self.metric)
                for train, test in cv)
            o=np.column_stack(scores)
        else:
            o=np.column_stack([crossV.crossV(
                    self.models, self.X_train, self.y_train ,train, test,self.X_test,self.y_test,self.metric)
                for train, test in cv])
        cvE=[np.concatenate(i) for i in cv]
        toOne=lambda a:flatArray(np.concatenate(a))
        toOneMap=lambda b:mapl(toOne,b)
        toOneO=lambda a:[j[np.argsort(i)] for i,j in zipl(cvE,a)]
        gg=lambda a:list(np.concatenate(i) for i in a)
        dj=lambda a:zipl(*[np.reshape(i,(np.shape(i)[1],-1)) for i in a])
        #toOneO=lambda a:a
        o3=gg(dj(o[3]))
        o4=gg(dj(o[4])  )    
        return {"preds":{
                    "Tr":[o[3],o3,toOneO(o3)],
                    "Val":[o[4],o4,toOneO(o4)]
                    },
           "score":{
                    "Tr":o[5],
                    "Val":o[6]
                    },
            "data":{"Tr":o[1],
                   "Val":o[2]},
            "es":o[0],
            "cv":cvE
           }
    def __crossV2(self,cv):
        predsTrAll,predsValAll,predsTeAll=[],[],[]
        scoreTrAll,scoreValAll,scoreTeAll=[],[],[]
        TRall, TVALall = []      
        esAll=[]
        for train, test in cv:
            es=[clone(i) for i in self.models]
            #esAll.append(es)
            X_tr,y_tr =self.X_train[train],self.y_train[train]
            #TRall.append([X_tr,y_tr])
            X_val,y_val =self.X_train[tes],self.y_test[test]
            #TVALall.append([X_val,y_val])
            fitted=[i.fit(X_tr,y_tr) for i in es]
            predsTr=[i.predict(X_tr) for i in es]
            #predsTrAll.append(predsTr)
            predsVal=[i.predict(X_val) for i in es]
            #predsValAll.append(predsVal)
            predsTe=[i.predict(X_test) for i in es]
            #predsTeAll.append(predsTe)
            scoreTr=[self.metrics(y_tr,i) for i in predsTr]
            #scoreTrAll.append(scoreTr)
            scoreTVal=[self.metrics(y_val,i) for i in predsVal]
            #scoreTValAll.append(scoreTVal)
            scoreTe=[self.metrics(self.y_test,i) for i in predsTe]
            #scoreTeAll.append(scoreTe)
        return {"preds":{
                        "Tr":predsTrAll,
                        "Val":predsValAll,
                        "Te":predsTeAll
                        },
               "score":{
                        "Tr":scoreTrAll,
                        "Val":scoreValAll,
                        "Te":scoreTeAll
                        },
                "data":{"Tr":TRall,
                       "Val":TVALall},
                "es":esAll
               }
    def __getattr__(self,a):
        if hasattr(self,"_"+a): return getattr(self,"_"+a)
        else: raise KeyError()
    def getCV(self,name=None):
        if name is None:
            name=self._nameCV
        return self._cv[name]
class crossV:
    @staticmethod
    def crossV(models,X_train,y_train,train, test,X_test,y_test,metrics):
        es=[clone(i) for i in models]
        X_tr,y_tr =X_train[train],y_train[train]
        #TRall.append([X_tr,y_tr])
        X_val,y_val =X_train[test],y_train[test]
        #TVALall.append([X_val,y_val])
        _=[i.fit(X_tr,y_tr) for i in es]
        predsTr=[i.predict(X_tr) for i in es]
        #predsTrAll.append(predsTr)
        predsVal=[i.predict(X_val) for i in es]
        #predsValAll.append(predsVal)
        #predsTe=[i.predict(X_test) for i in es]
        #predsTeAll.append(predsTe)
        scoreTr=[metrics(y_tr,i) for i in predsTr]
        #scoreTrAll.append(scoreTr)
        scoreTVal=[metrics(y_val,i) for i in predsVal]
        #scoreTValAll.append(scoreTVal)
        #scoreTe=[metrics(y_test,i) for i in predsTe]   
        return [[es],[X_tr,y_tr],[X_val,y_val],[predsTr],[predsVal],
                #[predsTe],
                [scoreTr],[scoreTVal]#,[scoreTe]
               ]
class studyClassif_Img:
    @staticmethod
    def plotImgsMultiClassif(im,title="",nr=2,nc=5,figsize=(9,5),w=28,h=28,titleSize=29,m=[],reshape=False):
        mm=flatArray(m)
        mm=getattr(im,"__study").getIndexFromNames(mm)
        names=getattr(im,"__study").names
        names = names if len(mm)==0 else names[mm]
        uu=studyClassif_Img.reshapeMultiClassif(im,w,h)  if reshape else im
        uu = uu if len(mm) ==0 else np.array(uu)[mm]
        for i in range(len(uu)):
            studyClassif_Img.plotImgs(im[i],names[i]+"\n",nr,nc,figsize,w,h,titleSize,not reshape,addToTitle=T)
        
    @staticmethod
    def plotImgs(im,title="",nr=2,nc=5,figsize=(9,5),w=28,h=28,titleSize=29,reshape=True,addToTitle=False):
        title_ = "Classe {} prédit en {}".format(getattr(im,"__study_i"),getattr(im,"__study_j"))
        if (len(title)==0) or (len(title) >0 and addToTitle): title=title+title_ 
        studyClassif_Img._plotImgs(im,title,nr,nc,w,h,w,h,figsize,titleSize,reshape)
                           
    @staticmethod
    def plotDigits(im,title="",elemsByRows=10,figsize=(9,5),w=28,h=28,titleSize=29,reshape=True,addToTitle=False,lim=10):
        title_ = "Classe {} prédit en {}".format(getattr(im,"__study_i"),getattr(im,"__study_j"))
        if (len(title)==0) or (len(title) >0 and addToTitle): title=title+title_ 
        studyClassif_Img._plotImgs(im,title,elemsByRows,w,h,figsize,titleSize,reshape,lim=lim)
                           
    @staticmethod
    def _plotImgs(im,title="",nr=2,nc=5,figsize=(9,5),w=28,h=28,titleSize=29,reshape=True):
        uu=studyClassif_Img.reshape(im,w,h) if reshape else im
        plt.figure(figsize=figsize)
        for _i,i in enumerate(uu): 
            plt.subplot(nr,nc,_i+1)
            plt.imshow(i)
            plt.axis('off')
        plt.suptitle(title,size=titleSize);
    @staticmethod
    def reshapeMultiClassif(im,w=28,h=28):
        return [studyClassif_Img.reshape(j,w,h) for ww,j in im.items()]
    
    @staticmethod
    def reshape(im,w=28,h=28):
        return [i.reshape(w,h) for i in im]
    
    @staticmethod
    def _plotDigits(instances,title="", elemsByRows=10,w=28,h=28,figsize=(9,5),titleSize=29,reshape=True,lim=10, **options):
        instances=instances[:lim]
        images_per_row=elemsByRows
        images_per_row = min(len(instances), images_per_row)
        images = instance if not reshape else [instance.reshape(w,h) for instance in instances]
        n_rows = (len(instances) - 1) // images_per_row + 1
        row_images = []
        n_empty = n_rows * images_per_row - len(instances)
        images.append(np.zeros((w, h * n_empty)))
        for row in range(n_rows):
            rimages = images[row * images_per_row : (row + 1) * images_per_row]
            row_images.append(np.concatenate(rimages, axis=1))
        image = np.concatenate(row_images, axis=0)
        plt.figure(figsize=figsize)
        plt.imshow(image, cmap = mpl.cm.binary, **options)
        plt.title(title,size=titleSize)
        plt.axis("off")


# In[ ]:





# In[3]:


from sklearn.utils._joblib import Parallel, delayed


# In[4]:


from sklearn.tree import DecisionTreeClassifier


# # MNIST

# In[5]:


from sklearn.datasets import fetch_openml


# In[6]:


mnist = fetch_openml('mnist_784', version=1,cache=True)
mnist.keys()


# In[7]:


X, y = mnist["data"], mnist["target"]
X.shape


# In[8]:


y.shape


# In[9]:


28 * 28


# In[10]:


X[:10].shape


# In[11]:


studyClassif_Img._plotDigits(X,lim=50)


# In[43]:


y[0]


# In[34]:


y = y.astype(np.uint8)


# In[45]:


def plot_digit(data):
    image = data.reshape(28, 28)
    plt.imshow(image, cmap = mpl.cm.binary,
               interpolation="nearest")
    plt.axis("off")


# In[46]:


# EXTRA
def plot_digits(instances, images_per_row=10, **options):
    size = 28
    images_per_row = min(len(instances), images_per_row)
    images = [instance.reshape(size,size) for instance in instances]
    n_rows = (len(instances) - 1) // images_per_row + 1
    row_images = []
    n_empty = n_rows * images_per_row - len(instances)
    images.append(np.zeros((size, size * n_empty)))
    for row in range(n_rows):
        rimages = images[row * images_per_row : (row + 1) * images_per_row]
        row_images.append(np.concatenate(rimages, axis=1))
    image = np.concatenate(row_images, axis=0)
    plt.imshow(image, cmap = mpl.cm.binary, **options)
    plt.axis("off")


# In[47]:


plt.figure(figsize=(9,9))
example_images = X[:100]
plot_digits(example_images, images_per_row=10)
save_fig("more_digits_plot")
plt.show()


# In[48]:


y[0]


# In[418]:


#setData test_size=0.2, train_size=0.8, shuffle=True, random_state=42
#studyMnist.setData(X,y)


# In[35]:


X_train, X_test, y_train, y_test = X[:60000], X[60000:], y[:60000], y[60000:]


# # Binary classifier

# ## Orig

# In[52]:


y_train_5 = (y_train == 5)
y_test_5 = (y_test == 5)


# **Note**: some hyperparameters will have a different defaut value in future versions of Scikit-Learn, such as `max_iter` and `tol`. To be future-proof, we explicitly set these hyperparameters to their future default values. For simplicity, this is not shown in the book.

# In[53]:


from sklearn.linear_model import SGDClassifier

sgd_clf = SGDClassifier(max_iter=500, tol=1e-3, random_state=42,n_jobs=-1)
sgd_clf.fit(X_train, y_train_5)


# In[54]:


sgd_clf.predict([some_digit])


# In[62]:


from sklearn.model_selection import cross_val_score
cross_val_score(sgd_clf, X_train, y_train_5, cv=3,n_jobs=-1)


# In[63]:


from sklearn.model_selection import StratifiedKFold
from sklearn.base import clone

skfolds = StratifiedKFold(n_splits=3, random_state=42)

for train_index, test_index in skfolds.split(X_train, y_train_5):
    clone_clf = clone(sgd_clf)
    X_train_folds = X_train[train_index]
    y_train_folds = y_train_5[train_index]
    X_test_fold = X_train[test_index]
    y_test_fold = y_train_5[test_index]

    clone_clf.fit(X_train_folds, y_train_folds)
    y_pred = clone_clf.predict(X_test_fold)
    n_correct = sum(y_pred == y_test_fold)
    print(n_correct / len(y_pred))


# In[226]:


from sklearn.base import BaseEstimator
class Never5Classifier(BaseEstimator):
    def __init__(self,i=0):
        self.i=i
    def fit(self, X, y=None):
        pass
    def predict(self, X):
        return np.full((len(X), 1),self.i, dtype=bool)


# In[23]:


never_5_clf = Never5Classifier()
cross_val_score(never_5_clf, X_train, y_train_5, cv=3, scoring="accuracy",n_jobs=-1)


# In[281]:


get_ipython().run_line_magic('pinfo2', 'cross_val_predict')


# In[277]:


from sklearn.model_selection import cross_val_predict

y_train_pred = cross_val_predict(sgd_clf, X_train, y_train_5, cv=2,n_jobs=-1)


# In[280]:


y_train_pred.shape


# In[25]:


from sklearn.metrics import confusion_matrix

confusion_matrix(y_train_5, y_train_pred)


# In[26]:


y_train_perfect_predictions = y_train_5  # pretend we reached perfection
confusion_matrix(y_train_5, y_train_perfect_predictions)


# In[27]:


from sklearn.metrics import precision_score, recall_score

precision_score(y_train_5, y_train_pred)


# In[28]:


4096 / (4096 + 1522)


# In[29]:


recall_score(y_train_5, y_train_pred)


# In[30]:


4096 / (4096 + 1325)


# In[31]:


from sklearn.metrics import f1_score

f1_score(y_train_5, y_train_pred)


# In[32]:


4096 / (4096 + (1522 + 1325) / 2)


# In[33]:


y_scores = sgd_clf.decision_function([some_digit])
y_scores


# In[34]:


threshold = 0
y_some_digit_pred = (y_scores > threshold)


# In[35]:


y_some_digit_pred


# In[36]:


threshold = 8000
y_some_digit_pred = (y_scores > threshold)
y_some_digit_pred


# In[309]:


y_scores = cross_val_predict(sgd_clf, X_train, y_train_5, cv=3,
                             method="decision_function",n_jobs=-1)


# In[310]:


y_scores


# In[38]:


from sklearn.metrics import precision_recall_curve

precisions, recalls, thresholds = precision_recall_curve(y_train_5, y_scores)


# In[39]:


def plot_precision_recall_vs_threshold(precisions, recalls, thresholds):
    plt.plot(thresholds, precisions[:-1], "b--", label="Precision", linewidth=2)
    plt.plot(thresholds, recalls[:-1], "g-", label="Recall", linewidth=2)
    plt.legend(loc="center right", fontsize=16) # Not shown in the book
    plt.xlabel("Threshold", fontsize=16)        # Not shown
    plt.grid(True)                              # Not shown
    plt.axis([-50000, 50000, 0, 1])             # Not shown

plt.figure(figsize=(8, 4))                      # Not shown
plot_precision_recall_vs_threshold(precisions, recalls, thresholds)
plt.plot([7813, 7813], [0., 0.9], "r:")         # Not shown
plt.plot([-50000, 7813], [0.9, 0.9], "r:")      # Not shown
plt.plot([-50000, 7813], [0.4368, 0.4368], "r:")# Not shown
plt.plot([7813], [0.9], "ro")                   # Not shown
plt.plot([7813], [0.4368], "ro")                # Not shown
save_fig("precision_recall_vs_threshold_plot")  # Not shown
plt.show()


# In[40]:


(y_train_pred == (y_scores > 0)).all()


# In[41]:


def plot_precision_vs_recall(precisions, recalls):
    plt.plot(recalls, precisions, "b-", linewidth=2)
    plt.xlabel("Recall", fontsize=16)
    plt.ylabel("Precision", fontsize=16)
    plt.axis([0, 1, 0, 1])
    plt.grid(True)

plt.figure(figsize=(8, 6))
plot_precision_vs_recall(precisions, recalls)
plt.plot([0.4368, 0.4368], [0., 0.9], "r:")
plt.plot([0.0, 0.4368], [0.9, 0.9], "r:")
plt.plot([0.4368], [0.9], "ro")
save_fig("precision_vs_recall_plot")
plt.show()


# In[42]:


threshold_90_precision = thresholds[np.argmax(precisions >= 0.90)]


# In[43]:


threshold_90_precision


# In[44]:


y_train_pred_90 = (y_scores >= threshold_90_precision)


# In[45]:


precision_score(y_train_5, y_train_pred_90)


# In[46]:


recall_score(y_train_5, y_train_pred_90)


# ## ME

# In[36]:


y_train_5 = (y_train == 5)
y_test_5 = (y_test == 5)


# In[584]:


from sklearn.linear_model import SGDClassifier
sgd_clf = SGDClassifier(max_iter=100, tol=1e-3, random_state=42,n_jobs=-1)
sgd_clf2 = SGDClassifier(max_iter=50, tol=1e-3, random_state=42,n_jobs=-1)


# In[1073]:


studyMnist = StudyClassif()


# In[1074]:


studyMnist.setDataTrainTest(X_train,X_test,y_train_5,y_test_5,names=["Pas5","5"])


# In[1075]:


studyMnist.setModels([sgd_clf,sgd_clf2])


# In[495]:


cvOld=studyMnist._cv
nameCVOld=studyMnist._nameCV


# In[1076]:


studyMnist._cv=cvOld
studyMnist._nameCV =nameCVOld


# In[1078]:


from yellowbrick.classifier import confusion_matrix


# In[ ]:


confusion_matrix(FalseClassif(studyMnist.))


# In[ ]:





# In[1077]:


studyMnist.plot_classe_balance(test=F,both=T)


# In[491]:


studyMnist.computeCV(cv=3,name="studyMnist_cv3",parallel=False)


# In[1067]:


studyMnist.plot_classification_report()               


# In[1068]:


studyMnist.plot_classification_report(yellowBrick=T) 


# In[650]:


#ADD IN CV -> PREDICT PROBA OR DECISION FONCTION
studyMnist.plot_roc_auc()


# In[663]:


#ADD IN CV -> PREDICT PROBA OR DECISION FONCTION
studyMnist.plot_precision_recall_curves()


# In[670]:


studyMnist.plot_classes_pred_erreurs()


# In[292]:


study_MNIST = studyClassif(sgd_clf,
                           X_test=X_test,y_test=y_test_5,
                           X_train=X_train,y_train=y_train_5,
                           names=["Pas5","5"])


# In[21]:


get_ipython().run_line_magic('load_ext', 'line_profiler')


# In[33]:


get_ipython().run_line_magic('lprun', '-f cross_val_score2 study_MNIST.cross_val_score2(X_train,y_train_5,cv=1,verbose=1)')


# In[487]:


studyMnist.visualize_model(studyMnist._models[0])


# In[490]:


studyMnist.visualize_model(studyMnist._models[0],fn=ConfusionMatrix)


# In[491]:


studyMnist.plot_classification_report(globally=T,predict=True)


# In[493]:


studyMnist.plot_conf_matrix(predict=True)


# In[331]:


study_MNIST.plot_classification_report(globally=F,predict=True)


# In[297]:


study_MNIST_SGD=study_MNIST.cross_val_score2(cv=3,returnNewStudy=True)


# In[298]:


study_MNIST_SGD.plot_classification_report(globally=T)


# In[299]:


study_MNIST_SGD.plot_classification_report(globally=F)


# In[300]:


from sklearn.base import BaseEstimator,ClassifierMixin
class Never5Classifier(BaseEstimator,ClassifierMixin):
    def __init__(self,i=0):
        self.i=i
    def fit(self, X, y=None):
        pass
    def predict(self, X):
        return np.full((len(X), 1),self.i, dtype=bool)


# In[301]:


nev_clf=Never5Classifier()


# In[302]:


study_MNIST_SGD_Never=study_MNIST.addModels(nev_clf,inplace=False)


# In[303]:


study_MNIST_SGD_Never.cross_val_score2(cv=3)


# In[304]:


study_MNIST_SGD_Never.plot_classification_report(globally=F,predict=True)


# In[345]:


study_MNIST_SGD_Never.plot_conf_matrix(predict=T)


# In[346]:


visualize_model(study_MNIST.X_train,study_MNIST.y_train,
                study_MNIST.X_test,study_MNIST.y_test,
                study_MNIST_SGD_Never.models[0],names=["Pas5","5"],fn=ConfusionMatrix,cmap="Blues")


# In[347]:


visualize_model(study_MNIST.X_train,study_MNIST.y_train,
                study_MNIST.X_test,study_MNIST.y_test,
                study_MNIST_SGD_Never.models[0],names=["Pas5","5"],fn=ClassPredictionError,cmap="Blues")


# In[349]:


from sklearn.linear_model import LogisticRegression


# In[350]:


logreg_clf = LogisticRegression()


# In[351]:


study_MNIST_Log=study_MNIST_SGD_Never.updateModels(logreg_clf)


# # ROC curves

# In[47]:


from sklearn.metrics import roc_curve

fpr, tpr, thresholds = roc_curve(y_train_5, y_scores)


# In[48]:


def plot_roc_curve(fpr, tpr, label=None):
    plt.plot(fpr, tpr, linewidth=2, label=label)
    plt.plot([0, 1], [0, 1], 'k--') # dashed diagonal
    plt.axis([0, 1, 0, 1])                                    # Not shown in the book
    plt.xlabel('False Positive Rate (Fall-Out)', fontsize=16) # Not shown
    plt.ylabel('True Positive Rate (Recall)', fontsize=16)    # Not shown
    plt.grid(True)                                            # Not shown

plt.figure(figsize=(8, 6))                         # Not shown
plot_roc_curve(fpr, tpr)
plt.plot([4.837e-3, 4.837e-3], [0., 0.4368], "r:") # Not shown
plt.plot([0.0, 4.837e-3], [0.4368, 0.4368], "r:")  # Not shown
plt.plot([4.837e-3], [0.4368], "ro")               # Not shown
save_fig("roc_curve_plot")                         # Not shown
plt.show()


# In[49]:


from sklearn.metrics import roc_auc_score

roc_auc_score(y_train_5, y_scores)


# **Note**: we set `n_estimators=100` to be future-proof since this will be the default value in Scikit-Learn 0.22.

# In[50]:


from sklearn.ensemble import RandomForestClassifier
forest_clf = RandomForestClassifier(n_estimators=100, random_state=42,n_jobs=-1)
y_probas_forest = cross_val_predict(forest_clf, X_train, y_train_5, cv=3,
                                    method="predict_proba",n_jobs=-1)


# In[51]:


y_scores_forest = y_probas_forest[:, 1] # score = proba of positive class
fpr_forest, tpr_forest, thresholds_forest = roc_curve(y_train_5,y_scores_forest)


# In[52]:


plt.figure(figsize=(8, 6))
plt.plot(fpr, tpr, "b:", linewidth=2, label="SGD")
plot_roc_curve(fpr_forest, tpr_forest, "Random Forest")
plt.plot([4.837e-3, 4.837e-3], [0., 0.4368], "r:")
plt.plot([0.0, 4.837e-3], [0.4368, 0.4368], "r:")
plt.plot([4.837e-3], [0.4368], "ro")
plt.plot([4.837e-3, 4.837e-3], [0., 0.9487], "r:")
plt.plot([4.837e-3], [0.9487], "ro")
plt.grid(True)
plt.legend(loc="lower right", fontsize=16)
save_fig("roc_curve_comparison_plot")
plt.show()


# In[53]:


roc_auc_score(y_train_5, y_scores_forest)


# In[55]:


y_train_pred_forest = cross_val_predict(forest_clf, X_train, y_train_5, cv=3,n_jobs=-1)
precision_score(y_train_5, y_train_pred_forest)


# In[56]:


recall_score(y_train_5, y_train_pred_forest)


# # Multiclass classification

# In[57]:


from sklearn.svm import SVC

svm_clf = SVC(gamma="auto", random_state=42)
svm_clf.fit(X_train[:1000], y_train[:1000]) # y_train, not y_train_5
svm_clf.predict([some_digit])


# In[58]:


some_digit_scores = svm_clf.decision_function([some_digit])
some_digit_scores


# In[59]:


np.argmax(some_digit_scores)


# In[60]:


svm_clf.classes_


# In[61]:


svm_clf.classes_[5]


# In[63]:


from sklearn.multiclass import OneVsRestClassifier
ovr_clf = OneVsRestClassifier(SVC(gamma="auto", random_state=42),n_jobs=-1)
ovr_clf.fit(X_train[:1000], y_train[:1000])
ovr_clf.predict([some_digit])


# In[64]:


len(ovr_clf.estimators_)


# In[65]:


sgd_clf.fit(X_train, y_train)
sgd_clf.predict([some_digit])


# In[ ]:


sgd_clf.decision_function([some_digit])


# In[ ]:


cross_val_score(sgd_clf, X_train, y_train, cv=3, scoring="accuracy",n_jobs=-1)


# In[ ]:


from sklearn.preprocessing import StandardScaler
scaler = StandardScaler()
X_train_scaled = scaler.fit_transform(X_train.astype(np.float64))
cross_val_score(sgd_clf, X_train_scaled, y_train, cv=3, scoring="accuracy",n_jobs=-1)


# In[ ]:


y_train_pred = cross_val_predict(sgd_clf, X_train_scaled, y_train, cv=3,n_jobs=-1)
conf_mx = confusion_matrix(y_train, y_train_pred)
conf_mx


# In[65]:


def plot_confusion_matrix(matrix):
    """If you prefer color and a colorbar"""
    fig = plt.figure(figsize=(8,8))
    ax = fig.add_subplot(111)
    cax = ax.matshow(matrix)
    fig.colorbar(cax)


# In[66]:


plt.matshow(conf_mx, cmap=plt.cm.gray)
save_fig("confusion_matrix_plot", tight_layout=False)
plt.show()


# In[67]:


row_sums = conf_mx.sum(axis=1, keepdims=True)
norm_conf_mx = conf_mx / row_sums


# In[68]:


np.fill_diagonal(norm_conf_mx, 0)
plt.matshow(norm_conf_mx, cmap=plt.cm.gray)
save_fig("confusion_matrix_errors_plot", tight_layout=False)
plt.show()


# In[69]:


cl_a, cl_b = 3, 5
X_aa = X_train[(y_train == cl_a) & (y_train_pred == cl_a)]
X_ab = X_train[(y_train == cl_a) & (y_train_pred == cl_b)]
X_ba = X_train[(y_train == cl_b) & (y_train_pred == cl_a)]
X_bb = X_train[(y_train == cl_b) & (y_train_pred == cl_b)]

plt.figure(figsize=(8,8))
plt.subplot(221); plot_digits(X_aa[:25], images_per_row=5)
plt.subplot(222); plot_digits(X_ab[:25], images_per_row=5)
plt.subplot(223); plot_digits(X_ba[:25], images_per_row=5)
plt.subplot(224); plot_digits(X_bb[:25], images_per_row=5)
save_fig("error_analysis_digits_plot")
plt.show()


# # Multilabel classification

# In[66]:


from sklearn.neighbors import KNeighborsClassifier

y_train_large = (y_train >= 7)
y_train_odd = (y_train % 2 == 1)
y_multilabel = np.c_[y_train_large, y_train_odd]

knn_clf = KNeighborsClassifier(n_jobs=-1)
knn_clf.fit(X_train, y_multilabel)


# In[67]:


knn_clf.predict([some_digit])


# **Warning**: the following cell may take a very long time (possibly hours depending on your hardware).

# In[68]:


y_train_knn_pred = cross_val_predict(knn_clf, X_train, y_multilabel, cv=3)
f1_score(y_multilabel, y_train_knn_pred, average="macro")


# # Multioutput classification

# In[69]:


noise = np.random.randint(0, 100, (len(X_train), 784))
X_train_mod = X_train + noise
noise = np.random.randint(0, 100, (len(X_test), 784))
X_test_mod = X_test + noise
y_train_mod = X_train
y_test_mod = X_test


# In[70]:


some_index = 0
plt.subplot(121); plot_digit(X_test_mod[some_index])
plt.subplot(122); plot_digit(y_test_mod[some_index])
save_fig("noisy_digit_example_plot")
plt.show()


# In[71]:


knn_clf.fit(X_train_mod, y_train_mod)
clean_digit = knn_clf.predict([X_test_mod[some_index]])
plot_digit(clean_digit)
save_fig("cleaned_digit_example_plot")


# # Extra material

# ## Dummy (ie. random) classifier

# In[72]:


from sklearn.dummy import DummyClassifier
dmy_clf = DummyClassifier()
y_probas_dmy = cross_val_predict(dmy_clf, X_train, y_train_5, cv=3, method="predict_proba")
y_scores_dmy = y_probas_dmy[:, 1]


# In[73]:


fprr, tprr, thresholdsr = roc_curve(y_train_5, y_scores_dmy)
plot_roc_curve(fprr, tprr)


# ## KNN classifier

# In[74]:


from sklearn.neighbors import KNeighborsClassifier
knn_clf = KNeighborsClassifier(weights='distance', n_neighbors=4,n_jobs=-1)
knn_clf.fit(X_train, y_train)


# In[75]:


y_knn_pred = knn_clf.predict(X_test)


# In[76]:


from sklearn.metrics import accuracy_score
accuracy_score(y_test, y_knn_pred)


# In[77]:


from scipy.ndimage.interpolation import shift
def shift_digit(digit_array, dx, dy, new=0):
    return shift(digit_array.reshape(28, 28), [dy, dx], cval=new).reshape(784)

plot_digit(shift_digit(some_digit, 5, 1, new=100))


# In[78]:


X_train_expanded = [X_train]
y_train_expanded = [y_train]
for dx, dy in ((1, 0), (-1, 0), (0, 1), (0, -1)):
    shifted_images = np.apply_along_axis(shift_digit, axis=1, arr=X_train, dx=dx, dy=dy)
    X_train_expanded.append(shifted_images)
    y_train_expanded.append(y_train)

X_train_expanded = np.concatenate(X_train_expanded)
y_train_expanded = np.concatenate(y_train_expanded)
X_train_expanded.shape, y_train_expanded.shape


# In[83]:


knn_clf.fit(X_train_expanded, y_train_expanded)


# In[84]:


y_knn_expanded_pred = knn_clf.predict(X_test)


# In[85]:


accuracy_score(y_test, y_knn_expanded_pred)


# In[86]:


ambiguous_digit = X_test[2589]
knn_clf.predict_proba([ambiguous_digit])


# In[87]:


plot_digit(ambiguous_digit)


# # Exercise solutions

# ## 1. An MNIST Classifier With Over 97% Accuracy

# **Warning**: the next cell may take hours to run, depending on your hardware.

# In[88]:


from sklearn.model_selection import GridSearchCV

param_grid = [{'weights': ["uniform", "distance"], 'n_neighbors': [3, 4, 5]}]

knn_clf = KNeighborsClassifier()
grid_search = GridSearchCV(knn_clf, param_grid, cv=5, verbose=3)
grid_search.fit(X_train, y_train)


# In[89]:


grid_search.best_params_


# In[90]:


grid_search.best_score_


# In[91]:


from sklearn.metrics import accuracy_score

y_pred = grid_search.predict(X_test)
accuracy_score(y_test, y_pred)


# ## 2. Data Augmentation

# In[79]:


from scipy.ndimage.interpolation import shift


# In[80]:


def shift_image(image, dx, dy):
    image = image.reshape((28, 28))
    shifted_image = shift(image, [dy, dx], cval=0, mode="constant")
    return shifted_image.reshape([-1])


# In[81]:


image = X_train[1000]
shifted_image_down = shift_image(image, 0, 5)
shifted_image_left = shift_image(image, -5, 0)

plt.figure(figsize=(12,3))
plt.subplot(131)
plt.title("Original", fontsize=14)
plt.imshow(image.reshape(28, 28), interpolation="nearest", cmap="Greys")
plt.subplot(132)
plt.title("Shifted down", fontsize=14)
plt.imshow(shifted_image_down.reshape(28, 28), interpolation="nearest", cmap="Greys")
plt.subplot(133)
plt.title("Shifted left", fontsize=14)
plt.imshow(shifted_image_left.reshape(28, 28), interpolation="nearest", cmap="Greys")
plt.show()


# In[95]:


X_train_augmented = [image for image in X_train]
y_train_augmented = [label for label in y_train]

for dx, dy in ((1, 0), (-1, 0), (0, 1), (0, -1)):
    for image, label in zip(X_train, y_train):
        X_train_augmented.append(shift_image(image, dx, dy))
        y_train_augmented.append(label)

X_train_augmented = np.array(X_train_augmented)
y_train_augmented = np.array(y_train_augmented)


# In[96]:


shuffle_idx = np.random.permutation(len(X_train_augmented))
X_train_augmented = X_train_augmented[shuffle_idx]
y_train_augmented = y_train_augmented[shuffle_idx]


# In[97]:


knn_clf = KNeighborsClassifier(**grid_search.best_params_)


# In[98]:


knn_clf.fit(X_train_augmented, y_train_augmented)


# In[99]:


y_pred = knn_clf.predict(X_test)
accuracy_score(y_test, y_pred)


# By simply augmenting the data, we got a 0.5% accuracy boost. :)

# ## 3. Tackle the Titanic dataset

# The goal is to predict whether or not a passenger survived based on attributes such as their age, sex, passenger class, where they embarked and so on.

# First, login to [Kaggle](https://www.kaggle.com/) and go to the [Titanic challenge](https://www.kaggle.com/c/titanic) to download `train.csv` and `test.csv`. Save them to the `datasets/titanic` directory.

# Next, let's load the data:

# In[3]:


import os

TITANIC_PATH = os.path.join("datasets", "titanic")


# In[4]:


import pandas as pd

def load_titanic_data(filename, titanic_path=TITANIC_PATH):
    csv_path = os.path.join(titanic_path, filename)
    return pd.read_csv(csv_path)


# In[5]:


train_data = load_titanic_data("train.csv")
test_data = load_titanic_data("test.csv")


# The data is already split into a training set and a test set. However, the test data does *not* contain the labels: your goal is to train the best model you can using the training data, then make your predictions on the test data and upload them to Kaggle to see your final score.

# Let's take a peek at the top few rows of the training set:

# In[6]:


train_data.head()


# The attributes have the following meaning:
# * **Survived**: that's the target, 0 means the passenger did not survive, while 1 means he/she survived.
# * **Pclass**: passenger class.
# * **Name**, **Sex**, **Age**: self-explanatory
# * **SibSp**: how many siblings & spouses of the passenger aboard the Titanic.
# * **Parch**: how many children & parents of the passenger aboard the Titanic.
# * **Ticket**: ticket id
# * **Fare**: price paid (in pounds)
# * **Cabin**: passenger's cabin number
# * **Embarked**: where the passenger embarked the Titanic

# Let's get more info to see how much data is missing:

# In[7]:


train_data.info()


# Okay, the **Age**, **Cabin** and **Embarked** attributes are sometimes null (less than 891 non-null), especially the **Cabin** (77% are null). We will ignore the **Cabin** for now and focus on the rest. The **Age** attribute has about 19% null values, so we will need to decide what to do with them. Replacing null values with the median age seems reasonable.

# The **Name** and **Ticket** attributes may have some value, but they will be a bit tricky to convert into useful numbers that a model can consume. So for now, we will ignore them.

# Let's take a look at the numerical attributes:

# In[8]:


train_data.describe()


# * Yikes, only 38% **Survived**. :(  That's close enough to 40%, so accuracy will be a reasonable metric to evaluate our model.
# * The mean **Fare** was £32.20, which does not seem so expensive (but it was probably a lot of money back then).
# * The mean **Age** was less than 30 years old.

# Let's check that the target is indeed 0 or 1:

# In[9]:


train_data["Survived"].value_counts()


# Now let's take a quick look at all the categorical attributes:

# In[10]:


train_data["Pclass"].value_counts()


# In[11]:


train_data["Sex"].value_counts()


# In[12]:


train_data["Embarked"].value_counts()


# The Embarked attribute tells us where the passenger embarked: C=Cherbourg, Q=Queenstown, S=Southampton.

# **Note**: the code below uses a mix of `Pipeline`, `FeatureUnion` and a custom `DataFrameSelector` to preprocess some columns differently.  Since Scikit-Learn 0.20, it is preferable to use a `ColumnTransformer`, like in the previous chapter.

# Now let's build our preprocessing pipelines. We will reuse the `DataframeSelector` we built in the previous chapter to select specific attributes from the `DataFrame`:

# In[13]:


from sklearn.base import BaseEstimator, TransformerMixin

class DataFrameSelector(BaseEstimator, TransformerMixin):
    def __init__(self, attribute_names):
        self.attribute_names = attribute_names
    def fit(self, X, y=None):
        return self
    def transform(self, X):
        return X[self.attribute_names]


# Let's build the pipeline for the numerical attributes:

# In[14]:


from sklearn.pipeline import Pipeline
from sklearn.impute import SimpleImputer

num_pipeline = Pipeline([
        ("select_numeric", DataFrameSelector(["Age", "SibSp", "Parch", "Fare"])),
        ("imputer", SimpleImputer(strategy="median")),
    ])


# In[15]:


num_pipeline.fit_transform(train_data)


# We will also need an imputer for the string categorical columns (the regular `SimpleImputer` does not work on those):

# In[16]:


# Inspired from stackoverflow.com/questions/25239958
class MostFrequentImputer(BaseEstimator, TransformerMixin):
    def fit(self, X, y=None):
        self.most_frequent_ = pd.Series([X[c].value_counts().index[0] for c in X],
                                        index=X.columns)
        return self
    def transform(self, X, y=None):
        return X.fillna(self.most_frequent_)


# In[17]:


from sklearn.preprocessing import OneHotEncoder


# Now we can build the pipeline for the categorical attributes:

# In[18]:


cat_pipeline = Pipeline([
        ("select_cat", DataFrameSelector(["Pclass", "Sex", "Embarked"])),
        ("imputer", MostFrequentImputer()),
        ("cat_encoder", OneHotEncoder(sparse=False)),
    ])


# In[19]:


cat_pipeline.fit_transform(train_data)


# Finally, let's join the numerical and categorical pipelines:

# In[20]:


from sklearn.pipeline import FeatureUnion
preprocess_pipeline = FeatureUnion(transformer_list=[
        ("num_pipeline", num_pipeline),
        ("cat_pipeline", cat_pipeline),
    ])


# Cool! Now we have a nice preprocessing pipeline that takes the raw data and outputs numerical input features that we can feed to any Machine Learning model we want.

# In[21]:


X_train = preprocess_pipeline.fit_transform(train_data)
X_train


# Let's not forget to get the labels:

# In[22]:


y_train = train_data["Survived"]


# We are now ready to train a classifier. Let's start with an `SVC`:

# In[23]:


from sklearn.svm import SVC

svm_clf = SVC(gamma="auto")
svm_clf.fit(X_train, y_train)


# Great, our model is trained, let's use it to make predictions on the test set:

# In[24]:


X_test = preprocess_pipeline.transform(test_data)
y_pred = svm_clf.predict(X_test)


# And now we could just build a CSV file with these predictions (respecting the format excepted by Kaggle), then upload it and hope for the best. But wait! We can do better than hope. Why don't we use cross-validation to have an idea of how good our model is?

# In[25]:


from sklearn.model_selection import cross_val_score

svm_scores = cross_val_score(svm_clf, X_train, y_train, cv=10,n_jobs=-1)
svm_scores.mean()


# Okay, over 73% accuracy, clearly better than random chance, but it's not a great score. Looking at the [leaderboard](https://www.kaggle.com/c/titanic/leaderboard) for the Titanic competition on Kaggle, you can see that you need to reach above 80% accuracy to be within the top 10% Kagglers. Some reached 100%, but since you can easily find the [list of victims](https://www.encyclopedia-titanica.org/titanic-victims/) of the Titanic, it seems likely that there was little Machine Learning involved in their performance! ;-) So let's try to build a model that reaches 80% accuracy.

# Let's try a `RandomForestClassifier`:

# In[26]:


from sklearn.ensemble import RandomForestClassifier

forest_clf = RandomForestClassifier(n_estimators=100, random_state=42,n_jobs=-1)
forest_scores = cross_val_score(forest_clf, X_train, y_train, cv=10,n_jobs=-1)
forest_scores.mean()


# That's much better!

# Instead of just looking at the mean accuracy across the 10 cross-validation folds, let's plot all 10 scores for each model, along with a box plot highlighting the lower and upper quartiles, and "whiskers" showing the extent of the scores (thanks to Nevin Yilmaz for suggesting this visualization). Note that the `boxplot()` function detects outliers (called "fliers") and does not include them within the whiskers. Specifically, if the lower quartile is $Q_1$ and the upper quartile is $Q_3$, then the interquartile range $IQR = Q_3 - Q_1$ (this is the box's height), and any score lower than $Q_1 - 1.5 \times IQR$ is a flier, and so is any score greater than $Q3 + 1.5 \times IQR$.

# In[27]:


plt.figure(figsize=(8, 4))
plt.plot([1]*10, svm_scores, ".")
plt.plot([2]*10, forest_scores, ".")
plt.boxplot([svm_scores, forest_scores], labels=("SVM","Random Forest"))
plt.ylabel("Accuracy", fontsize=14)
plt.show()


# To improve this result further, you could:
# * Compare many more models and tune hyperparameters using cross validation and grid search,
# * Do more feature engineering, for example:
#   * replace **SibSp** and **Parch** with their sum,
#   * try to identify parts of names that correlate well with the **Survived** attribute (e.g. if the name contains "Countess", then survival seems more likely),
# * try to convert numerical attributes to categorical attributes: for example, different age groups had very different survival rates (see below), so it may help to create an age bucket category and use it instead of the age. Similarly, it may be useful to have a special category for people traveling alone since only 30% of them survived (see below).

# In[29]:


train_data["AgeBucket"] = train_data["Age"] // 15 * 15
train_data[["AgeBucket", "Survived"]].groupby(['AgeBucket']).mean()


# In[30]:


train_data["RelativesOnboard"] = train_data["SibSp"] + train_data["Parch"]
train_data[["RelativesOnboard", "Survived"]].groupby(['RelativesOnboard']).mean()


# ## 4. Spam classifier

# First, let's fetch the data:

# In[31]:


import os
import tarfile
from six.moves import urllib

DOWNLOAD_ROOT = "http://spamassassin.apache.org/old/publiccorpus/"
HAM_URL = DOWNLOAD_ROOT + "20030228_easy_ham.tar.bz2"
SPAM_URL = DOWNLOAD_ROOT + "20030228_spam.tar.bz2"
SPAM_PATH = os.path.join("datasets", "spam")

def fetch_spam_data(spam_url=SPAM_URL, spam_path=SPAM_PATH):
    if not os.path.isdir(spam_path):
        os.makedirs(spam_path)
    for filename, url in (("ham.tar.bz2", HAM_URL), ("spam.tar.bz2", SPAM_URL)):
        path = os.path.join(spam_path, filename)
        if not os.path.isfile(path):
            urllib.request.urlretrieve(url, path)
        tar_bz2_file = tarfile.open(path)
        tar_bz2_file.extractall(path=SPAM_PATH)
        tar_bz2_file.close()


# In[32]:


fetch_spam_data()


# Next, let's load all the emails:

# In[33]:


HAM_DIR = os.path.join(SPAM_PATH, "easy_ham")
SPAM_DIR = os.path.join(SPAM_PATH, "spam")
ham_filenames = [name for name in sorted(os.listdir(HAM_DIR)) if len(name) > 20]
spam_filenames = [name for name in sorted(os.listdir(SPAM_DIR)) if len(name) > 20]


# In[34]:


len(ham_filenames)


# In[35]:


len(spam_filenames)


# We can use Python's `email` module to parse these emails (this handles headers, encoding, and so on):

# In[36]:


import email
import email.policy

def load_email(is_spam, filename, spam_path=SPAM_PATH):
    directory = "spam" if is_spam else "easy_ham"
    with open(os.path.join(spam_path, directory, filename), "rb") as f:
        return email.parser.BytesParser(policy=email.policy.default).parse(f)


# In[37]:


ham_emails = [load_email(is_spam=False, filename=name) for name in ham_filenames]
spam_emails = [load_email(is_spam=True, filename=name) for name in spam_filenames]


# Let's look at one example of ham and one example of spam, to get a feel of what the data looks like:

# In[38]:


print(ham_emails[1].get_content().strip())


# In[41]:


print(spam_emails[6].get_content().strip())


# Some emails are actually multipart, with images and attachments (which can have their own attachments). Let's look at the various types of structures we have:

# In[42]:


def get_email_structure(email):
    if isinstance(email, str):
        return email
    payload = email.get_payload()
    if isinstance(payload, list):
        return "multipart({})".format(", ".join([
            get_email_structure(sub_email)
            for sub_email in payload
        ]))
    else:
        return email.get_content_type()


# In[43]:


from collections import Counter

def structures_counter(emails):
    structures = Counter()
    for email in emails:
        structure = get_email_structure(email)
        structures[structure] += 1
    return structures


# In[44]:


structures_counter(ham_emails).most_common()


# In[45]:


structures_counter(spam_emails).most_common()


# It seems that the ham emails are more often plain text, while spam has quite a lot of HTML. Moreover, quite a few ham emails are signed using PGP, while no spam is. In short, it seems that the email structure is useful information to have.

# Now let's take a look at the email headers:

# In[46]:


for header, value in spam_emails[0].items():
    print(header,":",value)


# There's probably a lot of useful information in there, such as the sender's email address (12a1mailbot1@web.de looks fishy), but we will just focus on the `Subject` header:

# In[47]:


spam_emails[0]["Subject"]


# Okay, before we learn too much about the data, let's not forget to split it into a training set and a test set:

# In[48]:


import numpy as np
from sklearn.model_selection import train_test_split

X = np.array(ham_emails + spam_emails)
y = np.array([0] * len(ham_emails) + [1] * len(spam_emails))

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)


# Okay, let's start writing the preprocessing functions. First, we will need a function to convert HTML to plain text. Arguably the best way to do this would be to use the great [BeautifulSoup](https://www.crummy.com/software/BeautifulSoup/) library, but I would like to avoid adding another dependency to this project, so let's hack a quick & dirty solution using regular expressions (at the risk of [un̨ho͞ly radiańcé destro҉ying all enli̍̈́̂̈́ghtenment](https://stackoverflow.com/a/1732454/38626)). The following function first drops the `<head>` section, then converts all `<a>` tags to the word HYPERLINK, then it gets rid of all HTML tags, leaving only the plain text. For readability, it also replaces multiple newlines with single newlines, and finally it unescapes html entities (such as `&gt;` or `&nbsp;`):

# In[49]:


import re
from html import unescape

def html_to_plain_text(html):
    text = re.sub('<head.*?>.*?</head>', '', html, flags=re.M | re.S | re.I)
    text = re.sub('<a\s.*?>', ' HYPERLINK ', text, flags=re.M | re.S | re.I)
    text = re.sub('<.*?>', '', text, flags=re.M | re.S)
    text = re.sub(r'(\s*\n)+', '\n', text, flags=re.M | re.S)
    return unescape(text)


# Let's see if it works. This is HTML spam:

# In[50]:


html_spam_emails = [email for email in X_train[y_train==1]
                    if get_email_structure(email) == "text/html"]
sample_html_spam = html_spam_emails[7]
print(sample_html_spam.get_content().strip()[:1000], "...")


# And this is the resulting plain text:

# In[51]:


print(html_to_plain_text(sample_html_spam.get_content())[:1000], "...")


# Great! Now let's write a function that takes an email as input and returns its content as plain text, whatever its format is:

# In[52]:


def email_to_text(email):
    html = None
    for part in email.walk():
        ctype = part.get_content_type()
        if not ctype in ("text/plain", "text/html"):
            continue
        try:
            content = part.get_content()
        except: # in case of encoding issues
            content = str(part.get_payload())
        if ctype == "text/plain":
            return content
        else:
            html = content
    if html:
        return html_to_plain_text(html)


# In[53]:


print(email_to_text(sample_html_spam)[:100], "...")


# Let's throw in some stemming! For this to work, you need to install the Natural Language Toolkit ([NLTK](http://www.nltk.org/)). It's as simple as running the following command (don't forget to activate your virtualenv first; if you don't have one, you will likely need administrator rights, or use the `--user` option):
# 
# `$ pip3 install nltk`

# In[54]:


try:
    import nltk

    stemmer = nltk.PorterStemmer()
    for word in ("Computations", "Computation", "Computing", "Computed", "Compute", "Compulsive"):
        print(word, "=>", stemmer.stem(word))
except ImportError:
    print("Error: stemming requires the NLTK module.")
    stemmer = None


# We will also need a way to replace URLs with the word "URL". For this, we could use hard core [regular expressions](https://mathiasbynens.be/demo/url-regex) but we will just use the [urlextract](https://github.com/lipoja/URLExtract) library. You can install it with the following command (don't forget to activate your virtualenv first; if you don't have one, you will likely need administrator rights, or use the `--user` option):
# 
# `$ pip3 install urlextract`

# In[55]:


try:
    import urlextract # may require an Internet connection to download root domain names
    
    url_extractor = urlextract.URLExtract()
    print(url_extractor.find_urls("Will it detect github.com and https://youtu.be/7Pq-S557XQU?t=3m32s"))
except ImportError:
    print("Error: replacing URLs requires the urlextract module.")
    url_extractor = None


# We are ready to put all this together into a transformer that we will use to convert emails to word counters. Note that we split sentences into words using Python's `split()` method, which uses whitespaces for word boundaries. This works for many written languages, but not all. For example, Chinese and Japanese scripts generally don't use spaces between words, and Vietnamese often uses spaces even between syllables. It's okay in this exercise, because the dataset is (mostly) in English.

# In[56]:


from sklearn.base import BaseEstimator, TransformerMixin

class EmailToWordCounterTransformer(BaseEstimator, TransformerMixin):
    def __init__(self, strip_headers=True, lower_case=True, remove_punctuation=True,
                 replace_urls=True, replace_numbers=True, stemming=True):
        self.strip_headers = strip_headers
        self.lower_case = lower_case
        self.remove_punctuation = remove_punctuation
        self.replace_urls = replace_urls
        self.replace_numbers = replace_numbers
        self.stemming = stemming
    def fit(self, X, y=None):
        return self
    def transform(self, X, y=None):
        X_transformed = []
        for email in X:
            text = email_to_text(email) or ""
            if self.lower_case:
                text = text.lower()
            if self.replace_urls and url_extractor is not None:
                urls = list(set(url_extractor.find_urls(text)))
                urls.sort(key=lambda url: len(url), reverse=True)
                for url in urls:
                    text = text.replace(url, " URL ")
            if self.replace_numbers:
                text = re.sub(r'\d+(?:\.\d*(?:[eE]\d+))?', 'NUMBER', text)
            if self.remove_punctuation:
                text = re.sub(r'\W+', ' ', text, flags=re.M)
            word_counts = Counter(text.split())
            if self.stemming and stemmer is not None:
                stemmed_word_counts = Counter()
                for word, count in word_counts.items():
                    stemmed_word = stemmer.stem(word)
                    stemmed_word_counts[stemmed_word] += count
                word_counts = stemmed_word_counts
            X_transformed.append(word_counts)
        return np.array(X_transformed)


# Let's try this transformer on a few emails:

# In[57]:


X_few = X_train[:3]
X_few_wordcounts = EmailToWordCounterTransformer().fit_transform(X_few)
X_few_wordcounts


# This looks about right!

# Now we have the word counts, and we need to convert them to vectors. For this, we will build another transformer whose `fit()` method will build the vocabulary (an ordered list of the most common words) and whose `transform()` method will use the vocabulary to convert word counts to vectors. The output is a sparse matrix.

# In[58]:


from scipy.sparse import csr_matrix

class WordCounterToVectorTransformer(BaseEstimator, TransformerMixin):
    def __init__(self, vocabulary_size=1000):
        self.vocabulary_size = vocabulary_size
    def fit(self, X, y=None):
        total_count = Counter()
        for word_count in X:
            for word, count in word_count.items():
                total_count[word] += min(count, 10)
        most_common = total_count.most_common()[:self.vocabulary_size]
        self.most_common_ = most_common
        self.vocabulary_ = {word: index + 1 for index, (word, count) in enumerate(most_common)}
        return self
    def transform(self, X, y=None):
        rows = []
        cols = []
        data = []
        for row, word_count in enumerate(X):
            for word, count in word_count.items():
                rows.append(row)
                cols.append(self.vocabulary_.get(word, 0))
                data.append(count)
        return csr_matrix((data, (rows, cols)), shape=(len(X), self.vocabulary_size + 1))


# In[59]:


vocab_transformer = WordCounterToVectorTransformer(vocabulary_size=10)
X_few_vectors = vocab_transformer.fit_transform(X_few_wordcounts)
X_few_vectors


# In[60]:


X_few_vectors.toarray()


# What does this matrix mean? Well, the 64 in the third row, first column, means that the third email contains 64 words that are not part of the vocabulary. The 1 next to it means that the first word in the vocabulary is present once in this email. The 2 next to it means that the second word is present twice, and so on. You can look at the vocabulary to know which words we are talking about. The first word is "of", the second word is "and", etc.

# In[61]:


vocab_transformer.vocabulary_


# We are now ready to train our first spam classifier! Let's transform the whole dataset:

# In[62]:


from sklearn.pipeline import Pipeline

preprocess_pipeline = Pipeline([
    ("email_to_wordcount", EmailToWordCounterTransformer()),
    ("wordcount_to_vector", WordCounterToVectorTransformer()),
])

X_train_transformed = preprocess_pipeline.fit_transform(X_train)


# **Note**: to be future-proof, we set `solver="lbfgs"` since this will be the default value in Scikit-Learn 0.22.

# In[63]:


from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import cross_val_score

log_clf = LogisticRegression(solver="lbfgs", random_state=42)
score = cross_val_score(log_clf, X_train_transformed, y_train, cv=3, verbose=3)
score.mean()


# Over 98.7%, not bad for a first try! :) However, remember that we are using the "easy" dataset. You can try with the harder datasets, the results won't be so amazing. You would have to try multiple models, select the best ones and fine-tune them using cross-validation, and so on.
# 
# But you get the picture, so let's stop now, and just print out the precision/recall we get on the test set:

# In[64]:


from sklearn.metrics import precision_score, recall_score

X_test_transformed = preprocess_pipeline.transform(X_test)

log_clf = LogisticRegression(solver="lbfgs", random_state=42)
log_clf.fit(X_train_transformed, y_train)

y_pred = log_clf.predict(X_test_transformed)

print("Precision: {:.2f}%".format(100 * precision_score(y_test, y_pred)))
print("Recall: {:.2f}%".format(100 * recall_score(y_test, y_pred)))


# In[ ]:




