from .studyPipe import config
from .studyPipe import __, _fun_,_funs_, __fun__, _ftools_, _funsInv_
from .studyPipe import px, _px, px_, p, _p, p_ #sspipe
from .studyPipe import X, _X, X_, df, _df, df_ #dfply
from .studyPipe import curried, _c, c_ #toolz.curried
from .studyPipe import T, F

