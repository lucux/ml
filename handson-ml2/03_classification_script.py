#!/usr/bin/env python
# coding: utf-8

# <h1>Table of Contents<span class="tocSkip"></span></h1>
# <div class="toc"><ul class="toc-item"><li><span><a href="#Setup" data-toc-modified-id="Setup-1"><span class="toc-item-num">1&nbsp;&nbsp;</span>Setup</a></span></li><li><span><a href="#MNIST" data-toc-modified-id="MNIST-2"><span class="toc-item-num">2&nbsp;&nbsp;</span>MNIST</a></span><ul class="toc-item"><li><span><a href="#Create-Project" data-toc-modified-id="Create-Project-2.1"><span class="toc-item-num">2.1&nbsp;&nbsp;</span>Create Project</a></span><ul class="toc-item"><li><span><a href="#if-DATA-exist-already-in-project" data-toc-modified-id="if-DATA-exist-already-in-project-2.1.1"><span class="toc-item-num">2.1.1&nbsp;&nbsp;</span>if DATA exist already in project</a></span></li><li><span><a href="#load-DATAS-(set--Project-Data)" data-toc-modified-id="load-DATAS-(set--Project-Data)-2.1.2"><span class="toc-item-num">2.1.2&nbsp;&nbsp;</span>load DATAS (set  Project Data)</a></span><ul class="toc-item"><li><span><a href="#save-only-DataSupervise" data-toc-modified-id="save-only-DataSupervise-2.1.2.1"><span class="toc-item-num">2.1.2.1&nbsp;&nbsp;</span>save only DataSupervise</a></span></li></ul></li><li><span><a href="#FROM-IMPORTED-DATAS" data-toc-modified-id="FROM-IMPORTED-DATAS-2.1.3"><span class="toc-item-num">2.1.3&nbsp;&nbsp;</span>FROM IMPORTED DATAS</a></span></li><li><span><a href="#[OMIT]--fetchML" data-toc-modified-id="[OMIT]--fetchML-2.1.4"><span class="toc-item-num">2.1.4&nbsp;&nbsp;</span>[OMIT]  fetchML</a></span></li></ul></li></ul></li><li><span><a href="#Binary-classifier" data-toc-modified-id="Binary-classifier-3"><span class="toc-item-num">3&nbsp;&nbsp;</span>Binary classifier</a></span><ul class="toc-item"><li><span><a href="#Orig" data-toc-modified-id="Orig-3.1"><span class="toc-item-num">3.1&nbsp;&nbsp;</span>Orig</a></span></li><li><span><a href="#ME" data-toc-modified-id="ME-3.2"><span class="toc-item-num">3.2&nbsp;&nbsp;</span>ME</a></span><ul class="toc-item"><li><span><a href="#study-de-base-avec-quelque-models" data-toc-modified-id="study-de-base-avec-quelque-models-3.2.1"><span class="toc-item-num">3.2.1&nbsp;&nbsp;</span>study de base avec quelque models</a></span><ul class="toc-item"><li><span><a href="#recreate" data-toc-modified-id="recreate-3.2.1.1"><span class="toc-item-num">3.2.1.1&nbsp;&nbsp;</span>recreate</a></span></li><li><span><a href="#analyse" data-toc-modified-id="analyse-3.2.1.2"><span class="toc-item-num">3.2.1.2&nbsp;&nbsp;</span>analyse</a></span></li></ul></li><li><span><a href="#Study-base-+-dummy" data-toc-modified-id="Study-base-+-dummy-3.2.2"><span class="toc-item-num">3.2.2&nbsp;&nbsp;</span>Study base + dummy</a></span><ul class="toc-item"><li><span><a href="#create" data-toc-modified-id="create-3.2.2.1"><span class="toc-item-num">3.2.2.1&nbsp;&nbsp;</span>create</a></span></li><li><span><a href="#analyse" data-toc-modified-id="analyse-3.2.2.2"><span class="toc-item-num">3.2.2.2&nbsp;&nbsp;</span>analyse</a></span></li></ul></li><li><span><a href="#OLD" data-toc-modified-id="OLD-3.2.3"><span class="toc-item-num">3.2.3&nbsp;&nbsp;</span>OLD</a></span></li></ul></li></ul></li><li><span><a href="#ROC-curves" data-toc-modified-id="ROC-curves-4"><span class="toc-item-num">4&nbsp;&nbsp;</span>ROC curves</a></span><ul class="toc-item"><li><span><a href="#M2" data-toc-modified-id="M2-4.1"><span class="toc-item-num">4.1&nbsp;&nbsp;</span>M2</a></span><ul class="toc-item"><li><span><a href="#create" data-toc-modified-id="create-4.1.1"><span class="toc-item-num">4.1.1&nbsp;&nbsp;</span>create</a></span></li><li><span><a href="#analyse" data-toc-modified-id="analyse-4.1.2"><span class="toc-item-num">4.1.2&nbsp;&nbsp;</span>analyse</a></span></li></ul></li><li><span><a href="#Orig" data-toc-modified-id="Orig-4.2"><span class="toc-item-num">4.2&nbsp;&nbsp;</span>Orig</a></span></li></ul></li><li><span><a href="#Multiclass-classification" data-toc-modified-id="Multiclass-classification-5"><span class="toc-item-num">5&nbsp;&nbsp;</span>Multiclass classification</a></span><ul class="toc-item"><li><span><a href="#ME" data-toc-modified-id="ME-5.1"><span class="toc-item-num">5.1&nbsp;&nbsp;</span>ME</a></span><ul class="toc-item"><li><span><a href="#create" data-toc-modified-id="create-5.1.1"><span class="toc-item-num">5.1.1&nbsp;&nbsp;</span>create</a></span></li><li><span><a href="#analyse" data-toc-modified-id="analyse-5.1.2"><span class="toc-item-num">5.1.2&nbsp;&nbsp;</span>analyse</a></span><ul class="toc-item"><li><span><a href="#blabla" data-toc-modified-id="blabla-5.1.2.1"><span class="toc-item-num">5.1.2.1&nbsp;&nbsp;</span>blabla</a></span></li><li><span><a href="#RF" data-toc-modified-id="RF-5.1.2.2"><span class="toc-item-num">5.1.2.2&nbsp;&nbsp;</span>RF</a></span></li></ul></li></ul></li><li><span><a href="#Orig" data-toc-modified-id="Orig-5.2"><span class="toc-item-num">5.2&nbsp;&nbsp;</span>Orig</a></span></li></ul></li><li><span><a href="#Multilabel-classification" data-toc-modified-id="Multilabel-classification-6"><span class="toc-item-num">6&nbsp;&nbsp;</span>Multilabel classification</a></span><ul class="toc-item"><li><span><a href="#ME" data-toc-modified-id="ME-6.1"><span class="toc-item-num">6.1&nbsp;&nbsp;</span>ME</a></span></li><li><span><a href="#Origg" data-toc-modified-id="Origg-6.2"><span class="toc-item-num">6.2&nbsp;&nbsp;</span>Origg</a></span></li></ul></li><li><span><a href="#Multioutput-classification" data-toc-modified-id="Multioutput-classification-7"><span class="toc-item-num">7&nbsp;&nbsp;</span>Multioutput classification</a></span></li><li><span><a href="#Extra-material" data-toc-modified-id="Extra-material-8"><span class="toc-item-num">8&nbsp;&nbsp;</span>Extra material</a></span><ul class="toc-item"><li><span><a href="#Dummy-(ie.-random)-classifier" data-toc-modified-id="Dummy-(ie.-random)-classifier-8.1"><span class="toc-item-num">8.1&nbsp;&nbsp;</span>Dummy (ie. random) classifier</a></span></li><li><span><a href="#KNN-classifier" data-toc-modified-id="KNN-classifier-8.2"><span class="toc-item-num">8.2&nbsp;&nbsp;</span>KNN classifier</a></span></li></ul></li><li><span><a href="#Exercise-solutions" data-toc-modified-id="Exercise-solutions-9"><span class="toc-item-num">9&nbsp;&nbsp;</span>Exercise solutions</a></span><ul class="toc-item"><li><span><a href="#1.-An-MNIST-Classifier-With-Over-97%-Accuracy" data-toc-modified-id="1.-An-MNIST-Classifier-With-Over-97%-Accuracy-9.1"><span class="toc-item-num">9.1&nbsp;&nbsp;</span>1. An MNIST Classifier With Over 97% Accuracy</a></span><ul class="toc-item"><li><span><a href="#Orig" data-toc-modified-id="Orig-9.1.1"><span class="toc-item-num">9.1.1&nbsp;&nbsp;</span>Orig</a></span></li></ul></li><li><span><a href="#2.-Data-Augmentation" data-toc-modified-id="2.-Data-Augmentation-9.2"><span class="toc-item-num">9.2&nbsp;&nbsp;</span>2. Data Augmentation</a></span></li><li><span><a href="#3.-Tackle-the-Titanic-dataset" data-toc-modified-id="3.-Tackle-the-Titanic-dataset-9.3"><span class="toc-item-num">9.3&nbsp;&nbsp;</span>3. Tackle the Titanic dataset</a></span><ul class="toc-item"><li><span><a href="#ME" data-toc-modified-id="ME-9.3.1"><span class="toc-item-num">9.3.1&nbsp;&nbsp;</span>ME</a></span></li></ul></li><li><span><a href="#4.-Spam-classifier" data-toc-modified-id="4.-Spam-classifier-9.4"><span class="toc-item-num">9.4&nbsp;&nbsp;</span>4. Spam classifier</a></span></li></ul></li></ul></div>

# **Chapter 3 – Classification**
# 
# _This notebook contains all the sample code and solutions to the exercises in chapter 3._

# # Setup

# First, let's import a few common modules, ensure MatplotLib plots figures inline and prepare a function to save the figures. We also check that Python 3.5 or later is installed (although Python 2.x may work, it is deprecated so we strongly recommend you use Python 3 instead), as well as Scikit-Learn ≥0.20.

# StudyPipe sys

# In[2]:


get_ipython().system('pip  install --user -U git+https://github.com/luluperet/studyPipe.git')


# In[1]:


import sys,os


# In[4]:


sys.path.append('/home/devel/work/handson-ml2/studyPipeGit/studyPipe/')


# In[2]:


# Python ≥3.5 is required
import sys
assert sys.version_info >= (3, 5)

# Scikit-Learn ≥0.20 is required
import sklearn
assert sklearn.__version__ >= "0.20"
from sklearn.utils._joblib import Parallel, delayed
from sklearn.tree import DecisionTreeClassifier

# Common imports
import numpy as np
import os

# to make this notebook's output stable across runs
np.random.seed(42)

# To plot pretty figures
get_ipython().run_line_magic('matplotlib', 'inline')
import matplotlib as mpl
import matplotlib.pyplot as plt
mpl.rc('axes', labelsize=14)
mpl.rc('xtick', labelsize=12)
mpl.rc('ytick', labelsize=12)

# Where to save the figures
PROJECT_ROOT_DIR = "."
CHAPTER_ID = "classification"
IMAGES_PATH = os.path.join(PROJECT_ROOT_DIR, "images", CHAPTER_ID)
os.makedirs(IMAGES_PATH, exist_ok=True)

def save_fig(fig_id, tight_layout=True, fig_extension="png", resolution=300):
    path = os.path.join(IMAGES_PATH, fig_id + "." + fig_extension)
    print("Saving figure", fig_id)
    if tight_layout:
        plt.tight_layout()
    plt.savefig(path, format=fig_extension, dpi=resolution)
import re
import importlib
reloadModule=lambda m:importlib.reload(m)


# In[8]:


get_ipython().system('sudo -i pip install git+https://github.com/lucasiscovici/studyPipe.git')


# In[3]:


#CONFIG
from studyPipeGit import config
config.globalsFn=lambda:globals()
#import 
from studyPipeGit.studyPipe.pipes import *


# In[4]:


from studyProjectGit import StudyProject, StudyClassif, StudyClassifProject,Base, DatasSupervise,Datas
from studyProjectGit.studyProject.utils import *
from studyProjectGit.studyProject.helpers import *


# In[5]:


import warnings
warnings.filterwarnings("ignore", message="numpy.dtype size changed")
warnings.filterwarnings("ignore", message="numpy.ufunc size changed")


# # MNIST

# ## Create Project

# In[6]:


studyGlobal_MNIST=StudyProject.getOrCreate("MNIST",recreate=F)


# ### if DATA exist already in project

# In[7]:


X_train,y_train,X_test,y_test,_=studyGlobal_MNIST.data["DATA_MNIST"].get(deep=True,optsTrain={"withNamesY":F})
X_train_flat=X_train
X_test_flat=X_test


# In[7]:


def createPltText(texts,fontsize="14",**xargs):
    plt.figure(figsize=(0.5,0.5))
    [ plt.text(0,y,text,fontsize=fontsize,**xargs) for y,text in zip(range(0,len(texts)),texts)]
    plt.axis("off")


# In[8]:


def createPltTexts(texts,fontsize="14",**xargs):
    plt.figure(figsize=(0.5,0.5))
    [ plt.text(0,y,text,fontsize=fontsize,**xargs) for y,text in zip(range(0,len(texts)),texts)]
    plt.axis("off")


# In[9]:


studyGlobal_MNIST.studies.keys()  %_fun_% list |_fun_| createPltTexts


# ###   load DATAS (set  Project Data)

# In[3]:


studyGlobal_MNIST._studies["studyGlobal_MNIST_BaseModels_Sgd+LogReg"].cv


# In[5]:


#from sklearn.datasets import fetch_openml
from keras.datasets import mnist


# In[6]:


(X_train,y_train), (X_test,y_test) = mnist.load_data()


# In[7]:


X_train_flat=X_train.reshape((X_train.shape[0],-1))
X_test_flat=X_test.reshape((X_test.shape[0],-1))


# In[8]:


studyGlobal_MNIST.saveDatasWithId("DATA_MNIST",
                                  X_train_flat,y_train,
                                  X_test_flat,y_test)


# ####  save only DataSupervise

# In[9]:


studyGlobal_MNIST.data |_fun_| studyDico | __[0] | __.export


# In[12]:


studyGlobal_MNIST.data |_fun_| studyDico | __[0].dataTrain | __.export


# ###  FROM IMPORTED DATAS

# In[8]:


datu=importFromFile("DATA_MNIST.study_datas_supervise.EXP.bz2",inferDirs=T)


# In[9]:


studyGlobal_MNIST.saveDataSupervise(datu)


# In[ ]:


studyClassif_Img._plotDigits(X_train,
                             lim=100,
                             figsize=(8,8))


# ### [OMIT]  fetchML

# In[ ]:


mnist = fetch_openml('mnist_784', version=1)
mnist.keys()


# In[ ]:


X, y = mnist["data"], mnist["target"]
X.shape


# In[ ]:


y.shape


# In[ ]:


28 * 28


# In[ ]:


X[:10].shape


# In[ ]:


studyClassif_Img._plotDigits(X,lim=50)


# In[ ]:


y[0]


# In[ ]:


y = y.astype(np.uint8)


# In[ ]:


y[0]


# In[ ]:


#setData test_size=0.2, train_size=0.8, shuffle=True, random_state=42
#studyMnist.setData(X,y)


# In[ ]:


X_train, X_test, y_train, y_test = X[:60000], X[60000:], y[:60000], y[60000:]


# # Binary classifier

# ## Orig

# In[ ]:


y_train_5 = (y_train == 5)
y_test_5 = (y_test == 5)


# **Note**: some hyperparameters will have a different defaut value in future versions of Scikit-Learn, such as `max_iter` and `tol`. To be future-proof, we explicitly set these hyperparameters to their future default values. For simplicity, this is not shown in the book.

# In[ ]:


from sklearn.linear_model import SGDClassifier

sgd_clf = SGDClassifier(max_iter=500, tol=1e-3, random_state=42,n_jobs=-1)
sgd_clf.fit(X_train, y_train_5)


# In[ ]:


sgd_clf.predict([some_digit])


# In[ ]:


from sklearn.model_selection import cross_val_score
cross_val_score(sgd_clf, X_train, y_train_5, cv=3,n_jobs=-1)


# In[ ]:


from sklearn.model_selection import StratifiedKFold
from sklearn.base import clone

skfolds = StratifiedKFold(n_splits=3, random_state=42)

for train_index, test_index in skfolds.split(X_train, y_train_5):
    clone_clf = clone(sgd_clf)
    X_train_folds = X_train[train_index]
    y_train_folds = y_train_5[train_index]
    X_test_fold = X_train[test_index]
    y_test_fold = y_train_5[test_index]

    clone_clf.fit(X_train_folds, y_train_folds)
    y_pred = clone_clf.predict(X_test_fold)
    n_correct = sum(y_pred == y_test_fold)
    print(n_correct / len(y_pred))


# In[ ]:


from sklearn.base import BaseEstimator
class Never5Classifier(BaseEstimator):
    def __init__(self,i=0):
        self.i=i
    def fit(self, X, y=None):
        pass
    def predict(self, X):
        return np.full((len(X), 1),self.i, dtype=bool)


# In[ ]:


never_5_clf = Never5Classifier()
cross_val_score(never_5_clf, X_train, y_train_5, cv=3, scoring="accuracy",n_jobs=-1)


# In[ ]:


get_ipython().run_line_magic('pinfo2', 'cross_val_predict')


# In[ ]:


from sklearn.model_selection import cross_val_predict

y_train_pred = cross_val_predict(sgd_clf, X_train_flat, y_train_5, cv=2,n_jobs=-1)


# In[ ]:


y_train_pred.shape


# In[ ]:


get_ipython().run_line_magic('pinfo', 'confusion_matrix')


# In[ ]:


from sklearn.metrics import confusion_matrix

confusion_matrix(y_train_5, y_train_pred)  / confusion_matrix(y_train_5, y_train_pred).sum(axis=1)


# In[ ]:


y_train_perfect_predictions = y_train_5  # pretend we reached perfection
confusion_matrix(y_train_5, y_train_perfect_predictions)


# In[ ]:


from sklearn.metrics import precision_score, recall_score

precision_score(y_train_5, y_train_pred)


# In[ ]:


4096 / (4096 + 1522)


# In[ ]:


recall_score(y_train_5, y_train_pred)


# In[ ]:


4096 / (4096 + 1325)


# In[ ]:


from sklearn.metrics import f1_score

f1_score(y_train_5, y_train_pred)


# In[ ]:


4096 / (4096 + (1522 + 1325) / 2)


# In[ ]:


y_scores = sgd_clf.decision_function([some_digit])
y_scores


# In[ ]:


threshold = 0
y_some_digit_pred = (y_scores > threshold)


# In[ ]:


y_some_digit_pred


# In[ ]:


threshold = 8000
y_some_digit_pred = (y_scores > threshold)
y_some_digit_pred


# In[ ]:


y_scores = cross_val_predict(sgd_clf, X_train, y_train_5, cv=3,
                             method="decision_function",n_jobs=-1)


# In[ ]:


y_scores


# In[ ]:


from sklearn.metrics import precision_recall_curve

precisions, recalls, thresholds = precision_recall_curve(y_train_5, y_scores)


# In[ ]:


def plot_precision_recall_vs_threshold(precisions, recalls, thresholds):
    plt.plot(thresholds, precisions[:-1], "b--", label="Precision", linewidth=2)
    plt.plot(thresholds, recalls[:-1], "g-", label="Recall", linewidth=2)
    plt.legend(loc="center right", fontsize=16) # Not shown in the book
    plt.xlabel("Threshold", fontsize=16)        # Not shown
    plt.grid(True)                              # Not shown
    plt.axis([-50000, 50000, 0, 1])             # Not shown

plt.figure(figsize=(8, 4))                      # Not shown
plot_precision_recall_vs_threshold(precisions, recalls, thresholds)
plt.plot([7813, 7813], [0., 0.9], "r:")         # Not shown
plt.plot([-50000, 7813], [0.9, 0.9], "r:")      # Not shown
plt.plot([-50000, 7813], [0.4368, 0.4368], "r:")# Not shown
plt.plot([7813], [0.9], "ro")                   # Not shown
plt.plot([7813], [0.4368], "ro")                # Not shown
save_fig("precision_recall_vs_threshold_plot")  # Not shown
plt.show()


# In[ ]:


(y_train_pred == (y_scores > 0)).all()


# In[ ]:


def plot_precision_vs_recall(precisions, recalls):
    plt.plot(recalls, precisions, "b-", linewidth=2)
    plt.xlabel("Recall", fontsize=16)
    plt.ylabel("Precision", fontsize=16)
    plt.axis([0, 1, 0, 1])
    plt.grid(True)

plt.figure(figsize=(8, 6))
plot_precision_vs_recall(precisions, recalls)
plt.plot([0.4368, 0.4368], [0., 0.9], "r:")
plt.plot([0.0, 0.4368], [0.9, 0.9], "r:")
plt.plot([0.4368], [0.9], "ro")
save_fig("precision_vs_recall_plot")
plt.show()


# In[ ]:


threshold_90_precision = thresholds[np.argmax(precisions >= 0.90)]


# In[ ]:


threshold_90_precision


# In[ ]:


y_train_pred_90 = (y_scores >= threshold_90_precision)


# In[ ]:


precision_score(y_train_5, y_train_pred_90)


# In[ ]:


recall_score(y_train_5, y_train_pred_90)


# ## ME

# ### study de base avec quelque models

# create or get study classif base

# In[10]:


studyGlobal_MNIST_BaseModels = studyGlobal_MNIST.addOrGetStudy("studyGlobal_MNIST_BaseModels_Sgd+LogReg",
                                                                StudyClassifProject,recreate=F)


# #### recreate 

# add datas from project 

# In[17]:


studyGlobal_MNIST_BaseModels.setDataTrainTest( id_="DATA_MNIST" )


# change imported data from project to data we want in the study

# In[18]:


def preproces_binary(X_train,y_train,X_test,y_test,names):
    y_train_5 = (y_train == 5)
    y_test_5 = (y_test == 5)
    names=["Pas5","5"]
    return (X_train,y_train_5,X_test,y_test_5,names)


# In[19]:


studyGlobal_MNIST_BaseModels.proprocessDataFromProject(fn=preproces_binary,force=T)


# add models to study

# In[20]:


from sklearn.dummy import DummyClassifier
dum1=DummyClassifier()
dum2=DummyClassifier("most_frequent")
mods=[dum1,dum2]


# In[21]:


studyGlobal_MNIST_BaseModels.setModels(models=mods) 


# compute cross-Validation with these models

# In[22]:


studyGlobal_MNIST_BaseModels.computeCV()


# save study for latter

# In[15]:


studyGlobal_MNIST_BaseModels.export(saveArgs=dict(recreate=T,chut=F))


# In[36]:


oo=importFromFile("studyGlobal_MNIST_BaseModels_Sgd+LogReg.study_study_classif_project.EXP.bz2")


# In[39]:


ee=StudyClassifProject.Import("studyGlobal_MNIST_BaseModels_Sgd+LogReg")


# 1.27 s RIEN MNIST (77.8)  [1]  
# 5 s bz2 MINST2  (10.5) [0.13]  
# 36.9 s lzma MNIST3 (10.1) [0.129]  
# 19.4 s gzip MNIST4 (12.2) (0.15)

# In[427]:


computeOrSaveAuto(studyGlobal_MNIST_BaseModels,auto=T,cv=3)


# save globalClassif

# ~30s

# 21s

# In[24]:


studyGlobal_MNIST.export(saveArgs=dict(recreate=T,chut=F)) 


# ####  analyse

# Analyse des classes

# In[11]:


cb_figData=studyGlobal_MNIST_BaseModels.train_datas.viz.plot_class_balance(returnData=T,allsize=20)


# In[18]:


(
    cb_figData.data.class_balance_percent 
    | __.to_frame().T
    | __.table_plot(returnFig=T,plotFig=F)
    | __.update_layout(width=500,font=dict(size=19))
)


# In[20]:


studyGlobal_MNIST_BaseModels.datas.viz.plot_class_balance()


# In[148]:


from sklearn.metrics import classification_report, confusion_matrix
import plotly.figure_factory as ff


# In[170]:


ff2=confusion_matrix(studyGlobal_MNIST_BaseModels.y_train,
                 studyGlobal_MNIST_BaseModels._cv | _fun_| studyDico | __[0].resultats | _fun_| studyDico | __[0].preds.Val.sorted)
uu=np.round(np.divide(ff2,ff2.sum(axis=1))[::-1],2)*100 |_fun_.pd.DataFrame(__,columns=["Pas5","5"]).set_axis(["Pas5","5"],inplace=F)


# In[184]:


import plotly.graph_objs as po


# In[185]:


help(po.Heatmap)


# In[207]:


ff.create_annotated_heatmap(uu.values, x=["Pas5",'`5`'],y=["Pas5",'`5`'],zmid=40,colorscale="Greys",showscale=T).update_layout(plot_bgcolor="black",width=500)


# In[134]:


import cufflinks as cf
import plotly.express as pe


# In[136]:


pe.histogram(uu)


# In[131]:


cf.datagen.heatmap(20,20).iplot(kind="heatmap")


# In[ ]:


classification_report(studyGlobal_MNIST_BaseModels._cv.)


# In[ ]:


studyMnistBaseModels     | _px.plot_classification_report(   figsize=(8,8),
                                            sameColorBar=T,
                                            show=F,
                                            returnOK=T  ) \
    | _p(IMG_GRID.grid,_px,figsize=(25,25))


# In[18]:


with HideWarningsTmp():
    if True:
        studyGlobal_MNIST_BaseModels             %pipe% __.plot_classification_report(   figsize=(5,5),
                                                    sameColorBar=T,
                                                    show=F,
                                                    returnOK=T,titleSize=15,fontsize=13  ) \
            %pipe% __.IMG_GRID.grid(__,figsize=(50,50))
    else:
        studyGlobal_MNIST_BaseModels.plot_classification_report(yellowBrick=T,
                                                                figsize=(7, 4),
                                                                show=F,
                                                                returnOK=T) \
            |pipe_map| __fc.get(1) \
            |pipe| __.IMG_GRID.grid(__,figsize=(25,25))


# SGD:  
# precision: les prédictions pour le classe 5 sont bonne à 76%  
# recall:  81% des observations de classe 5 sont détecté
# 
# LogReg:  
# precision: les prédictions pour le classe 5 sont bonne à 89%  
# recall:  81% des observations de classe 5 sont détecté

# In[19]:


studyGlobal_MNIST_BaseModels.plotConfusionMatrix(cmap=plt.cm.gray_r,returnOK=True,show=False) |pipe_map| __fc.get(0) | __.IMG_GRID.grid(__,figsize=(50,50)) 


# In[20]:


studyGlobal_MNIST_BaseModels.plotConfusionMatrix(show=F,returnOK=T,both=T,
                                                 figsize=(3,3),figsizeShow=(5,5)) \
    |pipe_map| __fc.get(0) | __.IMG_GRID.grid(__,figsize=(30,30))


# SGD:  
# 19% de classe 5 predite en Pas5  
# 3% de classe Pas5 predite en 5  
# 
# LogReg:  
# 19% de classe 5 predite en Pas5  
# 1% de classe Pas5 predite en 5   
# 

# In[ ]:


u=studyGlobal_MNIST_BaseModels.plot_classes_pred_erreurs(returnOK=T,
                                               show=F,
                                               figsize=(15,15))

u |pipe_map| __fc.get(1) |pipe| __.IMG_GRID.grid(__,elemsByRows=2,figsize=(50,50))

u |pipe_map| __fc.get(0) |pipe| __[0].predictions_ |pipe| print
u |pipe_map| __fc.get(0) |pipe| __[1].predictions_ |pipe| print


# le premier model n'a que decision_functions -> micro,per_class, macro -> None  
# le deuxieme a predict_proba -> micro,per_class, macro -> au moins un different de None  

# In[21]:


o=studyGlobal_MNIST_BaseModels.plot_roc_auc(m=[0],show=F,returnOK=T,figsize=(64,35),size=(640,350),dpi=150)
oo=studyGlobal_MNIST_BaseModels.plot_roc_auc(m=[1],per_class=T,micro=T,show=F,returnOK=T,figsize=(100,100),size=(640,350),dpi=150)
o + oo |pipe_map| __fc.get(1) | __.IMG_GRID.grid(__,elemsByRows=2,figsize=(8,8),dpi=400)


# In[270]:


studyGlobal_MNIST_BaseModels.plot_precision_recall_curves(figsize=(10,10),
                                                           noSVG=T,
                                                           show=F,
                                                           returnOK=T) \
    |pipe_map| __fc.get(1) | __.IMG_GRID.grid(__,figsize=(25,15),elemsByRows=2)


# c'est excellent comparons avec un dummy 

# A macro-average will compute the metric independently for each class and then take the average (hence treating all classes equally),  
# A micro-average will aggregate the contributions of all classes to compute the average metric  
# In multi-class classification setup, micro-average is preferable if you suspect there might be class imbalance

# ### Study base + dummy 

# In[16]:


studyGlobal_MNIST_BaseModels2=studyGlobal_MNIST.addOrGetStudy("studyGlobal_MNIST_BaseModels_+Dummy",
                                                              studyGlobal_MNIST_BaseModels.clone,
                                                              recreate=T)


# #### create 

# new study with dummy

# In[17]:


from sklearn.linear_model import SGDClassifier,LogisticRegression
sgd_clf = SGDClassifier(max_iter=50,
                        tol=1e-3,
                        random_state=42,
                        n_jobs=-1)
log_clf = LogisticRegression(n_jobs=-1,
                             solver = 'sag',tol=0.4)
mods=[sgd_clf,log_clf]


# In[18]:


studyGlobal_MNIST_BaseModels2.addMethodsToCurrCV(models=mods)


# In[43]:


computeOrSaveAuto(studyGlobal_MNIST_BaseModels2,cv=3,models=F if not recreate_ and clone_ else mods,auto=T)


# 30s

# In[771]:


studyGlobal_MNIST.save()


# On crée ou on recupere la study  
# si on le crée, on clone sur l'ancienne study et ensuite on ajoute la cv pour les nouveaux models

# ####  analyse

# In[44]:


studyGlobal_MNIST_BaseModels2_=studyGlobal_MNIST_BaseModels2


# On va la study global

# In[46]:


studyGlobal_MNIST_BaseModels2.plot_classification_report(show=False,
                                                          returnOK=True,figsize=(4,4)) \
        %pipe% __.IMG_GRID.grid(__,elemsByRows=2,figsize=(20,20))


# sgd et logReg -> same  
# Dummy:  
# precision: les prédictions pour le classe 5 sont bonne à 9%  
# recall: 9% des observations de classe 5 sont détecté

# In[47]:


er=studyGlobal_MNIST_BaseModels2.plotConfusionMatrix(
                                         show=F,returnOK=T,both=T,
                                         figsize=(3,3),figsizeShow=(3,3),fontsize=13,titleSize=15,osefLine=True) \
    |pipe_map| __fc.get(0) | __.IMG_GRID.grid(__,elemsByRows=2,figsize=(40,30))


# In[48]:


o=studyGlobal_MNIST_BaseModels2.plot_roc_auc(m=[0],show=F,returnOK=T,figsize=(100,120),dpi=150)
oo=studyGlobal_MNIST_BaseModels2.plot_roc_auc(m=[1,2],per_class=T,micro=T,show=F,returnOK=T,dpi=150)
o + oo |pipe_map| __fc.get(1) | __.IMG_GRID.grid(__,elemsByRows=2,figsize=(10,6),dpi=400)


# In[278]:


studyGlobal_MNIST_BaseModels2.plot_precision_recall_curves(figsize=(10,10),
                                                           noSVG=T,
                                                           show=F,
                                                           returnOK=T) \
    |pipe_map| __fc.get(1) | __.IMG_GRID.grid(__,figsize=(25,15),elemsByRows=2)


# On voit que comme attendu la Prc pour Dummy est nul  
# logReg mieux en moyenne pour la precision que logReg

# Regardons les Erreurs communes

# In[107]:


me=studyGlobal_MNIST_BaseModels2

me2=me.getModelsSameErrors(pct=True).T
me2    | __.np.round(__,decimals=2)     | __.plt.imshow(__,cmap=plt.cm.gray)
plt.xticks(range(len(me.names)),me.names,ha="right",rotation=45)
plt.yticks(range(len(me.names)),me.names)
rr=Normalize(vmin=me2.values.min(),vmax=me2.values.max())
for i in range(me2.shape[0]):
    for j in range(me2.shape[0]):
        if me2.values[i,j]==0:
            continue
        plt.text(j,i,np.round(me2.values[i,j],2),horizontalalignment='center',
                 verticalalignment='center',color=frontColorFromCmapAndValue(rr(me2.values[i,j]),plt.cm.gray))
plt.colorbar();


# In[108]:


studyGlobal_MNIST_BaseModels2.most_confused(percent=T,roundVal=3)


# In[109]:


studyGlobal_MNIST_BaseModels2.most_confused_global(percent=T,roundVal=3)


# In[295]:


plt.figure(figsize=(1,1))
plt.text(0,0,"Obs pas en erreur en sgd et logReg",fontsize=20)
plt.axis("off")
studyClassif_Img._plotDigits(
    studyGlobal_MNIST_BaseModels2.X_train[studyGlobal_MNIST_BaseModels2.getObsNotErrors(0,1)],lim=50)


# In[296]:


plt.figure(figsize=(1,1))
plt.text(0,0,"Obs en erreur en sgd et logReg",fontsize=20)
plt.axis("off")

studyGlobal_MNIST_BaseModels2  |pipe| (lambda a: a.X_train[a.getObsErrors(0,1)])  |  __.studyClassif_Img._plotDigits(__,lim=50)


# In[ ]:


studyMnist2_=computeOrSave(studyMnist2,
                           if_exist_global('studyMnist2_'),
                           cv=3,
                           name="studyMnist2_cv3")


# In[297]:


plt.figure(figsize=(1,1))
plt.text(0,0,"Classe '5' prédite 'Pas5'",fontsize=20)
plt.axis("off")
studyGlobal_MNIST.getCurr.getObsConfused(T,F)      | __.items()     |pipe_| list     |pipe_map| __.studyClassif_Img._plotDigits(instances=__[1],title=__[0],titleSize=15);
plt.show()


# In[298]:


plt.figure(figsize=(1,1))
plt.text(0,0,"Classe 'Pas5' prédite '5'",fontsize=20)
plt.axis("off")
studyGlobal_MNIST.getCurr.getObsConfused(F,T)      %pipe% __.items()     %pipe_% list     %pipe_map% __.studyClassif_Img._plotDigits(instances=__[1],title=__[0],titleSize=15);


# In[110]:


getRes1=lambda i:studyGlobal_MNIST_BaseModels2.getCV()[1]["scores"][i]                 | __.pd.DataFrame(__,
                                       columns=studyGlobal_MNIST_BaseModels2._names,
                                       index=range(1,4) |pipe_map| "CV {}".format).T \
                | __.np.round(__,decimals=2) \
                | __.mean(axis=1).to_frame().T | __.rename(index={0:i})


# In[121]:


getRes1("Tr").append(getRes1("Val"))  | __.np.round(__,decimals=2)


# ###  OLD

# In[ ]:


study_MNIST = studyClassif(sgd_clf,
                           X_test=X_test,y_test=y_test_5,
                           X_train=X_train,y_train=y_train_5,
                           names=["Pas5","5"])


# In[ ]:


get_ipython().run_line_magic('load_ext', 'line_profiler')


# In[ ]:


get_ipython().run_line_magic('lprun', '-f cross_val_score2 study_MNIST.cross_val_score2(X_train,y_train_5,cv=1,verbose=1)')


# In[ ]:


studyMnist.visualize_model(studyMnist._models[0])


# In[ ]:


studyMnist.visualize_model(studyMnist._models[0],fn=ConfusionMatrix)


# In[ ]:


studyMnist.plot_classification_report(globally=T,predict=True)


# In[ ]:


studyMnist.plot_conf_matrix(predict=True)


# In[ ]:


study_MNIST.plot_classification_report(globally=F,predict=True)


# In[ ]:


study_MNIST_SGD=study_MNIST.cross_val_score2(cv=3,returnNewStudy=True)


# In[ ]:


study_MNIST_SGD.plot_classification_report(globally=T)


# In[ ]:


study_MNIST_SGD.plot_classification_report(globally=F)


# In[ ]:


from sklearn.base import BaseEstimator,ClassifierMixin
class Never5Classifier(BaseEstimator,ClassifierMixin):
    def __init__(self,i=0):
        self.i=i
    def fit(self, X, y=None):
        pass
    def predict(self, X):
        return np.full((len(X), 1),self.i, dtype=bool)


# In[ ]:


nev_clf=Never5Classifier()


# In[ ]:


study_MNIST_SGD_Never=study_MNIST.addModels(nev_clf,inplace=False)


# In[ ]:


study_MNIST_SGD_Never.cross_val_score2(cv=3)


# In[ ]:


study_MNIST_SGD_Never.plot_classification_report(globally=F,predict=True)


# In[ ]:


study_MNIST_SGD_Never.plot_conf_matrix(predict=T)


# In[ ]:


visualize_model(study_MNIST.X_train,study_MNIST.y_train,
                study_MNIST.X_test,study_MNIST.y_test,
                study_MNIST_SGD_Never.models[0],names=["Pas5","5"],fn=ConfusionMatrix,cmap="Blues")


# In[ ]:


visualize_model(study_MNIST.X_train,study_MNIST.y_train,
                study_MNIST.X_test,study_MNIST.y_test,
                study_MNIST_SGD_Never.models[0],names=["Pas5","5"],fn=ClassPredictionError,cmap="Blues")


# In[ ]:


from sklearn.linear_model import LogisticRegression


# In[ ]:


logreg_clf = LogisticRegression()


# In[ ]:


study_MNIST_Log=study_MNIST_SGD_Never.updateModels(logreg_clf)


# # ROC curves

# ## M2 

# In[19]:


studyGlobal_MNIST_BaseModels3 = studyGlobal_MNIST.addOrGetStudy('studyGlobal_MNIST_BaseModels_+rf',
                                                               studyGlobal_MNIST_BaseModels2.clone,
                                                               recreate=T)


# ### create

# In[20]:


from sklearn.ensemble import RandomForestClassifier
rnd_cld = RandomForestClassifier(n_jobs=-1,n_estimators=50)
mods=[rnd_cld]


# In[21]:


studyGlobal_MNIST_BaseModels3.addMethodsToCurrCV(models=mods)


# In[37]:


studyGlobal_MNIST_BaseModels3.setModels(studyGlobal_MNIST_BaseModels3.models+mods)


# In[22]:


sv=studyGlobal_MNIST.export()


# In[129]:


computeOrSaveAuto(studyGlobal_MNIST_BaseModels3,cv=3,models=F if not recreate_ and clone_ else mods,auto=T)


# 30s
# 

# In[776]:


studyGlobal_MNIST.save()


# ###  analyse

# In[130]:


studyGlobal_MNIST_BaseModels3_ =studyGlobal_MNIST_BaseModels3


# In[131]:


studyGlobal_MNIST_BaseModels3.plot_roc_auc(m=[-1],per_class=T,dpi=120,noSVG=T,figsize=(7,7))


# In[125]:


studyGlobal_MNIST_BaseModels3.plot_precision_recall_curves(m=[-1],dpi=100,noSVG=T,figsize=(7,7))


# In[ ]:


mplResetDefault()


# In[132]:


studyGlobal_MNIST_BaseModels3.plot_classification_report(returnOK=T,show=F) | __.IMG_GRID.grid(__,elemsByRows=2,figsize=(18,20))


# In[134]:


pyMat2=studyGlobal_MNIST_BaseModels3.getModelsSameErrors(pct=T).T

pyMat2  | __.plt.imshow(__,cmap="gray")
pyMat=pyMat2.values
plt.colorbar()
classes=studyGlobal_MNIST_BaseModels3.names
tick_marks = np.arange(len(classes))
plt.yticks(tick_marks,classes,rotation=0)
plt.xticks(tick_marks,classes,rotation=45,ha="right");
rr=Normalize(vmin=pyMat.min(),vmax=pyMat.max())
for i in range(pyMat.shape[0]):
    for j in range(pyMat.shape[1]):
        if pyMat[i, j]==0.0:
            continue
        plt.text(j, i, np.round(pyMat[i, j],2),
                ha="center", va="center", color=frontColorFromCmapAndValue(rr(pyMat[i, j]),plt.cm.gray))


# ## Orig

# In[ ]:


from sklearn.metrics import roc_curve

fpr, tpr, thresholds = roc_curve(y_train_5, y_scores)


# In[ ]:


def plot_roc_curve(fpr, tpr, label=None):
    plt.plot(fpr, tpr, linewidth=2, label=label)
    plt.plot([0, 1], [0, 1], 'k--') # dashed diagonal
    plt.axis([0, 1, 0, 1])                                    # Not shown in the book
    plt.xlabel('False Positive Rate (Fall-Out)', fontsize=16) # Not shown
    plt.ylabel('True Positive Rate (Recall)', fontsize=16)    # Not shown
    plt.grid(True)                                            # Not shown

plt.figure(figsize=(8, 6))                         # Not shown
plot_roc_curve(fpr, tpr)
plt.plot([4.837e-3, 4.837e-3], [0., 0.4368], "r:") # Not shown
plt.plot([0.0, 4.837e-3], [0.4368, 0.4368], "r:")  # Not shown
plt.plot([4.837e-3], [0.4368], "ro")               # Not shown
save_fig("roc_curve_plot")                         # Not shown
plt.show()


# In[ ]:


from sklearn.metrics import roc_auc_score

roc_auc_score(y_train_5, y_scores)


# **Note**: we set `n_estimators=100` to be future-proof since this will be the default value in Scikit-Learn 0.22.

# In[ ]:


from sklearn.ensemble import RandomForestClassifier
forest_clf = RandomForestClassifier(n_estimators=100, random_state=42,n_jobs=-1)
y_probas_forest = cross_val_predict(forest_clf, X_train, y_train_5, cv=3,
                                    method="predict_proba",n_jobs=-1)


# In[ ]:


y_scores_forest = y_probas_forest[:, 1] # score = proba of positive class
fpr_forest, tpr_forest, thresholds_forest = roc_curve(y_train_5,y_scores_forest)


# In[ ]:


plt.figure(figsize=(8, 6))
plt.plot(fpr, tpr, "b:", linewidth=2, label="SGD")
plot_roc_curve(fpr_forest, tpr_forest, "Random Forest")
plt.plot([4.837e-3, 4.837e-3], [0., 0.4368], "r:")
plt.plot([0.0, 4.837e-3], [0.4368, 0.4368], "r:")
plt.plot([4.837e-3], [0.4368], "ro")
plt.plot([4.837e-3, 4.837e-3], [0., 0.9487], "r:")
plt.plot([4.837e-3], [0.9487], "ro")
plt.grid(True)
plt.legend(loc="lower right", fontsize=16)
save_fig("roc_curve_comparison_plot")
plt.show()


# In[ ]:


roc_auc_score(y_train_5, y_scores_forest)


# In[ ]:


y_train_pred_forest = cross_val_predict(forest_clf, X_train, y_train_5, cv=3,n_jobs=-1)
precision_score(y_train_5, y_train_pred_forest)


# In[ ]:


recall_score(y_train_5, y_train_pred_forest)


# # Multiclass classification

# ## ME

# In[135]:


recreate_=F
clone_=F
studyGlobal_MNIST_MultiModels = studyGlobal_MNIST.addOrGetStudy("studyGlobal_MNIST_MultiModels_svc",
                                                              StudyClassif,
                                                                recreate=recreate_,
                                                                clone=clone_) 


# ### create

# In[501]:


studyGlobal_MNIST_MultiModels.setDataTrainTest( id_="DATA_MNIST" )


# In[ ]:


def preprocessMultiClass(x1,x2,y1,y2,n,random_state=42,nb=4000):
    x1,_,y1,_=train_test_split(x1,y1,random_state=random_state,train_size=nb)
    return (x1,x2,y1,y2,None)


# In[502]:


studyGlobal_MNIST_MultiModels.preprocessingData(fn=__fc.partial(preprocessMultiClass,nb=2500))


# In[503]:


from sklearn.svm import SVC
svm_clf = SVC( kernel='linear', random_state=42,probability=T,max_iter=1000)
mods=[svm_clf]


# In[504]:


studyGlobal_MNIST_MultiModels.setModels(models=mods) 


# In[505]:


computeOrSaveAuto(studyGlobal_MNIST_MultiModels,cv=3,auto=T)


# 40 s

# In[784]:


studyGlobal_MNIST.save()


# ### analyse

# In[136]:


studyGlobal_MNIST_MultiModels_=studyGlobal_MNIST_MultiModels


# In[137]:


mplResetDefault()


# In[138]:


studyGlobal_MNIST_MultiModels.plot_classe_balance(figsizeShow=(5,1),fontsize=0,noSVG=T)


# In[140]:


studyGlobal_MNIST_MultiModels.plot_classification_report(figsize=(6,6),fontsize=0,returnOK=T,show=F,noText=T) | __[0].show(figsize=(1.5,1.5))


# In[ ]:


studyGlobal_MNIST_MultiModels.plotConfusionMatrix(both=T,fontsize=10,
                                                  figsize=(10,10),figsizeShow=(10,10),modulo=250)


# #### blabla

# In[506]:


clone_=T
recreate_=T
studyGlobal_MNIST_MultiModels2 = studyGlobal_MNIST.addOrGetStudy("studyGlobal_MNIST_MultiModels_+ovo_svc",
                                                          studyGlobal_MNIST_MultiModels.clone,
                                                                 recreate=recreate_,
                                                                 clone=clone_)


# In[ ]:


from sklearn.multiclass import OneVsRestClassifier,OneVsOneClassifier
n_estimators = 10
ovr_clf = OneVsOneClassifier(SVC(kernel='linear', random_state=42, probability=True,max_iter=1000),n_jobs=-1)
mods=[ovr_clf]


# In[507]:


computeOrSaveAuto(studyGlobal_MNIST_MultiModels2,cv=3,models=F if not recreate_ and clone_ else mods,auto=T)


# In[802]:


studyGlobal_MNIST2.save(returnOK=F)


# #####  analyse

# In[161]:


studyGlobal_MNIST_MultiModels2.plotConfusionMatrix(both=T,fontsize=10,
                                                  figsize=(10,10),figsizeShow=(10,10),modulo=250)


# #### RF 

# In[509]:


clone_=T
recreate_=F
studyGlobal_MNIST_MultiModels3 = studyGlobal_MNIST.addOrGetStudy("studyGlobal_MNIST_MultiModels_+RF",
                                                          studyGlobal_MNIST_MultiModels2.clone,
                                                                 recreate=recreate_,
                                                                 clone=clone_)


# In[510]:


rf_clf=RandomForestClassifier(n_jobs=-1)
mods=[rf_clf]


# In[511]:


computeOrSaveAuto(studyGlobal_MNIST_MultiModels3,cv=3,
                  models=F if not recreate_ and clone_ else mods,
                  auto=T)


# In[245]:


studyGlobal_MNIST.save()


# In[ ]:





# In[183]:


studyGlobal_MNIST_MultiModels3_=studyGlobal_MNIST_MultiModels3


# In[517]:


studyGlobal_MNIST_MultiModels3.plot_classification_report(figsize=(2.5,2.5),noText=T,rotationXLabel=90,returnOK=T,show=F)      | __.IMG_GRID.grid(__,figsize=(25,25))


# In[518]:


with HideWarningsTmp():
    studyGlobal_MNIST_MultiModels3.plotConfusionMatrix(both=T,fontsize=0,figsize=(2,2),linewidth=2.5,modulo=-75,long2=-165,long=80,noText=True,returnOK=T,show=F)         |pipe_map| __fc.get(0) | __.IMG_GRID.grid(__,elemsByRows=3,figsize=(30,60))


# In[519]:


pyMat2=studyGlobal_MNIST_MultiModels3.getModelsSameErrors(pct=T).T

pyMat2  | __.plt.imshow(__,cmap="gray")
pyMat=pyMat2.values
plt.colorbar()
classes=studyGlobal_MNIST_MultiModels3.names
tick_marks = np.arange(len(classes))
plt.yticks(tick_marks,classes,rotation=0)
plt.xticks(tick_marks,classes,rotation=45,ha="right");
for i in range(pyMat.shape[0]):
    for j in range(pyMat.shape[1]):
        plt.text(j, i, np.round(pyMat[i, j],2),
                ha="center", va="center", color=frontColorFromCmapAndValue(pyMat[i, j],plt.cm.gray))


# In[613]:


fig,ax=plt.subplots(len(studyGlobal_MNIST.getCurr.models),figsize=(10,5), constrained_layout=True)
axs=ax.flatten()
fig.suptitle("Classe '5' prédite en '3'",fontsize=25)
#print(axs[1:])
zipl(studyGlobal_MNIST.getCurr.getObsConfused(5,3)      | __.items()     |pipe_| list,axs[0:])    |pipe_map| __.studyClassif_Img._plotDigits(instances=__[0][1],title=__[0][0],titleSize=15,ax=__[1],noImg=T);
#plt.show()


# In[616]:


fig,ax=plt.subplots(len(studyGlobal_MNIST.getCurr.models),figsize=(10,5), constrained_layout=True)
axs=ax.flatten()
fig.suptitle("Classe '5' prédite en '5'",fontsize=25)
#print(axs[1:])
zipl(studyGlobal_MNIST.getCurr.getObsConfused(5,5)      | __.items()     |pipe_| list,axs[0:])    |pipe_map| __.studyClassif_Img._plotDigits(instances=__[0][1],title=__[0][0],titleSize=15,ax=__[1],noImg=T);
#plt.show()


# In[615]:


fig,ax=plt.subplots(len(studyGlobal_MNIST.getCurr.models),figsize=(10,5), constrained_layout=True)
axs=ax.flatten()
fig.suptitle("Classe '3' prédite en '5'",fontsize=25)
#print(axs[1:])
zipl(studyGlobal_MNIST.getCurr.getObsConfused(3,5)      | __.items()     |pipe_| list,axs[0:])    |pipe_map| __.studyClassif_Img._plotDigits(instances=__[0][1],title=__[0][0],titleSize=15,ax=__[1],noImg=T);
#plt.show()


# In[617]:


fig,ax=plt.subplots(len(studyGlobal_MNIST.getCurr.models),figsize=(10,5), constrained_layout=True)
axs=ax.flatten()
fig.suptitle("Classe '3' prédite en '3'",fontsize=25)
#print(axs[1:])
zipl(studyGlobal_MNIST.getCurr.getObsConfused(3,3)      | __.items()     |pipe_| list,axs[0:])    |pipe_map| __.studyClassif_Img._plotDigits(instances=__[0][1],title=__[0][0],titleSize=15,ax=__[1],noImg=T);
#plt.show()


# In[621]:


fig,ax=plt.subplots(figsize=(10,5), constrained_layout=True)
fig.suptitle("Erreur entre 'SVC' et 'oVo'",fontsize=25);
studyClassif_Img._plotDigits(
    studyGlobal_MNIST_MultiModels3.X_train[studyGlobal_MNIST_MultiModels3.getObsErrors(0,1)],lim=50,ax=ax,noImg=T)


# In[622]:


fig,ax=plt.subplots(figsize=(10,5), constrained_layout=True)
fig.suptitle("Erreur entre 'SVC' et 'RF'",fontsize=25);
studyClassif_Img._plotDigits(
    studyGlobal_MNIST_MultiModels3.X_train[studyGlobal_MNIST_MultiModels3.getObsErrors(0,2)],lim=50,ax=ax,noImg=T)


# In[623]:


fig,ax=plt.subplots(figsize=(10,5), constrained_layout=True)
fig.suptitle("Erreur entre 'oVo' et 'RF'",fontsize=25);
studyClassif_Img._plotDigits(
    studyGlobal_MNIST_MultiModels3.X_train[studyGlobal_MNIST_MultiModels3.getObsErrors(1,2)],lim=50,ax=ax,noImg=T)


# In[614]:


studyGlobal_MNIST.save()


# ##  Orig

# In[35]:


from sklearn.svm import SVC

svm_clf = SVC(kernel="linear", random_state=42)
svm_clf.fit(X_train[:1000], y_train[:1000]) # y_train, not y_train_5
svm_clf.predict([some_digit])


# In[366]:


some_digit_scores = svm_clf.decision_function([some_digit])
some_digit_scores


# In[367]:


np.argmax(some_digit_scores)


# In[364]:


svm_clf.classes_


# In[365]:


svm_clf.classes_[5]


# In[ ]:


from sklearn.multiclass import OneVsRestClassifier
ovr_clf = OneVsRestClassifier(SVC(gamma="auto", random_state=42),n_jobs=-1)
ovr_clf.fit(X_train[:1000], y_train[:1000])
ovr_clf.predict([some_digit])


# In[ ]:


len(ovr_clf.estimators_)


# In[ ]:


sgd_clf.fit(X_train, y_train)
sgd_clf.predict([some_digit])


# In[ ]:


sgd_clf.decision_function([some_digit])


# In[ ]:


cross_val_score(sgd_clf, X_train, y_train, cv=3, scoring="accuracy",n_jobs=-1)


# In[ ]:


from sklearn.preprocessing import StandardScaler
scaler = StandardScaler()
X_train_scaled = scaler.fit_transform(X_train.astype(np.float64))
cross_val_score(sgd_clf, X_train_scaled, y_train, cv=3, scoring="accuracy",n_jobs=-1)


# In[ ]:


y_train_pred = cross_val_predict(sgd_clf, X_train_scaled, y_train, cv=3,n_jobs=-1)
conf_mx = confusion_matrix(y_train, y_train_pred)
conf_mx


# In[ ]:


def plot_confusion_matrix(matrix):
    """If you prefer color and a colorbar"""
    fig = plt.figure(figsize=(8,8))
    ax = fig.add_subplot(111)
    cax = ax.matshow(matrix)
    fig.colorbar(cax)


# In[ ]:


plt.matshow(conf_mx, cmap=plt.cm.gray)
save_fig("confusion_matrix_plot", tight_layout=False)
plt.show()


# In[ ]:


row_sums = conf_mx.sum(axis=1, keepdims=True)
norm_conf_mx = conf_mx / row_sums


# In[ ]:


np.fill_diagonal(norm_conf_mx, 0)
plt.matshow(norm_conf_mx, cmap=plt.cm.gray)
save_fig("confusion_matrix_errors_plot", tight_layout=False)
plt.show()


# In[ ]:


cl_a, cl_b = 3, 5
X_aa = X_train[(y_train == cl_a) & (y_train_pred == cl_a)]
X_ab = X_train[(y_train == cl_a) & (y_train_pred == cl_b)]
X_ba = X_train[(y_train == cl_b) & (y_train_pred == cl_a)]
X_bb = X_train[(y_train == cl_b) & (y_train_pred == cl_b)]

plt.figure(figsize=(8,8))
plt.subplot(221); plot_digits(X_aa[:25], images_per_row=5)
plt.subplot(222); plot_digits(X_ab[:25], images_per_row=5)
plt.subplot(223); plot_digits(X_ba[:25], images_per_row=5)
plt.subplot(224); plot_digits(X_bb[:25], images_per_row=5)
save_fig("error_analysis_digits_plot")
plt.show()


# # Multilabel classification

# ## ME

# Not Implemented -> scikit-multilearn

# In[ ]:


#del studyGlobal_MNIST.studies["studyGlobal_MNIST_MultiLabel"]


# In[ ]:


studyGlobal_MNIST_MultiLabel = studyGlobal_MNIST.addOrGetStudy("studyGlobal_MNIST_MultiLabel",StudyClassif,T)


# In[ ]:


studyGlobal_MNIST_MultiLabel.ifNotReady( __.setDataTrainTest(id_="DATA_MNIST"))


# In[ ]:


def multilab_preprocess(x1,x2,y1,y2,n,random_state=42,nb=10000):
    res=np.random.RandomState(random_state)
    inds=res.choice(range(len(x1)),10000)
    train_test_split(x1,x1)
    x1=x1[inds]
    y1=y1[inds]
    y_train_large = (y1 >= 7)
    y_train_odd = (y1 % 2 == 1)
    y_multilabel = np.c_[y_train_large, y_train_odd]
    y_test_large = (y2 >= 7)
    y_test_odd = (y2 % 2 == 1)
    y_multilabel_test = np.c_[y_test_large, y_test_odd]
    return (x1,x2,y_multilabel,y_multilabel_test,[">=7","impair"])


# In[ ]:


#studyGlobal_MNIST_MultiLabel.isReady=False


# In[ ]:


studyGlobal_MNIST_MultiLabel.ifNotReady( 
    studyGlobal_MNIST_MultiLabel.preprocessingData(__fc.curry(multilab_preprocess)(nb=5000))
)


# In[ ]:


from sklearn.neighbors import KNeighborsClassifier
knn_clf = KNeighborsClassifier(n_jobs=-1)
from sklearn.neighbors import KNeighborsClassifier
knn_clf2 = KNeighborsClassifier(n_jobs=-1,n_neighbors=10)


# In[ ]:


studyGlobal_MNIST_MultiLabel.ifNotReady(
    studyGlobal_MNIST_MultiLabel.setModels([knn_clf,knn_clf2])
)


# In[ ]:


computeOrSaveAuto(studyGlobal_MNIST_MultiLabel,cv=3)


# In[ ]:


studyGlobal_MNIST.save()


# ## Origg

# In[ ]:


from sklearn.neighbors import KNeighborsClassifier

y_train_large = (y_train >= 7)
y_train_odd = (y_train % 2 == 1)
y_multilabel = np.c_[y_train_large, y_train_odd]

knn_clf = KNeighborsClassifier(n_jobs=-1)
knn_clf.fit(X_train, y_multilabel)


# In[ ]:


knn_clf.predict([some_digit])


# **Warning**: the following cell may take a very long time (possibly hours depending on your hardware).

# In[ ]:


y_train_knn_pred = cross_val_predict(knn_clf, X_train, y_multilabel, cv=3)
f1_score(y_multilabel, y_train_knn_pred, average="macro")


# # Multioutput classification

# Not Implemented -> scikit-multilearn

# In[ ]:


noise = np.random.randint(0, 100, (len(X_train_flat), 784))
X_train_mod = X_train_flat + noise
noise = np.random.randint(0, 100, (len(X_test_flat), 784))
X_test_mod = X_test_flat + noise
y_train_mod = X_train_flat
y_test_mod = X_test_flat


# In[ ]:


some_index = 0
plt.subplot(121); plot_digit(X_test_mod[some_index])
plt.subplot(122); plot_digit(y_test_mod[some_index])
save_fig("noisy_digit_example_plot")
plt.show()


# In[ ]:


knn_clf.fit(X_train_mod, y_train_mod)
clean_digit = knn_clf.predict([X_test_mod[some_index]])
plot_digit(clean_digit)
save_fig("cleaned_digit_example_plot")


# # Extra material

# ## Dummy (ie. random) classifier

# In[ ]:


from sklearn.dummy import DummyClassifier
dmy_clf = DummyClassifier()
y_probas_dmy = cross_val_predict(dmy_clf, X_train, y_train_5, cv=3, method="predict_proba")
y_scores_dmy = y_probas_dmy[:, 1]


# In[ ]:


fprr, tprr, thresholdsr = roc_curve(y_train_5, y_scores_dmy)
plot_roc_curve(fprr, tprr)


# ## KNN classifier

# In[ ]:


from sklearn.neighbors import KNeighborsClassifier
knn_clf = KNeighborsClassifier(weights='distance', n_neighbors=4,n_jobs=-1)
knn_clf.fit(X_train, y_train)


# In[ ]:


y_knn_pred = knn_clf.predict(X_test)


# In[ ]:


from sklearn.metrics import accuracy_score
accuracy_score(y_test, y_knn_pred)


# In[ ]:


from scipy.ndimage.interpolation import shift
def shift_digit(digit_array, dx, dy, new=0):
    return shift(digit_array.reshape(28, 28), [dy, dx], cval=new).reshape(784)

plot_digit(shift_digit(some_digit, 5, 1, new=100))


# In[ ]:


X_train_expanded = [X_train]
y_train_expanded = [y_train]
for dx, dy in ((1, 0), (-1, 0), (0, 1), (0, -1)):
    shifted_images = np.apply_along_axis(shift_digit, axis=1, arr=X_train, dx=dx, dy=dy)
    X_train_expanded.append(shifted_images)
    y_train_expanded.append(y_train)

X_train_expanded = np.concatenate(X_train_expanded)
y_train_expanded = np.concatenate(y_train_expanded)
X_train_expanded.shape, y_train_expanded.shape


# In[ ]:


knn_clf.fit(X_train_expanded, y_train_expanded)


# In[ ]:


y_knn_expanded_pred = knn_clf.predict(X_test)


# In[ ]:


accuracy_score(y_test, y_knn_expanded_pred)


# In[ ]:


ambiguous_digit = X_test[2589]
knn_clf.predict_proba([ambiguous_digit])


# In[ ]:


plot_digit(ambiguous_digit)


# # Exercise solutions

# ## 1. An MNIST Classifier With Over 97% Accuracy

# In[4]:





# In[ ]:





# In[ ]:





# ### Orig 

# **Warning**: the next cell may take hours to run, depending on your hardware.

# In[ ]:


from sklearn.model_selection import GridSearchCV

param_grid = [{'weights': ["uniform", "distance"], 'n_neighbors': [3, 4, 5]}]

knn_clf = KNeighborsClassifier()
grid_search = GridSearchCV(knn_clf, param_grid, cv=5, verbose=3)
#grid_search.fit(X_train, y_train)


# In[ ]:


grid_search.best_params_


# In[ ]:


grid_search.best_score_


# In[ ]:


from sklearn.metrics import accuracy_score

y_pred = grid_search.predict(X_test)
accuracy_score(y_test, y_pred)


# ## 2. Data Augmentation

# In[ ]:


from scipy.ndimage.interpolation import shift


# In[ ]:


def shift_image(image, dx, dy):
    image = image.reshape((28, 28))
    shifted_image = shift(image, [dy, dx], cval=0, mode="constant")
    return shifted_image.reshape([-1])


# In[ ]:


image = X_train[1000]
shifted_image_down = shift_image(image, 0, 5)
shifted_image_left = shift_image(image, -5, 0)

plt.figure(figsize=(12,3))
plt.subplot(131)
plt.title("Original", fontsize=14)
plt.imshow(image.reshape(28, 28), interpolation="nearest", cmap="Greys")
plt.subplot(132)
plt.title("Shifted down", fontsize=14)
plt.imshow(shifted_image_down.reshape(28, 28), interpolation="nearest", cmap="Greys")
plt.subplot(133)
plt.title("Shifted left", fontsize=14)
plt.imshow(shifted_image_left.reshape(28, 28), interpolation="nearest", cmap="Greys")
plt.show()


# In[ ]:


X_train_augmented = [image for image in X_train]
y_train_augmented = [label for label in y_train]

for dx, dy in ((1, 0), (-1, 0), (0, 1), (0, -1)):
    for image, label in zip(X_train, y_train):
        X_train_augmented.append(shift_image(image, dx, dy))
        y_train_augmented.append(label)

X_train_augmented = np.array(X_train_augmented)
y_train_augmented = np.array(y_train_augmented)


# In[ ]:


shuffle_idx = np.random.permutation(len(X_train_augmented))
X_train_augmented = X_train_augmented[shuffle_idx]
y_train_augmented = y_train_augmented[shuffle_idx]


# In[ ]:


knn_clf = KNeighborsClassifier(**grid_search.best_params_)


# In[ ]:


knn_clf.fit(X_train_augmented, y_train_augmented)


# In[ ]:


y_pred = knn_clf.predict(X_test)
accuracy_score(y_test, y_pred)


# By simply augmenting the data, we got a 0.5% accuracy boost. :)

# ## 3. Tackle the Titanic dataset

# The goal is to predict whether or not a passenger survived based on attributes such as their age, sex, passenger class, where they embarked and so on.

# First, login to [Kaggle](https://www.kaggle.com/) and go to the [Titanic challenge](https://www.kaggle.com/c/titanic) to download `train.csv` and `test.csv`. Save them to the `datasets/titanic` directory.

# Next, let's load the data:

# In[69]:


import os

TITANIC_PATH = os.path.join("datasets", "titanic")


# In[70]:


import pandas as pd

def load_titanic_data(filename, titanic_path=TITANIC_PATH):
    csv_path = os.path.join(titanic_path, filename)
    return pd.read_csv(csv_path)


# In[71]:


train_data = load_titanic_data("train.csv")
test_data = load_titanic_data("test.csv")


# The data is already split into a training set and a test set. However, the test data does *not* contain the labels: your goal is to train the best model you can using the training data, then make your predictions on the test data and upload them to Kaggle to see your final score.

# Let's take a peek at the top few rows of the training set:

# In[72]:


train_data.head()


# The attributes have the following meaning:
# * **Survived**: that's the target, 0 means the passenger did not survive, while 1 means he/she survived.
# * **Pclass**: passenger class.
# * **Name**, **Sex**, **Age**: self-explanatory
# * **SibSp**: how many siblings & spouses of the passenger aboard the Titanic.
# * **Parch**: how many children & parents of the passenger aboard the Titanic.
# * **Ticket**: ticket id
# * **Fare**: price paid (in pounds)
# * **Cabin**: passenger's cabin number
# * **Embarked**: where the passenger embarked the Titanic

# Let's get more info to see how much data is missing:

# In[73]:


train_data.info()


# Okay, the **Age**, **Cabin** and **Embarked** attributes are sometimes null (less than 891 non-null), especially the **Cabin** (77% are null). We will ignore the **Cabin** for now and focus on the rest. The **Age** attribute has about 19% null values, so we will need to decide what to do with them. Replacing null values with the median age seems reasonable.

# The **Name** and **Ticket** attributes may have some value, but they will be a bit tricky to convert into useful numbers that a model can consume. So for now, we will ignore them.

# Let's take a look at the numerical attributes:

# In[74]:


train_data.describe()


# * Yikes, only 38% **Survived**. :(  That's close enough to 40%, so accuracy will be a reasonable metric to evaluate our model.
# * The mean **Fare** was £32.20, which does not seem so expensive (but it was probably a lot of money back then).
# * The mean **Age** was less than 30 years old.

# Let's check that the target is indeed 0 or 1:

# In[75]:


train_data["Survived"].value_counts()


# Now let's take a quick look at all the categorical attributes:

# In[76]:


train_data["Pclass"].value_counts()


# In[77]:


train_data["Sex"].value_counts()


# In[78]:


train_data["Embarked"].value_counts()


# The Embarked attribute tells us where the passenger embarked: C=Cherbourg, Q=Queenstown, S=Southampton.

# **Note**: the code below uses a mix of `Pipeline`, `FeatureUnion` and a custom `DataFrameSelector` to preprocess some columns differently.  Since Scikit-Learn 0.20, it is preferable to use a `ColumnTransformer`, like in the previous chapter.

# Now let's build our preprocessing pipelines. We will reuse the `DataframeSelector` we built in the previous chapter to select specific attributes from the `DataFrame`:

# In[79]:


from sklearn.base import BaseEstimator, TransformerMixin

class DataFrameSelector(BaseEstimator, TransformerMixin):
    def __init__(self, attribute_names):
        self.attribute_names = attribute_names
    def fit(self, X, y=None):
        return self
    def transform(self, X):
        return X[self.attribute_names]
    


# Let's build the pipeline for the numerical attributes:

# In[80]:


from sklearn.pipeline import Pipeline
from sklearn.impute import SimpleImputer

num_pipeline = Pipeline([
        ("select_numeric", DataFrameSelector(["Age", "SibSp", "Parch", "Fare"])),
        ("imputer", SimpleImputer(strategy="median")),
    ])


# In[ ]:


num_pipeline.fit_transform(train_data)


# We will also need an imputer for the string categorical columns (the regular `SimpleImputer` does not work on those):

# In[81]:


# Inspired from stackoverflow.com/questions/25239958
class MostFrequentImputer(BaseEstimator, TransformerMixin):
    def fit(self, X, y=None):
        self.most_frequent_ = pd.Series([X[c].value_counts().index[0] for c in X],
                                        index=X.columns)
        return self
    def transform(self, X, y=None):
        return X.fillna(self.most_frequent_)


# In[82]:


from sklearn.preprocessing import OneHotEncoder


# Now we can build the pipeline for the categorical attributes:

# In[83]:


cat_pipeline = Pipeline([
        ("select_cat", DataFrameSelector(["Pclass", "Sex", "Embarked"])),
        ("imputer", MostFrequentImputer()),
        ("cat_encoder", OneHotEncoder(sparse=False)),
    ])


# In[ ]:


cat_pipeline.fit_transform(train_data)


# Finally, let's join the numerical and categorical pipelines:

# In[84]:


from sklearn.pipeline import FeatureUnion
preprocess_pipeline = FeatureUnion(transformer_list=[
        ("num_pipeline", num_pipeline),
        ("cat_pipeline", cat_pipeline),
    ])


# Cool! Now we have a nice preprocessing pipeline that takes the raw data and outputs numerical input features that we can feed to any Machine Learning model we want.

# In[ ]:


X_train = preprocess_pipeline.fit_transform(train_data)
X_train


# Let's not forget to get the labels:

# In[ ]:


y_train = train_data["Survived"]


# We are now ready to train a classifier. Let's start with an `SVC`:

# In[ ]:


from sklearn.svm import SVC

svm_clf = SVC(gamma="auto")
svm_clf.fit(X_train, y_train)


# Great, our model is trained, let's use it to make predictions on the test set:

# In[ ]:


X_test = preprocess_pipeline.transform(test_data)
y_pred = svm_clf.predict(X_test)


# And now we could just build a CSV file with these predictions (respecting the format excepted by Kaggle), then upload it and hope for the best. But wait! We can do better than hope. Why don't we use cross-validation to have an idea of how good our model is?

# In[ ]:


from sklearn.model_selection import cross_val_score

svm_scores = cross_val_score(svm_clf, X_train, y_train, cv=10,n_jobs=-1)
svm_scores.mean()


# Okay, over 73% accuracy, clearly better than random chance, but it's not a great score. Looking at the [leaderboard](https://www.kaggle.com/c/titanic/leaderboard) for the Titanic competition on Kaggle, you can see that you need to reach above 80% accuracy to be within the top 10% Kagglers. Some reached 100%, but since you can easily find the [list of victims](https://www.encyclopedia-titanica.org/titanic-victims/) of the Titanic, it seems likely that there was little Machine Learning involved in their performance! ;-) So let's try to build a model that reaches 80% accuracy.

# Let's try a `RandomForestClassifier`:

# In[ ]:


from sklearn.ensemble import RandomForestClassifier

forest_clf = RandomForestClassifier(n_estimators=100, random_state=42,n_jobs=-1)
forest_scores = cross_val_score(forest_clf, X_train, y_train, cv=10,n_jobs=-1)
forest_scores.mean()


# That's much better!

# Instead of just looking at the mean accuracy across the 10 cross-validation folds, let's plot all 10 scores for each model, along with a box plot highlighting the lower and upper quartiles, and "whiskers" showing the extent of the scores (thanks to Nevin Yilmaz for suggesting this visualization). Note that the `boxplot()` function detects outliers (called "fliers") and does not include them within the whiskers. Specifically, if the lower quartile is $Q_1$ and the upper quartile is $Q_3$, then the interquartile range $IQR = Q_3 - Q_1$ (this is the box's height), and any score lower than $Q_1 - 1.5 \times IQR$ is a flier, and so is any score greater than $Q3 + 1.5 \times IQR$.

# In[ ]:


plt.figure(figsize=(8, 4))
plt.plot([1]*10, svm_scores, ".")
plt.plot([2]*10, forest_scores, ".")
plt.boxplot([svm_scores, forest_scores], labels=("SVM","Random Forest"))
plt.ylabel("Accuracy", fontsize=14)
plt.show()


# To improve this result further, you could:
# * Compare many more models and tune hyperparameters using cross validation and grid search,
# * Do more feature engineering, for example:
#   * replace **SibSp** and **Parch** with their sum,
#   * try to identify parts of names that correlate well with the **Survived** attribute (e.g. if the name contains "Countess", then survival seems more likely),
# * try to convert numerical attributes to categorical attributes: for example, different age groups had very different survival rates (see below), so it may help to create an age bucket category and use it instead of the age. Similarly, it may be useful to have a special category for people traveling alone since only 30% of them survived (see below).

# In[ ]:


train_data["AgeBucket"] = train_data["Age"] // 15 * 15
train_data[["AgeBucket", "Survived"]].groupby(['AgeBucket']).mean()


# In[ ]:


train_data["RelativesOnboard"] = train_data["SibSp"] + train_data["Parch"]
train_data[["RelativesOnboard", "Survived"]].groupby(['RelativesOnboard']).mean()


# ### ME 

# In[85]:


studyGlobalClassif=studyGlobal_MNIST


# In[94]:


X_train,X_test,y_train,y_test = train_test_split(train_data.drop("Survived",axis=1),
                                                 train_data["Survived"],random_state=42)


# In[100]:


studyGlobalClassif.saveDatasWithId("DATA_TITANIC", X_train,y_train,
                                                   X_test,y_test,["mort","survivant"])


# In[101]:


studyGlobalClassif_titanic=studyGlobalClassif.addOrGetStudy("Titanic",StudyClassif)


# In[102]:


studyGlobalClassif_titanic.setDataTrainTest(id_="DATA_TITANIC")


# In[103]:


from sklearn.base import BaseEstimator, TransformerMixin
from sklearn.pipeline import Pipeline
from sklearn.impute import SimpleImputer
from sklearn.preprocessing import OneHotEncoder
from sklearn.pipeline import FeatureUnion
class DataFrameSelector(BaseEstimator, TransformerMixin):
    def __init__(self, attribute_names):
        self.attribute_names = attribute_names
    def fit(self, X, y=None):
        return self
    def transform(self, X):
        return X[self.attribute_names]
    
# Inspired from stackoverflow.com/questions/25239958
class MostFrequentImputer(BaseEstimator, TransformerMixin):
    def fit(self, X, y=None):
        self.most_frequent_ = pd.Series([X[c].value_counts().index[0] for c in X],
                                        index=X.columns)
        return self
    def transform(self, X, y=None):
        return X.fillna(self.most_frequent_)


# In[104]:


def preprocess_titanic(x1,x2,y1,y2,n):
    num_pipeline = Pipeline([
        ("select_numeric", DataFrameSelector(["Age", "SibSp", "Parch", "Fare"])),
        ("imputer", SimpleImputer(strategy="median")),
    ])
    cat_pipeline = Pipeline([
            ("select_cat", DataFrameSelector(["Pclass", "Sex", "Embarked"])),
            ("imputer", MostFrequentImputer()),
            ("cat_encoder", OneHotEncoder(sparse=False)),
        ])
    preprocess_pipeline = FeatureUnion(transformer_list=[
            ("num_pipeline", num_pipeline),
            ("cat_pipeline", cat_pipeline),
        ])
    return (preprocess_pipeline.fit_transform(x1),preprocess_pipeline.fit_transform(x2),y1,y2,n)


# In[105]:


studyGlobalClassif_titanic.preprocessingData(preprocess_titanic)


# In[106]:


from sklearn.svm import SVC

svm_clf = SVC(gamma="auto",)
mods=[svm_clf]


# In[107]:


studyGlobalClassif_titanic.setModels(mods)


# In[108]:


computeOrSaveAuto(studyGlobalClassif_titanic,cv=3)


# In[111]:


studyGlobalClassif.save()


# In[ ]:





# In[114]:


mplResetDefault()


# In[116]:


studyGlobalClassif_titanic.plot_classe_balance()


# In[115]:


studyGlobalClassif_titanic.plot_classification_report()


# In[127]:


getRes=lambda i:np.transpose(studyGlobalClassif_titanic.getCV()[1]["scores"][i])                 | __.pd.DataFrame(__,
                                       columns=studyGlobalClassif_titanic._names,
                                       index=range(1,4) |pipe_map| "CV {}".format).T \
                | __.np.round(__,decimals=2) \
                | __.mean(axis=1).to_frame().T | __.rename(index={0:i})


# In[130]:


getRes("Tr").append(getRes("Val"))


# In[149]:


studyGlobalClassif_titanic.getCV()[1]["scores"]["Val"] | __.np.mean(__)


# ## 4. Spam classifier

# First, let's fetch the data:

# In[ ]:


import os
import tarfile
from six.moves import urllib

DOWNLOAD_ROOT = "http://spamassassin.apache.org/old/publiccorpus/"
HAM_URL = DOWNLOAD_ROOT + "20030228_easy_ham.tar.bz2"
SPAM_URL = DOWNLOAD_ROOT + "20030228_spam.tar.bz2"
SPAM_PATH = os.path.join("datasets", "spam")

def fetch_spam_data(spam_url=SPAM_URL, spam_path=SPAM_PATH):
    if not os.path.isdir(spam_path):
        os.makedirs(spam_path)
    for filename, url in (("ham.tar.bz2", HAM_URL), ("spam.tar.bz2", SPAM_URL)):
        path = os.path.join(spam_path, filename)
        if not os.path.isfile(path):
            urllib.request.urlretrieve(url, path)
        tar_bz2_file = tarfile.open(path)
        tar_bz2_file.extractall(path=SPAM_PATH)
        tar_bz2_file.close()


# In[ ]:


fetch_spam_data()


# Next, let's load all the emails:

# In[ ]:


HAM_DIR = os.path.join(SPAM_PATH, "easy_ham")
SPAM_DIR = os.path.join(SPAM_PATH, "spam")
ham_filenames = [name for name in sorted(os.listdir(HAM_DIR)) if len(name) > 20]
spam_filenames = [name for name in sorted(os.listdir(SPAM_DIR)) if len(name) > 20]


# In[ ]:


len(ham_filenames)


# In[ ]:


len(spam_filenames)


# We can use Python's `email` module to parse these emails (this handles headers, encoding, and so on):

# In[ ]:


import email
import email.policy

def load_email(is_spam, filename, spam_path=SPAM_PATH):
    directory = "spam" if is_spam else "easy_ham"
    with open(os.path.join(spam_path, directory, filename), "rb") as f:
        return email.parser.BytesParser(policy=email.policy.default).parse(f)


# In[ ]:


ham_emails = [load_email(is_spam=False, filename=name) for name in ham_filenames]
spam_emails = [load_email(is_spam=True, filename=name) for name in spam_filenames]


# Let's look at one example of ham and one example of spam, to get a feel of what the data looks like:

# In[ ]:


print(ham_emails[1].get_content().strip())


# In[ ]:


print(spam_emails[6].get_content().strip())


# Some emails are actually multipart, with images and attachments (which can have their own attachments). Let's look at the various types of structures we have:

# In[ ]:


def get_email_structure(email):
    if isinstance(email, str):
        return email
    payload = email.get_payload()
    if isinstance(payload, list):
        return "multipart({})".format(", ".join([
            get_email_structure(sub_email)
            for sub_email in payload
        ]))
    else:
        return email.get_content_type()


# In[ ]:


from collections import Counter

def structures_counter(emails):
    structures = Counter()
    for email in emails:
        structure = get_email_structure(email)
        structures[structure] += 1
    return structures


# In[ ]:


structures_counter(ham_emails).most_common()


# In[ ]:


structures_counter(spam_emails).most_common()


# It seems that the ham emails are more often plain text, while spam has quite a lot of HTML. Moreover, quite a few ham emails are signed using PGP, while no spam is. In short, it seems that the email structure is useful information to have.

# Now let's take a look at the email headers:

# In[ ]:


for header, value in spam_emails[0].items():
    print(header,":",value)


# There's probably a lot of useful information in there, such as the sender's email address (12a1mailbot1@web.de looks fishy), but we will just focus on the `Subject` header:

# In[ ]:


spam_emails[0]["Subject"]


# Okay, before we learn too much about the data, let's not forget to split it into a training set and a test set:

# In[ ]:


import numpy as np
from sklearn.model_selection import train_test_split

X = np.array(ham_emails + spam_emails)
y = np.array([0] * len(ham_emails) + [1] * len(spam_emails))

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)


# Okay, let's start writing the preprocessing functions. First, we will need a function to convert HTML to plain text. Arguably the best way to do this would be to use the great [BeautifulSoup](https://www.crummy.com/software/BeautifulSoup/) library, but I would like to avoid adding another dependency to this project, so let's hack a quick & dirty solution using regular expressions (at the risk of [un̨ho͞ly radiańcé destro҉ying all enli̍̈́̂̈́ghtenment](https://stackoverflow.com/a/1732454/38626)). The following function first drops the `<head>` section, then converts all `<a>` tags to the word HYPERLINK, then it gets rid of all HTML tags, leaving only the plain text. For readability, it also replaces multiple newlines with single newlines, and finally it unescapes html entities (such as `&gt;` or `&nbsp;`):

# In[ ]:


import re
from html import unescape

def html_to_plain_text(html):
    text = re.sub('<head.*?>.*?</head>', '', html, flags=re.M | re.S | re.I)
    text = re.sub('<a\s.*?>', ' HYPERLINK ', text, flags=re.M | re.S | re.I)
    text = re.sub('<.*?>', '', text, flags=re.M | re.S)
    text = re.sub(r'(\s*\n)+', '\n', text, flags=re.M | re.S)
    return unescape(text)


# Let's see if it works. This is HTML spam:

# In[ ]:


html_spam_emails = [email for email in X_train[y_train==1]
                    if get_email_structure(email) == "text/html"]
sample_html_spam = html_spam_emails[7]
print(sample_html_spam.get_content().strip()[:1000], "...")


# And this is the resulting plain text:

# In[ ]:


print(html_to_plain_text(sample_html_spam.get_content())[:1000], "...")


# Great! Now let's write a function that takes an email as input and returns its content as plain text, whatever its format is:

# In[ ]:


def email_to_text(email):
    html = None
    for part in email.walk():
        ctype = part.get_content_type()
        if not ctype in ("text/plain", "text/html"):
            continue
        try:
            content = part.get_content()
        except: # in case of encoding issues
            content = str(part.get_payload())
        if ctype == "text/plain":
            return content
        else:
            html = content
    if html:
        return html_to_plain_text(html)


# In[ ]:


print(email_to_text(sample_html_spam)[:100], "...")


# Let's throw in some stemming! For this to work, you need to install the Natural Language Toolkit ([NLTK](http://www.nltk.org/)). It's as simple as running the following command (don't forget to activate your virtualenv first; if you don't have one, you will likely need administrator rights, or use the `--user` option):
# 
# `$ pip3 install nltk`

# In[ ]:


try:
    import nltk

    stemmer = nltk.PorterStemmer()
    for word in ("Computations", "Computation", "Computing", "Computed", "Compute", "Compulsive"):
        print(word, "=>", stemmer.stem(word))
except ImportError:
    print("Error: stemming requires the NLTK module.")
    stemmer = None


# We will also need a way to replace URLs with the word "URL". For this, we could use hard core [regular expressions](https://mathiasbynens.be/demo/url-regex) but we will just use the [urlextract](https://github.com/lipoja/URLExtract) library. You can install it with the following command (don't forget to activate your virtualenv first; if you don't have one, you will likely need administrator rights, or use the `--user` option):
# 
# `$ pip3 install urlextract`

# In[ ]:


try:
    import urlextract # may require an Internet connection to download root domain names
    
    url_extractor = urlextract.URLExtract()
    print(url_extractor.find_urls("Will it detect github.com and https://youtu.be/7Pq-S557XQU?t=3m32s"))
except ImportError:
    print("Error: replacing URLs requires the urlextract module.")
    url_extractor = None


# We are ready to put all this together into a transformer that we will use to convert emails to word counters. Note that we split sentences into words using Python's `split()` method, which uses whitespaces for word boundaries. This works for many written languages, but not all. For example, Chinese and Japanese scripts generally don't use spaces between words, and Vietnamese often uses spaces even between syllables. It's okay in this exercise, because the dataset is (mostly) in English.

# In[ ]:


from sklearn.base import BaseEstimator, TransformerMixin

class EmailToWordCounterTransformer(BaseEstimator, TransformerMixin):
    def __init__(self, strip_headers=True, lower_case=True, remove_punctuation=True,
                 replace_urls=True, replace_numbers=True, stemming=True):
        self.strip_headers = strip_headers
        self.lower_case = lower_case
        self.remove_punctuation = remove_punctuation
        self.replace_urls = replace_urls
        self.replace_numbers = replace_numbers
        self.stemming = stemming
    def fit(self, X, y=None):
        return self
    def transform(self, X, y=None):
        X_transformed = []
        for email in X:
            text = email_to_text(email) or ""
            if self.lower_case:
                text = text.lower()
            if self.replace_urls and url_extractor is not None:
                urls = list(set(url_extractor.find_urls(text)))
                urls.sort(key=lambda url: len(url), reverse=True)
                for url in urls:
                    text = text.replace(url, " URL ")
            if self.replace_numbers:
                text = re.sub(r'\d+(?:\.\d*(?:[eE]\d+))?', 'NUMBER', text)
            if self.remove_punctuation:
                text = re.sub(r'\W+', ' ', text, flags=re.M)
            word_counts = Counter(text.split())
            if self.stemming and stemmer is not None:
                stemmed_word_counts = Counter()
                for word, count in word_counts.items():
                    stemmed_word = stemmer.stem(word)
                    stemmed_word_counts[stemmed_word] += count
                word_counts = stemmed_word_counts
            X_transformed.append(word_counts)
        return np.array(X_transformed)


# Let's try this transformer on a few emails:

# In[ ]:


X_few = X_train[:3]
X_few_wordcounts = EmailToWordCounterTransformer().fit_transform(X_few)
X_few_wordcounts


# This looks about right!

# Now we have the word counts, and we need to convert them to vectors. For this, we will build another transformer whose `fit()` method will build the vocabulary (an ordered list of the most common words) and whose `transform()` method will use the vocabulary to convert word counts to vectors. The output is a sparse matrix.

# In[ ]:


from scipy.sparse import csr_matrix

class WordCounterToVectorTransformer(BaseEstimator, TransformerMixin):
    def __init__(self, vocabulary_size=1000):
        self.vocabulary_size = vocabulary_size
    def fit(self, X, y=None):
        total_count = Counter()
        for word_count in X:
            for word, count in word_count.items():
                total_count[word] += min(count, 10)
        most_common = total_count.most_common()[:self.vocabulary_size]
        self.most_common_ = most_common
        self.vocabulary_ = {word: index + 1 for index, (word, count) in enumerate(most_common)}
        return self
    def transform(self, X, y=None):
        rows = []
        cols = []
        data = []
        for row, word_count in enumerate(X):
            for word, count in word_count.items():
                rows.append(row)
                cols.append(self.vocabulary_.get(word, 0))
                data.append(count)
        return csr_matrix((data, (rows, cols)), shape=(len(X), self.vocabulary_size + 1))


# In[ ]:


vocab_transformer = WordCounterToVectorTransformer(vocabulary_size=10)
X_few_vectors = vocab_transformer.fit_transform(X_few_wordcounts)
X_few_vectors


# In[ ]:


X_few_vectors.toarray()


# What does this matrix mean? Well, the 64 in the third row, first column, means that the third email contains 64 words that are not part of the vocabulary. The 1 next to it means that the first word in the vocabulary is present once in this email. The 2 next to it means that the second word is present twice, and so on. You can look at the vocabulary to know which words we are talking about. The first word is "of", the second word is "and", etc.

# In[ ]:


vocab_transformer.vocabulary_


# We are now ready to train our first spam classifier! Let's transform the whole dataset:

# In[ ]:


from sklearn.pipeline import Pipeline

preprocess_pipeline = Pipeline([
    ("email_to_wordcount", EmailToWordCounterTransformer()),
    ("wordcount_to_vector", WordCounterToVectorTransformer()),
])

X_train_transformed = preprocess_pipeline.fit_transform(X_train)


# **Note**: to be future-proof, we set `solver="lbfgs"` since this will be the default value in Scikit-Learn 0.22.

# In[ ]:


from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import cross_val_score

log_clf = LogisticRegression(solver="lbfgs", random_state=42)
score = cross_val_score(log_clf, X_train_transformed, y_train, cv=3, verbose=3)
score.mean()


# Over 98.7%, not bad for a first try! :) However, remember that we are using the "easy" dataset. You can try with the harder datasets, the results won't be so amazing. You would have to try multiple models, select the best ones and fine-tune them using cross-validation, and so on.
# 
# But you get the picture, so let's stop now, and just print out the precision/recall we get on the test set:

# In[ ]:


from sklearn.metrics import precision_score, recall_score

X_test_transformed = preprocess_pipeline.transform(X_test)

log_clf = LogisticRegression(solver="lbfgs", random_state=42)
log_clf.fit(X_train_transformed, y_train)

y_pred = log_clf.predict(X_test_transformed)

print("Precision: {:.2f}%".format(100 * precision_score(y_test, y_pred)))
print("Recall: {:.2f}%".format(100 * recall_score(y_test, y_pred)))


# In[ ]:




