#!/usr/bin/env python
# coding: utf-8

# In[1]:


from preamble import *
get_ipython().run_line_magic('matplotlib', 'inline')


# ## Introduction
# ### Why Machine Learning?
# #### Problems Machine Learning Can Solve

# #### Knowing Your Task and Knowing Your Data

# ### Why Python?

# ### scikit-learn
# #### Installing scikit-learn

# ### Essential Libraries and Tools

# #### Jupyter Notebook

# #### NumPy

# In[1]:


import numpy as np

x = np.array([[1, 2, 3], [4, 5, 6]])
print("x:\n{}".format(x))


# #### SciPy

# In[3]:


from scipy import sparse

# Create a 2D NumPy array with a diagonal of ones, and zeros everywhere else
eye = np.eye(4)
print("NumPy array:\n", eye)


# In[4]:


# Convert the NumPy array to a SciPy sparse matrix in CSR format
# Only the nonzero entries are stored
sparse_matrix = sparse.csr_matrix(eye)
print("\nSciPy sparse CSR matrix:\n", sparse_matrix)


# In[5]:


data = np.ones(4)
row_indices = np.arange(4)
col_indices = np.arange(4)
eye_coo = sparse.coo_matrix((data, (row_indices, col_indices)))
print("COO representation:\n", eye_coo)


# #### matplotlib

# In[6]:


get_ipython().run_line_magic('matplotlib', 'inline')
import matplotlib.pyplot as plt

# Generate a sequence of numbers from -10 to 10 with 100 steps in between
x = np.linspace(-10, 10, 100)
# Create a second array using sine
y = np.sin(x)
# The plot function makes a line chart of one array against another
plt.plot(x, y, marker="x")


# #### pandas

# In[7]:


import pandas as pd

# create a simple dataset of people
data = {'Name': ["John", "Anna", "Peter", "Linda"],
        'Location' : ["New York", "Paris", "Berlin", "London"],
        'Age' : [24, 13, 53, 33]
       }

data_pandas = pd.DataFrame(data)
# IPython.display allows "pretty printing" of dataframes
# in the Jupyter notebook
display(data_pandas)


# In[8]:


# Select all rows that have an age column greater than 30
display(data_pandas[data_pandas.Age > 30])


# #### mglearn

# ### Python 2 versus Python 3

# ### Versions Used in this Book

# In[2]:


import sys
print("Python version:", sys.version)

import pandas as pd
print("pandas version:", pd.__version__)

import matplotlib
print("matplotlib version:", matplotlib.__version__)

import numpy as np
print("NumPy version:", np.__version__)

import scipy as sp
print("SciPy version:", sp.__version__)

import IPython
print("IPython version:", IPython.__version__)

import sklearn
print("scikit-learn version:", sklearn.__version__)


# ### A First Application: Classifying Iris Species
# ![sepal_petal](images/iris_petal_sepal.png)
# #### Meet the Data

# In[3]:


from sklearn.datasets import load_iris
iris_dataset = load_iris()


# In[4]:


print("Keys of iris_dataset:\n", iris_dataset.keys())


# In[5]:


print(iris_dataset['DESCR'][:193] + "\n...")


# In[6]:


print("Target names:", iris_dataset['target_names'])


# In[7]:


print("Feature names:\n", iris_dataset['feature_names'])


# In[8]:


print("Type of data:", type(iris_dataset['data']))


# In[9]:


print("Shape of data:", iris_dataset['data'].shape)


# In[10]:


print("First five rows of data:\n", iris_dataset['data'][:5])


# In[11]:


print("Type of target:", type(iris_dataset['target']))


# In[12]:


print("Shape of target:", iris_dataset['target'].shape)


# In[13]:


print("Target:\n", iris_dataset['target'])


# #### Measuring Success: Training and Testing Data

# In[14]:


from sklearn.model_selection import train_test_split
X_train, X_test, y_train, y_test = train_test_split(
    iris_dataset['data'], iris_dataset['target'], random_state=0)


# In[15]:


print("X_train shape:", X_train.shape)
print("y_train shape:", y_train.shape)


# In[16]:


print("X_test shape:", X_test.shape)
print("y_test shape:", y_test.shape)


# #### First Things First: Look at Your Data

# In[18]:


import mglearn


# In[19]:


# create dataframe from data in X_train
# label the columns using the strings in iris_dataset.feature_names
iris_dataframe = pd.DataFrame(X_train, columns=iris_dataset.feature_names)
# create a scatter matrix from the dataframe, color by y_train
pd.plotting.scatter_matrix(iris_dataframe, c=y_train, figsize=(15, 15),
                           marker='o', hist_kwds={'bins': 20}, s=60,
                           alpha=.8, cmap=mglearn.cm3);


# #### Building Your First Model: k-Nearest Neighbors

# In[20]:


from sklearn.neighbors import KNeighborsClassifier
knn = KNeighborsClassifier(n_neighbors=1)


# In[21]:


knn.fit(X_train, y_train)


# #### Making Predictions

# In[22]:


X_new = np.array([[5, 2.9, 1, 0.2]])
print("X_new.shape:", X_new.shape)


# In[23]:


prediction = knn.predict(X_new)
print("Prediction:", prediction)
print("Predicted target name:",
       iris_dataset['target_names'][prediction])


# #### Evaluating the Model

# In[24]:


y_pred = knn.predict(X_test)
print("Test set predictions:\n", y_pred)


# In[25]:


print("Test set score: {:.2f}".format(np.mean(y_pred == y_test)))


# In[26]:


print("Test set score: {:.2f}".format(knn.score(X_test, y_test)))


# ### Summary and Outlook

# In[34]:


X_train, X_test, y_train, y_test = train_test_split(
    iris_dataset['data'], iris_dataset['target'], random_state=0)

knn = KNeighborsClassifier(n_neighbors=1)
knn.fit(X_train, y_train)

print("Test set score: {:.2f}".format(knn.score(X_test, y_test)))


# In[82]:


from sklearn.metrics import confusion_matrix,classification_report


# In[67]:


from fastai import *
from fastai.basics import ClassificationInterpretation
import matplotlib.pyplot as plt
import itertools
def plot_confusion_matrix2(pred,true,classes=None,normalize:bool=False, title:str='Confusion matrix', cmap="Blues", slice_size=1,
                              norm_dec:int=2, plot_txt:bool=True, return_fig:bool=None, **kwargs):
        "Plot the confusion matrix, with `title` and using `cmap`."
        # This function is mainly copied from the sklearn docs
        if classes is None:
            classes = np.unique(np.concatenate([pred,true]))
        cm = confusion_matrix(true,pred)
        if normalize: cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
        fig = plt.figure(**kwargs)
        plt.imshow(cm, interpolation='nearest', cmap=cmap)
        plt.title(title)
        tick_marks = np.arange(len(classes))
        plt.xticks(tick_marks, classes, rotation=90)
        plt.yticks(tick_marks, classes, rotation=0)

        if plot_txt:
            thresh = cm.max() / 2.
            for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
                coeff = f'{cm[i, j]:.{norm_dec}f}' if normalize else f'{cm[i, j]}'
                plt.text(j, i, coeff, horizontalalignment="center", verticalalignment="center", color="white" if cm[i, j] > thresh else "black")

        plt.tight_layout()
        plt.ylabel('Actual')
        plt.xlabel('Predicted')
        plt.grid(False)
        fig


# In[79]:


def plot_confusion_matrix2(pred,true,classes=None,normalize:bool=False, title:str='Confusion matrix', cmap="Blues", slice_size=1,
                              norm_dec:int=2, plot_txt:bool=True, return_fig:bool=None, **kwargs):
        "Plot the confusion matrix, with `title` and using `cmap`."
        # This function is mainly copied from the sklearn docs
        if classes is None:
            classes = np.unique(np.concatenate([pred,true]))
        cm = confusion_matrix(true,pred)
        if normalize: cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
        fig = plt.figure(**kwargs)
        plt.imshow(cm, interpolation='nearest', cmap=cmap)
        plt.title(title)
        tick_marks = np.arange(len(classes))
        plt.xticks(tick_marks, classes, rotation=90)
        plt.yticks(tick_marks, classes, rotation=0)

        if plot_txt:
            thresh = cm.max() / 2.
            for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
                coeff = f'{cm[i, j]:.{norm_dec}f}' if normalize else f'{cm[i, j]}'
                plt.text(j, i, coeff, horizontalalignment="center", verticalalignment="center", color="white" if cm[i, j] > thresh else "black")

        plt.tight_layout()
        plt.ylabel('Actual')
        plt.xlabel('Predicted')
        plt.grid(False)
        fig


# In[80]:


np.concatenate([y_test,knn.predict(X_test)])


# In[81]:


plot_confusion_matrix2(y_test,knn.predict(X_test))


# In[85]:


print(classification_report(y_test,knn.predict(X_test)))


# In[1]:


import yellowbrick


# In[ ]:





# In[ ]:





# In[87]:


get_ipython().system('pip install yellowbrick --user')


# In[49]:


ConfusionMatrix.get_state()


# In[ ]:




